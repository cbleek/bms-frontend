import setupService from 'src/core/services/_setup';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import {
  AdminUserCredentials,
  AdminUserType,
  APIResponse,
  ContentTypeID,
  UserCredentials,
  UserType
} from 'src/core/types';
import { APIParams } from 'src/types';

const authService = (httpClient: HttpClient<UserType>) => {
  const baseUrl = '/api/auth';

  return {
    loginUser(data: UserCredentials, options?: HttpRequest<UserType>) {
      return httpClient.post<{ jwt: string; refresh_token: string; user: Partial<UserType> }>(
        `${baseUrl}/local`,
        data,
        options
      );
    },

    loginAdmin(data: AdminUserCredentials, options?: HttpRequest<UserType>) {
      return httpClient.post<APIResponse<{ token: string; user: Partial<AdminUserType> }>>(
        '/admin/login',
        data,
        options
      );
    },

    loginWithToken(loginToken: string, options?: HttpRequest<UserType>) {
      return httpClient.get<{ context: string; jwt: string; user: Partial<UserType> }>('/api/passwordless/login', {
        ...options,
        params: { loginToken }
      });
    },

    refreshToken(refresh_token?: string | null, options?: HttpRequest<UserType>) {
      return httpClient.post<{ jwt: string; refresh_token: string }>('/api/token/refresh', { refresh_token }, options);
    },

    registerUser(data: { username: string; email: string; password: string }, options?: HttpRequest<UserType>) {
      return httpClient.post<{ user: Partial<UserType> }>(`${baseUrl}/local/register`, data, options);
    },

    forgotPassword(email: string, options?: HttpRequest<UserType>) {
      return httpClient.post<{ ok: boolean }>(`${baseUrl}/forgot-password`, { email }, options);
    },

    resetPassword(
      data: { password: string; passwordConfirmation: string; code: string },
      options?: HttpRequest<UserType>
    ) {
      return httpClient.post<{ jwt: string; user: Partial<UserType> }>(`${baseUrl}/reset-password`, data, options);
    },

    profileUser(params: APIParams<UserType>, options?: HttpRequest<UserType>) {
      return httpClient.get<Partial<UserType>>('/api/users/me', { ...options, params });
    },

    updateUser(id: ContentTypeID, data: Partial<UserType>, options?: HttpRequest<UserType>) {
      return httpClient.put<Partial<UserType>>(`/api/users/${id}`, data, options);
    }
  };
};

export default setupService<UserType, ReturnType<typeof authService>>(authService);
