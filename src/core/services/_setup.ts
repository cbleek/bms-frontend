import { ShallowRef, unref } from 'vue';
import { HttpClient } from 'src/core/lib/http';

/**
 * Typed utility that injects http clients into services
 *
 * @param callback
 */
export default function setupService<T, TService>(callback: (_: HttpClient<T>) => TService) {
  return (http: ShallowRef<HttpClient<T> | undefined>) => {
    const httpValue = unref(http);

    if (!httpValue) throw new Error('Missing http client');
    return callback(httpValue);
  };
}
