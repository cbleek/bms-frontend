import setupService from 'src/core/services/_setup';
import {
  APICollectionResponse,
  APIResponse,
  CompanySchema,
  CompanyType,
  ContentTypeID,
  MediaType
} from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const companyService = (httpClient: HttpClient<CompanyType>) => {
  const baseUrl = '/api/company';

  type RegisterData = { email: string; firstname: string; lastname: string };

  return {
    getAll(params?: APIParams<CompanyType>, options?: HttpRequest<CompanyType>) {
      return httpClient.get<APICollectionResponse<CompanySchema>>('/api/companies', { ...options, params });
    },

    getById(id: ContentTypeID, params?: APIParams<CompanyType>, options?: HttpRequest<CompanyType>) {
      return httpClient.get<APIResponse<CompanySchema>>(`/api/companies/${id}`, { ...options, params });
    },

    register(data: RegisterData, options?: HttpRequest<RegisterData>) {
      // data possibly Partial<user>
      // returns APIResponse<user>
      return httpClient.post(`${baseUrl}/register`, { data }, options);
    },

    update(data: Partial<CompanyType>, options?: HttpRequest<CompanyType>) {
      return httpClient.put<APIResponse<CompanySchema>>(`${baseUrl}/careersite`, data, options);
    },

    updateStatus(careersiteEnabled: boolean, options?: HttpRequest<CompanyType>) {
      return httpClient.post<APIResponse<CompanySchema>>(`${baseUrl}/careersite`, { careersiteEnabled }, options);
    },

    updateLogo(data: FormData, options?: HttpRequest<unknown>) {
      // returns logo
      return httpClient.post<MediaType>(`${baseUrl}/logo`, data, {
        ...options,
        headers: { 'Content-Type': 'multipart/form-data' }
      });
    },

    updateHeader(id: ContentTypeID, data: FormData, options?: HttpRequest<unknown>) {
      return httpClient.post<{ company: Partial<CompanyType>; photo: MediaType }>(`${baseUrl}/header/${id}`, data, {
        ...options,
        headers: { 'Content-Type': 'multipart/form-data' }
      });
    }
  };
};

export default setupService<CompanyType, ReturnType<typeof companyService>>(companyService);
