import setupService from 'src/core/services/_setup';
import { APIResponse, CompanyType, ContentTypeID, MailTemplateCompanyType, UserType } from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const adminService = (httpClient: HttpClient<unknown>) => {
  const collectionBaseUrl = '/content-manager/collection-types';
  const relationBaseUrl = '/content-manager/relations';

  return {
    getCompanies(params?: APIParams<CompanyType>, options?: HttpRequest<CompanyType>) {
      return httpClient.get(`${collectionBaseUrl}/api::company.company`, {
        ...options,
        params
      });
    },

    getMailTemplates(params?: APIParams<MailTemplateCompanyType>, options?: HttpRequest<MailTemplateCompanyType>) {
      return httpClient.get(`${collectionBaseUrl}/api::mail-template-company.mail-template-company`, {
        ...options,
        params
      });
    },

    getMailTemplate(id: ContentTypeID, options?: HttpRequest<MailTemplateCompanyType>) {
      return httpClient.get(`${collectionBaseUrl}/api::mail-template-company.mail-template-company/${id}`, options);
    },

    updateMailTemplate(
      id: ContentTypeID,
      data: Partial<MailTemplateCompanyType>,
      options?: HttpRequest<MailTemplateCompanyType>
    ) {
      return httpClient.put(
        `${collectionBaseUrl}/api::mail-template-company.mail-template-company/${id}`,
        data,
        options
      );
    },

    getMailTemplateCompany(id: ContentTypeID, options?: HttpRequest<CompanyType>) {
      return httpClient.get(
        `${relationBaseUrl}/api::mail-template-company.mail-template-company/${id}/company`,
        options
      );
    },

    getMailTemplateMessage(id: ContentTypeID, options?: HttpRequest<unknown>) {
      return httpClient.get(
        `${relationBaseUrl}/api::mail-template-company.mail-template-company/${id}/mailTemplate`,
        options
      );
    },

    getMailTemplateRelations(options?: HttpRequest<unknown>) {
      return httpClient.get(
        `${relationBaseUrl}/api::mail-template-company.mail-template-company/mailTemplate`,
        options
      );
    },

    getMailTemplateCompanyRelations(options?: HttpRequest<unknown>) {
      return httpClient.get(`${relationBaseUrl}/api::mail-template-company.mail-template-company/company`, options);
    },

    getUsers(params?: APIParams<UserType>, options?: HttpRequest<UserType>) {
      return httpClient.get(`${collectionBaseUrl}/plugin::users-permissions.user`, {
        ...options,
        params
      });
    },

    init() {
      return httpClient.get<APIResponse<{ authLogo: string; menuLogo: string; hasAdmin: boolean; uuid: boolean }>>(
        '/admin/init'
      );
    }
  };
};

export default setupService<unknown, ReturnType<typeof adminService>>(adminService);
