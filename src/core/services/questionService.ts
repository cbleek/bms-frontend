import setupService from 'src/core/services/_setup';
import {
    APICollectionResponse,
    APIResponse,
    ContentTypeID,
    QuestionSchema,
    QuestionType
} from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const questionService = (httpClient: HttpClient<QuestionType>) => {
    const baseUrl = '/api/questions';

    return {
        getAll(params?: APIParams<QuestionType>, options?: HttpRequest<QuestionType>) {
            return httpClient.get<APICollectionResponse<QuestionSchema>>(baseUrl, { ...options, params });
        },

        create(data: Partial<QuestionType>, options?: HttpRequest<QuestionType>) {
            return httpClient.post<APIResponse<QuestionSchema>>(baseUrl, { data }, options);
        },
        update(id: ContentTypeID, data: Partial<QuestionType>, options?: HttpRequest<QuestionType>) {
            return httpClient.put<APIResponse<QuestionSchema>>(`${baseUrl}/${id}`, { data }, options);
        },

        delete(id: ContentTypeID, options?: HttpRequest<QuestionType>) {
            return httpClient.delete<APIResponse<QuestionSchema>>(`${baseUrl}/${id}`, options);
        }
    };
};

export default setupService<QuestionType, ReturnType<typeof questionService>>(questionService);
