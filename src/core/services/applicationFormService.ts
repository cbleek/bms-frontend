import setupService from 'src/core/services/_setup';
import {
  APICollectionResponse,
  APIResponse,
  ApplicationFormSchema,
  ApplicationFormType,
  ContentTypeID
} from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const applicationFormService = (httpClient: HttpClient<ApplicationFormType>) => {
  const baseUrl = '/api/application-forms';

  return {
    getAll(params?: APIParams<ApplicationFormType>, options?: HttpRequest<ApplicationFormType>) {
      return httpClient.get<APICollectionResponse<ApplicationFormSchema>>(baseUrl, { ...options, params });
    },

    getById(id: ContentTypeID, params?: APIParams<ApplicationFormType>, options?: HttpRequest<ApplicationFormType>) {
      return httpClient.get<APIResponse<ApplicationFormSchema>>(`${baseUrl}/${id}`, { ...options, params });
    },

    create(data: Partial<ApplicationFormType>, options?: HttpRequest<ApplicationFormType>) {
      return httpClient.post<APIResponse<ApplicationFormSchema>>(baseUrl, { data }, options);
    },

    update(id: ContentTypeID, data: Partial<ApplicationFormType>, options?: HttpRequest<ApplicationFormType>) {
      return httpClient.put<APIResponse<ApplicationFormSchema>>(`${baseUrl}/${id}`, { data }, options);
    },

    delete(id: ContentTypeID, options?: HttpRequest<ApplicationFormType>) {
      return httpClient.delete<APIResponse<ApplicationFormSchema>>(`${baseUrl}/${id}`, options);
    }
  };
};

export default setupService<ApplicationFormType, ReturnType<typeof applicationFormService>>(applicationFormService);
