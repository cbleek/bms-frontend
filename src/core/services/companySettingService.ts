import setupService from 'src/core/services/_setup';
import { CompanySettings, CompanySettingType } from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const companySettingService = (httpClient: HttpClient<CompanySettingType>) => {
  const baseUrl = '/api/company-settings';

  return {
    getAll(params?: APIParams<CompanySettingType>, options?: HttpRequest<CompanySettingType>) {
      return httpClient.get<CompanySettings>(baseUrl, { ...options, params });
    },

    update(data: Partial<CompanySettings>, options?: HttpRequest<CompanySettingType>) {
      return httpClient.post<Partial<CompanySettings>>(baseUrl, { data }, options);
    }
  };
};

export default setupService<CompanySettingType, ReturnType<typeof companySettingService>>(companySettingService);
