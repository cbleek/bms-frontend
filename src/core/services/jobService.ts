import setupService from './_setup';
import { APICollectionResponse, APIResponse, ContentTypeID, JobSchema, JobType } from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const jobService = (httpClient: HttpClient<JobType>) => {
  const baseUrl = '/api/jobs';

  return {
    getAll(params?: APIParams<JobType>, options?: HttpRequest<JobType>) {
      return httpClient.get<APICollectionResponse<JobSchema>>(baseUrl, { ...options, params });
    },

    getById(id: ContentTypeID, params?: APIParams<JobType>, options?: HttpRequest<JobType>) {
      return httpClient.get<APIResponse<JobSchema>>(`${baseUrl}/${id}`, { ...options, params });
    },

    getByCompany(params?: APIParams<JobType>, options?: HttpRequest<JobType>) {
      return httpClient.get<APICollectionResponse<JobSchema>>('/api/company/jobs', { ...options, params });
    },

    create(data: Partial<JobType>, options?: HttpRequest<JobType>) {
      return httpClient.post<APIResponse<JobSchema>>(baseUrl, { data }, options);
    },

    update(id: ContentTypeID, data: Partial<JobType>, options?: HttpRequest<JobType>) {
      return httpClient.put<APIResponse<JobSchema>>(`${baseUrl}/${id}`, { data }, options);
    },

    delete(id: ContentTypeID, options?: HttpRequest<JobType>) {
      return httpClient.delete<APIResponse<JobSchema>>(`${baseUrl}/${id}`, options);
    }
  };
};

export default setupService<JobType, ReturnType<typeof jobService>>(jobService);
