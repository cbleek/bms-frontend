import setupService from 'src/core/services/_setup';
import {
    APICollectionResponse,
    APIResponse,
    ContentTypeID,
    AnswerSchema,
    AnswerType
} from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const answerService = (httpClient: HttpClient<AnswerType>) => {
    const baseUrl = '/api/answers';
    return {
        getAll(params?: APIParams<AnswerType>, options?: HttpRequest<AnswerType>) {
            return httpClient.get<APICollectionResponse<AnswerSchema>>(baseUrl, { ...options, params });
        },

        create(data: Partial<AnswerType>, options?: HttpRequest<AnswerType>) {
            return httpClient.post<APIResponse<AnswerSchema>>(baseUrl, { data }, options);
        },
        update(id: ContentTypeID, data: Partial<AnswerType>, options?: HttpRequest<AnswerType>) {
            return httpClient.put<APIResponse<AnswerSchema>>(`${baseUrl}/${id}`, { data }, options);
        },

        delete(id: ContentTypeID, options?: HttpRequest<AnswerType>) {
            return httpClient.delete<APIResponse<AnswerSchema>>(`${baseUrl}/${id}`, options);
        }
    };
};

export default setupService<AnswerType, ReturnType<typeof answerService>>(answerService);
