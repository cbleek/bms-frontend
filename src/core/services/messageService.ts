import setupService from 'src/core/services/_setup';
import { APICollectionResponse, APIResponse, ContentTypeID, MessageSchema, MessageType } from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const messageService = (httpClient: HttpClient<MessageType>) => {
  const baseUrl = '/api/messages';

  return {
    getAll(params?: APIParams<MessageType>, options?: HttpRequest<MessageType>) {
      return httpClient.get<APICollectionResponse<MessageSchema>>(baseUrl, { ...options, params });
    },

    create(data: Partial<MessageType>, options?: HttpRequest<MessageType>) {
      return httpClient.post<APIResponse<MessageSchema>>(baseUrl, { data }, options);
    },

    delete(id: ContentTypeID, options?: HttpRequest<MessageType>) {
      return httpClient.delete<APIResponse<MessageSchema>>(`${baseUrl}/${id}`, options);
    }
  };
};

export default setupService<MessageType, ReturnType<typeof messageService>>(messageService);
