import setupService from 'src/core/services/_setup';
import {
  APICollectionResponse,
  APIResponse,
  ContentTypeID,
  MailTemplateSchema,
  MailTemplateType
} from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const mailTemplateService = (httpClient: HttpClient<MailTemplateType>) => {
  const baseUrl = '/api/mail-template-companies';

  return {
    getAll(params?: APIParams<MailTemplateType>, options?: HttpRequest<MailTemplateType>) {
      return httpClient.get<APICollectionResponse<MailTemplateSchema>>(baseUrl, { ...options, params });
    },

    update(id: ContentTypeID, data: Partial<MailTemplateType>, options?: HttpRequest<MailTemplateType>) {
      return httpClient.put<APIResponse<MailTemplateSchema>>(`${baseUrl}/${id}`, { data }, options);
    }
  };
};

export default setupService<MailTemplateType, ReturnType<typeof mailTemplateService>>(mailTemplateService);
