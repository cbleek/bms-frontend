import setupService from './_setup';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import {
  APICollectionResponse,
  APIResponse,
  CandidateSchema,
  CandidateType,
  ContentTypeID,
  ReviewSchema,
  ReviewType,
  UserSchema
} from 'src/core/types';
import { APIParams } from 'src/types';

const candidateService = (httpClient: HttpClient<CandidateType>) => {
  const baseUrl = '/api/candidate';

  return {
    getAll(params?: APIParams<CandidateType>, options?: HttpRequest<CandidateType>) {
      return httpClient.get<APICollectionResponse<CandidateSchema>>(`${baseUrl}s`, { ...options, params });
    },

    getById(id: ContentTypeID, params?: APIParams<CandidateType>, options?: HttpRequest<CandidateType>) {
      return httpClient.get<APIResponse<CandidateSchema>>(`${baseUrl}s/${id}`, { ...options, params });
    },

    inviteCandidate(data?: Pick<CandidateType, 'id' | 'email'>, options?: HttpRequest<CandidateType>) {
      return httpClient.post<APIResponse<UserSchema>>(`${baseUrl}/invite`, { data }, options);
    },

    reviewCandidate(data: Partial<ReviewType>, options?: HttpRequest<CandidateType>) {
      return httpClient.post<APIResponse<ReviewSchema>>(`${baseUrl}/review`, { data }, options);
    },

    create(data: Partial<CandidateType>, options?: HttpRequest<CandidateType>) {
      return httpClient.post<APIResponse<CandidateSchema>>(`${baseUrl}s`, { data }, options);
    },

    update(id: ContentTypeID, data: Partial<CandidateType>, options?: HttpRequest<CandidateType>) {
      return httpClient.put<APIResponse<CandidateSchema>>(`${baseUrl}s/${id}`, { data }, options);
    },
    createWithFormData(formData: Partial<CandidateSchema>, options?: HttpRequest<CandidateType>) {
      return httpClient.post<APIResponse<CandidateSchema>>(`${baseUrl}s`, formData, { ...options, headers: { 'Content-Type': 'multipart/form-data' }});
    },
  };
};

export default setupService<CandidateType, ReturnType<typeof candidateService>>(candidateService);
