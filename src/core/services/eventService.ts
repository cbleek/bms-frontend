import setupService from 'src/core/services/_setup';
import { ContentTypeID } from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const eventService = (httpClient: HttpClient<unknown>) => {
  const baseUrl = '/api/events';

  return {
    getAll(params?: APIParams<unknown>, options?: HttpRequest<unknown>) {
      return httpClient.get(baseUrl, { ...options, params });
    },

    create(data: unknown, options?: HttpRequest<unknown>) {
      return httpClient.post(baseUrl, { data }, options);
    },

    update(id: ContentTypeID, data: unknown, options?: HttpRequest<unknown>) {
      return httpClient.put(`${baseUrl}/${id}`, { data }, options);
    },

    delete(id: ContentTypeID, options?: HttpRequest<unknown>) {
      return httpClient.delete(`${baseUrl}/${id}`, options);
    }
  };
};

export default setupService<unknown, ReturnType<typeof eventService>>(eventService);
