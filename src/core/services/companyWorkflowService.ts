import setupService from 'src/core/services/_setup';
import {
  APICollectionResponse,
  APIResponse,
  CompanyWorkflowSchema,
  CompanyWorkflowType,
  ContentTypeID
} from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const companyWorkflowService = (httpClient: HttpClient<CompanyWorkflowType>) => {
  const baseUrl = '/api/company-workflows';

  return {
    getAll(params?: APIParams<CompanyWorkflowType>, options?: HttpRequest<CompanyWorkflowType>) {
      return httpClient.get<APICollectionResponse<CompanyWorkflowSchema>>(baseUrl, { ...options, params });
    },

    create(data: Partial<CompanyWorkflowType>, options?: HttpRequest<CompanyWorkflowType>) {
      return httpClient.post<APIResponse<CompanyWorkflowSchema>>(`${baseUrl}`, { data }, options);
    },

    update(id: ContentTypeID, data: Partial<CompanyWorkflowType>, options?: HttpRequest<CompanyWorkflowType>) {
      return httpClient.put<APIResponse<CompanyWorkflowSchema>>(`${baseUrl}/${id}`, { data }, options);
    },

    delete(id: ContentTypeID, options?: HttpRequest<unknown>) {
      return httpClient.delete(`${baseUrl}/${id}`, options);
    }
  };
};

export default setupService<CompanyWorkflowType, ReturnType<typeof companyWorkflowService>>(companyWorkflowService);
