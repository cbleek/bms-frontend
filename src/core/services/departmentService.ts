import setupService from 'src/core/services/_setup';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const departmentService = (httpClient: HttpClient<unknown>) => {
  const baseUrl = '/api/departments';

  return {
    getAll(params?: APIParams<unknown>, options?: HttpRequest<unknown>) {
      return httpClient.get(baseUrl, { ...options, params });
    }
  };
};

export default setupService<unknown, ReturnType<typeof departmentService>>(departmentService);
