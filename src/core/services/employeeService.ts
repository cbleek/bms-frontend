import setupService from 'src/core/services/_setup';
import { ContentTypeID, EmployeeType, UserType } from 'src/core/types';
import { HttpClient, HttpRequest } from 'src/core/lib/http';
import { APIParams } from 'src/types';

const employeeService = (httpClient: HttpClient<EmployeeType>) => {
  const baseUrl = '/api/employee';

  return {
    getAll(params?: APIParams<EmployeeType>, options?: HttpRequest<EmployeeType>) {
      return httpClient.get(`${baseUrl}s`, { ...options, params });
    },

    create(data: Partial<EmployeeType>, options?: HttpRequest<EmployeeType>) {
      return httpClient.post(`${baseUrl}s`, { data }, options);
    },

    update(id: ContentTypeID, data: Partial<EmployeeType>, options?: HttpRequest<EmployeeType>) {
      return httpClient.put(`${baseUrl}s/${id}`, { data }, options);
    },

    updatePassword(password: string, options?: HttpRequest<EmployeeType>) {
      return httpClient.post<Partial<UserType>>(`${baseUrl}/password`, { data: { password }}, options);
    },

    updatePhoto(id: ContentTypeID, data: FormData, options?: HttpRequest<EmployeeType>) {
      return httpClient.put(
        `${baseUrl}/photo/${id}`,
        { data },
        { ...options, headers: { 'Content-Type': 'multipart/form-data' }}
      );
    },

    savePhoto(data: FormData, options?: HttpRequest<EmployeeType>) {
      return httpClient.post(
        `${baseUrl}/photo`,
        { data },
        { ...options, headers: { 'Content-Type': 'multipart/form-data' }}
      );
    },

    delete(id: ContentTypeID, options?: HttpRequest<EmployeeType>) {
      return httpClient.delete(`${baseUrl}s/${id}`, options);
    }
  };
};

export default setupService<EmployeeType, ReturnType<typeof employeeService>>(employeeService);
