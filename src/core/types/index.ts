export * from './api';
export * from './consumers';

export type KeysOfUnion<T, TType> = T extends TType ? T : never;
export type KeysOfPattern<T, TPattern> = { [K in keyof T as K extends TPattern ? K : never]: T[K] };

export type BaseTypeFilter = {
  locale?: string;
  q?: string;
};
