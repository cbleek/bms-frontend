import { ContentType, ContentTypeResponse } from 'src/core/types';

export type QuestionSchema = 'api::question.question';
export type QuestionType = ContentType<QuestionSchema>;
export type QuestionTypeResponse = ContentTypeResponse<QuestionType>;
