import { BaseTypeFilter, ContentType, ContentTypeID, ContentTypeResponse } from 'src/core/types';

export type CandidateSchema = 'api::candidate.candidate';
export type CandidateType = ContentType<CandidateSchema>;
export type CandidateTypeResponse = ContentTypeResponse<CandidateSchema>;

export type CandidateTypeFilter = {
  department?: Array<string>;
  group?: string;
  job?: ContentTypeID;
  recruiter?: 'all' | 'own' | string;
  status?: CandidateType['status'];
} & BaseTypeFilter;
