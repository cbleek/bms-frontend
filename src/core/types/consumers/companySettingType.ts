import { BaseTypeFilter, ContentType, ContentTypeResponse, KeysOfUnion } from 'src/core/types';

export type CompanySettingSchema = 'api::company-setting.company-setting';
export type CompanySettingType = ContentType<CompanySettingSchema>;
export type CompanySettingTypeResponse = ContentTypeResponse<CompanySettingSchema>;

export type CompanySettingTypeFilter = {
  // ...
} & BaseTypeFilter;

export type CompanySettings = Record<KeysOfUnion<CompanySettingType['name'], string>, CompanySettingType['value']>;
