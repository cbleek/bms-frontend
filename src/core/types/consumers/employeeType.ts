import { ContentType, ContentTypeResponse } from 'src/core/types';

export type EmployeeSchema = 'api::employee.employee';
export type EmployeeType = ContentType<EmployeeSchema>;
export type EmployeeTypeResponse = ContentTypeResponse<EmployeeSchema>;
