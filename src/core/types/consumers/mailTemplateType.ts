import { BaseTypeFilter, ContentType, ContentTypeResponse } from 'src/core/types';

export type MailTemplateSchema = 'api::mail-template.mail-template';
export type MailTemplateType = ContentType<MailTemplateSchema>;
export type MailTemplateTypeResponse = ContentTypeResponse<MailTemplateSchema>;

export type MailTemplateCompanySchema = 'api::mail-template-company.mail-template-company';
export type MailTemplateCompanyType = ContentType<MailTemplateCompanySchema>;
export type MailTemplateCompanyTypeResponse = ContentTypeResponse<MailTemplateCompanySchema>;

export type MailTemplateTypeFilter = {
  // ...
} & BaseTypeFilter;
