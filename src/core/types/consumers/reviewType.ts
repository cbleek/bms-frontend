import { ContentType, ContentTypeResponse } from 'src/core/types';

export type ReviewSchema = 'api::review.review';
export type ReviewType = ContentType<ReviewSchema>;
export type ReviewTypeResponse = ContentTypeResponse<ReviewSchema>;
