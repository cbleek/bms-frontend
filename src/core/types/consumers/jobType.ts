import { BaseTypeFilter, ContentType, ContentTypeResponse } from 'src/core/types';

export type JobSchema = 'api::job.job';
export type JobType = ContentType<JobSchema>;
export type JobTypeResponse = ContentTypeResponse<JobSchema>;

export type JobTypeFilter = {
  // ...
} & BaseTypeFilter;
