import { BaseTypeFilter, ContentType, ContentTypeResponse } from 'src/core/types';

export type CompanySchema = 'api::company.company';
export type CompanyType = ContentType<CompanySchema>;
export type CompanyTypeResponse = ContentTypeResponse<CompanySchema>;

export type CompanyTypeFilter = {
  // ...
} & BaseTypeFilter;
