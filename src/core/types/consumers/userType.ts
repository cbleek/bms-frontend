import { ContentType, ContentTypeResponse } from 'src/core/types';

export type UserSchema = 'plugin::users-permissions.user';
export type UserType = ContentType<UserSchema>;
export type UserTypeResponse = ContentTypeResponse<UserSchema>;

export type AdminUserSchema = 'admin::user';
export type AdminUserType = ContentType<AdminUserSchema>;
export type AdminUserTypeResponse = ContentTypeResponse<AdminUserSchema>;

export type UserCredentials = {
  identifier: string;
  password: string;
};

export type AdminUserCredentials = {
  email: string;
  password: string;
};
