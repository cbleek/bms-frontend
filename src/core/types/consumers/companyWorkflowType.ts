import { BaseTypeFilter, ContentType, ContentTypeResponse } from 'src/core/types';

export type CompanyWorkflowSchema = 'api::company-workflow.company-workflow';
export type CompanyWorkflowType = ContentType<CompanyWorkflowSchema>;
export type CompanyWorkflowTypeResponse = ContentTypeResponse<CompanyWorkflowSchema>;

export type CompanyWorkflowTypeFilter = {
  // ...
} & BaseTypeFilter;
