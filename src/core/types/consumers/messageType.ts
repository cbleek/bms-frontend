import { BaseTypeFilter, ContentType, ContentTypeResponse } from 'src/core/types';

export type MessageSchema = 'api::message.message';
export type MessageType = ContentType<MessageSchema>;
export type MessageTypeResponse = ContentTypeResponse<MessageSchema>;

export type MessageTypeFilter = {
  box?: string;
} & BaseTypeFilter;
