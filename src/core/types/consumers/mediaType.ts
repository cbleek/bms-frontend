import { ContentType, ContentTypeResponse } from 'src/core/types';

export type MediaSchema = 'plugin::upload.file';
export type MediaType = ContentType<MediaSchema>;
export type MediaTypeResponse = ContentTypeResponse<MediaSchema>;
