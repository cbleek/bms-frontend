import { BaseTypeFilter, ContentType, ContentTypeResponse, KeysOfPattern } from 'src/core/types';

export type ApplicationFormSchema = 'api::application-form.application-form';
export type ApplicationFormType = ContentType<ApplicationFormSchema>;
export type ApplicationFormTypeResponse = ContentTypeResponse<ApplicationFormSchema>;

export type ApplicationFormTypeFilter = {
  // ...
} & BaseTypeFilter;

export type ApplicationFormSettings = KeysOfPattern<ApplicationFormType, `enable${string}`>;
