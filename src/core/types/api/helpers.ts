import type { Attribute, Common, Entity, Utils } from '@strapi/types';

/**
 * Relation type
 */
type GetRelationValue<T extends Attribute.Attribute> = T extends Attribute.Relation<
  infer _,
  infer TRelationKind,
  infer TTarget
>
  ? TRelationKind extends `${string}ToMany`
    ? Collection<TTarget>
    : Data<TTarget> | null
  : never;

/**
 * Dynamic zone type
 */
type GetDynamicZoneValue<T extends Attribute.Attribute> = T extends Attribute.DynamicZone<infer TComponentsUID>
  ? Array<
      Utils.Array.Values<TComponentsUID> extends infer TComponentUID
        ? TComponentUID extends Common.UID.Component
          ? GetAttributes<TComponentUID> & { id: ContentTypeID; __component: TComponentUID }
          : never
        : never
    >
  : never;

/**
 * Component type
 */
type GetComponentValue<T extends Attribute.Attribute> = T extends Attribute.Component<
  infer TComponentUID,
  infer TRepeatable
>
  ? GetAttributes<TComponentUID> & { id: ContentTypeID } extends infer TValues
    ? Utils.Expression.If<TRepeatable, TValues[], TValues>
    : never
  : never;

/**
 * Media type
 */
type GetMediaValue<T extends Attribute.Attribute> = T extends Attribute.Media<infer _TKind, infer TMultiple>
  ? // strapi auto-generated types does not mark media attributes as multiple
    // use type-casting when needed
    Utils.Expression.If<TMultiple, Collection<'plugin::upload.file'>, Data<'plugin::upload.file'>>
  : never;

/**
 * Json type
 */
type JSONValue = string | number | boolean | null | Array<JSONValue>;
type GetJsonValue<T extends Attribute.Attribute> = T extends Attribute.JSON
  ? // fixes infinite recursion for JSON objects
    // use type-casting when needed
    T extends JSONValue
    ? T
    : unknown
  : never;

/**
 * Attribute type helper
 */
type GetAttribute<T extends Attribute.Attribute> = Utils.Expression.If<
  Utils.Expression.IsNotNever<T>,
  Utils.Expression.MatchFirst<
    [
      [Utils.Expression.Extends<T, Attribute.OfType<'relation'>>, GetRelationValue<T>],
      [Utils.Expression.Extends<T, Attribute.OfType<'dynamiczone'>>, GetDynamicZoneValue<T>],
      [Utils.Expression.Extends<T, Attribute.OfType<'component'>>, GetComponentValue<T>],
      [Utils.Expression.Extends<T, Attribute.OfType<'media'>>, GetMediaValue<T>],
      [Utils.Expression.Extends<T, Attribute.OfType<'json'>>, GetJsonValue<T>],

      // Default
      [Utils.Expression.True, Attribute.GetValue<T>]
    ],
    unknown
  >,
  unknown
>;

/**
 * Attributes type helper
 */
type GetAttributes<T extends Common.UID.Schema, TKey extends Attribute.GetKeys<T> = Attribute.GetKeys<T>> = Omit<
  {
    [key in Attribute.GetRequiredKeys<T> as key extends TKey ? key : never]-?: Attribute.Get<
      T,
      key
    > extends infer TAttribute extends Attribute.Attribute
      ? GetAttribute<TAttribute>
      : never;
  } & {
    [key in Attribute.GetOptionalKeys<T> as key extends TKey ? key : never]?: Attribute.Get<
      T,
      key
    > extends infer TAttribute extends Attribute.Attribute
      ? GetAttribute<TAttribute>
      : never;
  },
  Utils.Object.KeysBy<T, Attribute.Password | Attribute.Private>
>;

export type ContentTypeID = Entity.ID;

export type ContentType<T extends Common.UID.ContentType> = {
  id: ContentTypeID;
} & GetAttributes<T>;

export type ContentTypeResponse<T extends Common.UID.ContentType> = {
  id: ContentTypeID;
  attributes: GetAttributes<T>;
};

export type Data<T extends Common.UID.ContentType> = {
  data: ContentTypeResponse<T>;
};

export type Collection<T extends Common.UID.ContentType> = {
  data: Array<ContentTypeResponse<T>>;
};
