import type { Common } from '@strapi/types';
import { Data, Collection } from './helpers';

export type { ContentType, ContentTypeID, ContentTypeResponse } from './helpers';

export type APIResponse<T> = T extends Common.UID.ContentType ? Data<T> : { data: T };

export type APICollectionResponse<T> = (T extends Common.UID.ContentType ? Collection<T> : { data: Array<T> }) & {
  meta?: {
    pagination: APIPagination;
  };
};

export type APIPagination = {
  page: number;
  pageCount: number;
  pageSize: number;
  total: number;
};
