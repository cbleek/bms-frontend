import { AxiosInstance } from 'axios';
import { useAuthStore } from 'src/stores';
import { notificationService } from 'src/services/_singletons';

function intercept(axios: AxiosInstance) {
  let isRefreshing = false;
  let refreshSubscribers: ((token: string) => void)[] = [];

  axios.interceptors.response.use(null, async error => {
    const authStore = useAuthStore();
    const token = authStore.token;
    const originalRequest = error.config;

    if (error?.code === 'ERR_CANCELED') return Promise.reject(error);

    if (error && !error.response && !error.status) {
      notificationService.showError('Backend is not available right now.');
    } else if (error.response.status === 401 && token) {
      if (!isRefreshing) {
        isRefreshing = true;

        return new Promise<string>((resolve, reject) => {
          refreshSubscribers.push(() => {
            resolve(axios(originalRequest));
          });

          authStore
            .refresh()
            .then(data => {
              refreshSubscribers.forEach(callback => callback(data.jwt));
            })
            .catch(refreshError => {
              refreshSubscribers.forEach(() => reject(refreshError));
            })
            .finally(() => {
              refreshSubscribers = [];
              isRefreshing = false;
            });
        });
      } else {
        return new Promise<string>(resolve => {
          refreshSubscribers.push(() => {
            resolve(axios(originalRequest));
          });
        });
      }
    } else if (error.response.status === 403) {
      const err = error.response.data?.error;
      notificationService.showError(err?.message);
    }

    return Promise.reject(error);
  });
}

export default intercept;
