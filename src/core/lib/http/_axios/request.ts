import { AxiosInstance } from 'axios';
import { useAuthStore } from 'src/stores';

function intercept(axios: AxiosInstance) {
  axios.interceptors.request.use(request => {
    const authStore = useAuthStore();
    if (authStore.isLoggedIn && request.url !== '/api/token/refresh')
      Object.assign(request.headers, { Authorization: `Bearer ${authStore.token}` });

    return request;
  });
}

export default intercept;
