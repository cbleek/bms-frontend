import axios, { AxiosRequestConfig } from 'axios';
import { QSsrContext } from '@quasar/app-vite';
import { HttpProvider } from 'src/core/lib/http';
import requestIntercept from './request';
import responseIntercept from './response';

const httpProvider = HttpProvider<never, AxiosRequestConfig<never>>();

/**
 * Provider strategy
 *
 * @param cached
 * @param ssrContext
 */
export default (cached = true, ssrContext?: QSsrContext) => {
  // when in client mode return cached instance if available
  if (cached && !ssrContext && !!httpProvider.client.value) return httpProvider;

  const newInstance = axios.create({
    baseURL: import.meta.env.VITE_AUTH_URL,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',

      // appending remote ip in server mode so requests get their true origin
      ...(ssrContext?.req && {
        'X-Forwarded-For': ssrContext.req.ip
      })
    }
  });

  // attach interceptors
  requestIntercept(newInstance);
  responseIntercept(newInstance);

  httpProvider.use(newInstance);
  return httpProvider;
};
