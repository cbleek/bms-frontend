import { App, InjectionKey, provide, shallowRef } from 'vue';

export { default as AxiosProvider } from './_axios';

type HttpHeaders = Record<string, unknown>;

type HttpData<T> = Partial<T> | FormData | Record<string, unknown>;

export type HttpResponse<T> = {
  data: T;
  status: number;
  statusText: string;
  headers: HttpHeaders;
};

export type HttpResponseError<T> = {
  data: null;
  error: { details: T; message: string; name: string; status: string };
};

export type HttpRequest<T> = {
  url?: string;
  baseURL?: string;
  headers?: HttpHeaders;
  data?: HttpData<T>;
  params?: URLSearchParams | { [key: string]: unknown };
  signal?: Partial<AbortSignal>;
  timeout?: number;
  onUploadProgress?: (progressEvent: ProgressEvent & { bytes: number }) => void;
  onDownloadProgress?: (progressEvent: ProgressEvent & { bytes: number }) => void;
};

export type HttpError<T extends Record<string, unknown> = never> = {
  code: string;
  message: string;
  name: string;
  response: HttpResponse<HttpResponseError<T>>;
};

export type HttpClient<T, TData extends HttpData<T> = HttpData<T>, TRequest extends HttpRequest<T> = HttpRequest<T>> = {
  post<TResponse, TErrorResponse = never>(
    path: string,
    data?: TData,
    request?: TRequest
  ): Promise<HttpResponse<TResponse> | HttpResponseError<TErrorResponse>>;
  patch<TResponse, TErrorResponse = never>(
    path: string,
    data?: TData,
    request?: TRequest
  ): Promise<HttpResponse<TResponse> | HttpResponseError<TErrorResponse>>;
  put<TResponse, TErrorResponse = never>(
    path: string,
    data?: TData,
    request?: TRequest
  ): Promise<HttpResponse<TResponse> | HttpResponseError<TErrorResponse>>;
  get<TResponse, TErrorResponse = never>(
    path: string,
    request?: TRequest
  ): Promise<HttpResponse<TResponse> | HttpResponseError<TErrorResponse>>;
  delete<TResponse, TErrorResponse = never>(
    path: string,
    request?: TRequest
  ): Promise<HttpResponse<TResponse> | HttpResponseError<TErrorResponse>>;
};

/**
 * Provider instance
 */
export function HttpProvider<TData extends HttpData<unknown>, TRequest extends HttpRequest<unknown>>() {
  const _instance = shallowRef<HttpClient<unknown, TData, TRequest>>();

  return {
    get client() {
      return _instance;
    },

    use(client: HttpClient<unknown, TData, TRequest>) {
      _instance.value = client;
    }
  };
}

export const CoreHttpSymbol: InjectionKey<ReturnType<typeof provideHttp>> = Symbol.for('core:lib-http');

/**
 * Scopes a client to component tree, so it can be accessed within
 *
 * @param provider
 * @param app
 */
export function provideHttp(provider: ReturnType<typeof HttpProvider>, app?: App) {
  if (!app) provide(CoreHttpSymbol, provider.client);
  else app.provide(CoreHttpSymbol, provider.client);
  return provider.client;
}
