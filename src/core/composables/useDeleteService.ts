import { computed, MaybeRefOrGetter, ref, toValue, watch } from 'vue';
import {
  useApplicationFormService,
  useCompanyWorkflowService,
  useEmployeeService,
  useEventService,
  useJobService,
  useMessageService
} from 'src/core/services';
import { useHttp } from 'src/core/composables/useHttp';
import { useService } from 'src/core/composables/useService';

/**
 * Services mappings
 */
const mappedServices = Object.freeze({
  applicationForm: useApplicationFormService,
  employee: useEmployeeService,
  event: useEventService,
  job: useJobService,
  message: useMessageService,
  companyWorkflow: useCompanyWorkflowService
});

/**
 * @param type
 */
export function useDeleteService(type: MaybeRefOrGetter<keyof typeof mappedServices>) {
  type DeleteService<T extends keyof typeof mappedServices> = ReturnType<(typeof mappedServices)[T]>['delete'];

  const entityType = computed(() => toValue(type));
  const entityService = ref<ReturnType<typeof useService<DeleteService<typeof entityType.value>>> | undefined>();

  watch(
    entityType,
    (value, oldValue) => {
      if (!value || value === oldValue) return (entityService.value = undefined);

      const service = mappedServices[value]?.(useHttp());
      if (!service) return (entityService.value = undefined);

      entityService.value = useService(service.delete, { immediate: false });
    },
    { immediate: true }
  );

  return entityService;
}
