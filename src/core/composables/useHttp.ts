import { inject } from 'vue';
import { CoreHttpSymbol } from 'src/core/lib/http';

/**
 * Http composable that gives direct access to client instance
 * Should only be used inside setup hook of components if needed
 */
export function useHttp() {
  const client = inject(CoreHttpSymbol);
  if (!client) throw new Error('Missing app context!');
  return client;
}
