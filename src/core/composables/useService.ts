import { Ref, ref, ShallowRef, shallowRef, watch } from 'vue';
import { HttpError, HttpRequest, HttpResponse, HttpResponseError } from 'src/core/lib/http';

type ServiceApiCall = (...params: [never]) => Promise<HttpResponse<unknown> | HttpResponseError<never>>;
type ServiceApiData<T> = T extends HttpResponse<infer TData>
  ? TData
  : T extends HttpResponseError<infer TDataError>
  ? TDataError
  : never;

type UseApiReturn<T extends ServiceApiCall, TResponse> = {
  error: ShallowRef<HttpError | Error | undefined>;
  response: ShallowRef<TResponse | undefined>;
  data: Ref<ServiceApiData<TResponse> | undefined>;
  isLoading: Ref<boolean>;
  isFinished: Ref<boolean>;
  isCanceled: Ref<boolean>;
  cancel: (message?: string) => void;
  execute: (...args: Parameters<T>) => Promise<TResponse>;
};

type UseApiOptions<T> = {
  immediate?: boolean;
  onSuccess?: (response?: T) => void;
  onError?: (error: unknown) => void;
  onFinish?: () => void;
};

export function useService<T extends ServiceApiCall, TResponse = Awaited<ReturnType<T>>>(
  serviceCall: T,
  serviceProps?: Parameters<T>,
  options?: UseApiOptions<TResponse>
): UseApiReturn<T, TResponse> & Promise<TResponse>;
export function useService<T extends ServiceApiCall, TResponse = Awaited<ReturnType<T>>>(
  serviceCall: T,
  serviceProps?: Parameters<T>
): UseApiReturn<T, TResponse> & Promise<TResponse>;
export function useService<T extends ServiceApiCall, TResponse = Awaited<ReturnType<T>>>(
  serviceCall: T,
  options?: UseApiOptions<TResponse>
): UseApiReturn<T, TResponse> & Promise<TResponse>;
export function useService<T extends ServiceApiCall, TResponse = Awaited<ReturnType<T>>>(
  serviceCall: T
): UseApiReturn<T, TResponse> & Promise<TResponse>;

/**
 * Wrapper composable that provides an isolated context for requests
 *
 * @param serviceCall
 * @param args
 */
export function useService<T extends ServiceApiCall, TResponse = Awaited<ReturnType<T>>>(
  serviceCall: T,
  ...args: unknown[]
): UseApiReturn<T, TResponse> & Promise<TResponse> {
  const serviceProps = (args.length && Array.isArray(args[0]) ? args[0] : []) as Parameters<T>;
  const options = ((args.length && serviceProps?.length ? args[1] : args[0]) as UseApiOptions<TResponse>) ?? {};

  let controller: AbortController;
  const { immediate = true, onSuccess = null, onError = null, onFinish = null } = options;

  const error = shallowRef<HttpError | Error | undefined>();
  const response = shallowRef<TResponse | undefined>();
  const data = shallowRef<ServiceApiData<TResponse> | undefined>();
  const isLoading = ref(false);
  const isFinished = ref(false);
  const isCanceled = ref(false);

  /**
   * Extends service props with default request options
   *
   * @param props
   * @param requestOptions
   */
  const extendProps = (props: Parameters<T>, requestOptions?: HttpRequest<unknown>): Parameters<T> => {
    const args = props.length ? props : serviceProps;
    if (Object.keys(requestOptions as Record<string, unknown>).length < 1) return args;

    args[serviceCall.length - 1] = Object.assign(Object.create(null), args[serviceCall.length - 1], requestOptions);
    return args;
  };

  /**
   * Request cancellation helper
   *
   * @param message
   */
  const cancel = (message?: string) => {
    if (isFinished.value || !isLoading.value) return;

    controller?.abort(message);
    isCanceled.value = true;
    isFinished.value = false;
    isLoading.value = false;
  };

  /**
   * Toggles internal flags
   *
   * @param value
   */
  const load = (value: boolean) => {
    isLoading.value = value;
    isFinished.value = !value;
  };

  /**
   * Promise that awaits the request completion
   *
   */
  const waitExecution = (): Promise<TResponse | undefined> =>
    new Promise((resolve, reject) => {
      const stopWatch = watch(
        isFinished,
        value => {
          if (!value) return;
          stopWatch?.();

          if (error.value) reject(error.value);
          resolve(response.value);
        },
        {
          flush: 'sync',
          immediate: true
        }
      );
    });

  const promise = {
    then: (...args: never[]) => waitExecution().then(...args),
    catch: (...args: never[]) => waitExecution().catch(...args),
    finally: (...args: never[]) => waitExecution().finally(...args)
  } as Promise<TResponse>;

  /**
   * Service execution helper
   *
   * @param args
   */
  const execute = (...args: Parameters<T>) => {
    // reset
    cancel();
    error.value = undefined;
    isCanceled.value = false;

    load(true);
    controller = new AbortController();

    serviceCall
      .call(null, ...extendProps(args, { signal: controller.signal }))
      .then(value => {
        if (isCanceled.value) return;
        if (!value.data) throw new Error((value as HttpResponseError<unknown>).error.message);

        data.value = value.data as ServiceApiData<TResponse>;
        response.value = value as TResponse;
        onSuccess?.(value as TResponse);
      })
      .catch((value: HttpError | Error) => {
        error.value = value;
        onError?.(value);
      })
      .finally(() => {
        onFinish?.();
        load(false);
      });

    return promise;
  };

  if (immediate) execute(...serviceProps);

  return {
    error,
    response,
    data,
    isLoading,
    isFinished,
    isCanceled,
    cancel,
    execute,
    ...promise
  };
}
