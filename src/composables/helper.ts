import { matLooks, matLooks3, matLooks4, matLooks5, matLooksOne, matLooksTwo } from '@quasar/extras/material-icons';

export default function useHelper() {
    const matIcon = (index: number) => {
        const iconNames = [matLooksOne, matLooksTwo, matLooks3, matLooks4, matLooks5, matLooks];
        let iconName = matLooks
        if (index >= 0 && index < iconNames.length) {
            iconName = iconNames[index];
        }
        return iconName;
    }

    return { matIcon };
}
