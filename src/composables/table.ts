'use strict';

import { computed, onMounted, ref } from 'vue';
import { defaults } from 'components/default';
import { useAdminStore } from 'stores/admin';
import { useAuthStore } from 'stores/auth';
import { useCandidateStore } from 'stores/candidate';
import { useCompanyStore } from 'stores/company';
import { useEmployeeStore } from 'stores/employee';
import { useFormStore } from 'stores/form';
import { useJobStore } from 'stores/job';

type ParameterTypes =
  | 'jobs'
  | 'companyjobs'
  | 'forms'
  | 'candidates'
  | 'companies'
  | 'employees'
  | 'messages'
  | 'mailTemplates'
  | 'adminCustomers'
  | 'adminApplicants'
  | 'adminMessageTemplates'
  | 'settings';

export default function useTable(type: ParameterTypes) {
  let fetchFunc = null,
    filterAttribute = null,
    store = null;
  switch (type) {
    case 'jobs':
      fetchFunc = 'fetchJobs';
      filterAttribute = 'filterJobs';
      store = useJobStore();
      break;
    case 'companyjobs':
      fetchFunc = 'fetchCompanyJobs';
      filterAttribute = 'filterJobs';
      store = useJobStore();
      break;
    case 'forms':
      fetchFunc = 'fetchForms';
      filterAttribute = 'filterForms';
      store = useFormStore();
      break;
    case 'candidates':
      fetchFunc = 'fetchCandidates';
      filterAttribute = 'filterCandidates';
      store = useCandidateStore();
      break;
    case 'companies':
      fetchFunc = 'fetchCompanies';
      filterAttribute = 'filterCompanies';
      store = useCompanyStore();
      break;
    case 'employees':
      fetchFunc = 'fetchEmployees';
      filterAttribute = 'filterEmployees';
      store = useEmployeeStore();
      break;
    case 'messages':
      fetchFunc = 'fetchMessages';
      filterAttribute = 'filterMessages';
      store = useAuthStore();
      break;
    case 'mailTemplates':
      fetchFunc = 'fetchMailTemplates';
      filterAttribute = 'filterMailTemplates';
      store = useCompanyStore();
      break;
    case 'adminCustomers':
      fetchFunc = 'fetchCustomers';
      store = useAdminStore();
      break;
    case 'adminApplicants':
      fetchFunc = 'fetchApplicants';
      store = useAdminStore();
      break;
    case 'adminMessageTemplates':
      fetchFunc = 'fetchMessageTemplates';
      store = useAdminStore();
      break;
    case 'settings':
      fetchFunc = 'fetchSettings';
      filterAttribute = 'filterSettings';
      store = useCompanyStore();
      break;
  }

  // TODO: let api service calls define default params
  const filter = ref(defaults[filterAttribute]);
  const rowsPerPage = ref([10, 25, 50]);

  const isLoading = computed(() => {
    return store.isLoading;
  });
  const pagination = computed({
    get() {
      //console.log("TYPE", type, store)
      if (type === 'messages') return store.getMessagesPagination;
      if (type === 'mailTemplates') return store.getMailTemplatesPagination;

      return store.getPagination;
    },
    set(v) {
      console.log('What shall we do with the pagination?', v);
    }
  });
  const data = computed(() => {
    const myStore =  ( type=='companyjobs' ? 'jobs' : type )
    return store[myStore].result;
  });

  function onRequest(args) {
    store[fetchFunc]({ ...args, status: true }).then(() => {
      pagination.value = store.getPagination;
    });
  }

  onMounted(() => {
    onRequest({ filter: filter.value, pagination: { ...defaults.pagination }});
  });
  return { filter, onRequest, pagination, isLoading, rowsPerPage, data };
}
