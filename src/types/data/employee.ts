import { Identifiable, Company, User, Datable } from 'src/types';

export type Employee = {
  company?: Company;
  user?: User;
} & Identifiable
  & Datable;
