import { Identifiable, Datable, User } from 'src/types';

type WorkflowProps = 'description' | 'name';

type Right = {
  read: boolean,
  write: boolean,
  mail: boolean,
  delete: boolean
}

export type WorkflowSetting = {
  user: User,
  rights: Right
}

export type WorkflowStates = {
  label: string;
  value: string;
  enabled: boolean;
  teleport?: string | null,
  icon?: string | null,
  color?: string | null,
  draggable?: boolean
}

export type Workflow = {
  name: string;
  description: string;
  settings: Array<WorkflowSetting>,
  states: Array<WorkflowStates>
} & Record<WorkflowProps, string>
  & Identifiable
  & Datable;

export type Workflows = {
  events: Array<Workflow>
}


