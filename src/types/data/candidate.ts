// import { Identifiable, Datable, Nullable, User } from 'src/types';
//
// type CandidateProps = 'firstname' | 'lastname' | 'phone' | 'email' | 'city' | 'status' | 'gender';
// type OptionalCandidateProps = 'postalcode' | 'street';
// export type Candidate = {
//   job: Nullable<object>; //WrappedData
//   applicant?: User;
//   // attachments: WrappedArray,
//   // recruiter_users: WrappedArray
// } & Record<CandidateProps, string> &
//   Partial<Record<OptionalCandidateProps, Nullable<string>>> &
//   Identifiable &
//   Datable;
//
// export type CandidateFilter = {
//   // me
//   job?: { value?: string }; // simple number
//   status?: { value?: string }; // simple
//   department?: { value?: string }; // array[string]
//   recruiter?: string;
//   group?: string;
//   q?: string;
// };
//
// export type CandidateData = Partial<Pick<Candidate, CandidateProps | OptionalCandidateProps | 'job'>>;
