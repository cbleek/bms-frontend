enum Currency {
  'EUR',
  'CHF',
  'GBP',
  'USD'
}

enum Intervall {
  'hour',
  'day',
  'week',
  'month',
  'year'
}

export type Salary = {
  min: number | string | undefined;
  max: number | string | undefined;
  currency: Currency;
  intervall: Intervall;
  visible: boolean;
};
