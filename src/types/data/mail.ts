import { Identifiable } from 'src/types';

type MailTemplateProps = 'subject' | 'text';
export type MailTemplate = {
  enabled: boolean;
} & Identifiable &
  Record<MailTemplateProps, string>;

export type MailTemplateFilter = {
  locale?: string;
  q?: string;
}

export type MailTemplateData = Partial<Pick<MailTemplate, MailTemplateProps | 'enabled'>>;
