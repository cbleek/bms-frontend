// import { Identifiable, Datable, Salary, Workflow } from 'src/types';
//
// type JobProps =
//   'title' |
//   'intro' |
//   'tasks' |
//   'profile' |
//   'benefits' |
//   'outro' |
//   'education' |
//   'employmentType' |
//   'experience' |
//   'contract' |
//   'careerLevel' |
//   'category' |
//   'applicationLanguage' |
//   'homeoffice' |
//   'country' |
//   'city';
//
// interface IApplicationSettings {
//   emailEnabled: boolean,
//   formEnabled: boolean
//   deadlineEnabled: boolean,
//   applicationStartEnabled: boolean
// }
//
// export type JobType = {
//   salary: Salary;
//   applicationDeadline: Date | null;
//   applicationStart: Date | null;
//   location: JSON;
//   recruiters: [];
//   departmentStaff: [];
//   worksCouncil: [];
//   departments: [];
//   workflow: Workflow;
//   applicationSettings: IApplicationSettings
//
// } & Identifiable
//   & Datable
//   & Record<JobProps, string>;
//
// export type JobFilter = {
//   locale?: string;
//   q?: string;
// }
//
//
