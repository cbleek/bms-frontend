type SettingBooleanProps =  'employesCanCreateCandidate' |
  'jobRequiresRecruiter' |
  'multilingualEnabled' |
  'rolesEnabled';

type SettingNumberProps = 'maxUploadFiles' |
  'maxUploadSizeAttachments' |
  'maxUploadSizePhoto' |
  'minPasswordLength';

type SettingStringProps = 'darkmode' |
  'redirectAfterLogin' |
  'languagesAvailable';

type SettingArrayProps = 'languagesAvailable';

export type Settings = Partial<Record<SettingBooleanProps, boolean>>
  & Partial<Record<SettingNumberProps, number>>
  & Partial<Record<SettingStringProps, string>>
  & Partial<Record<SettingArrayProps,  Array<object>>>;



