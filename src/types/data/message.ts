import { Datable, Identifiable, User, WrappedArray } from 'src/types';

export type Message = {
  message: string;
  subject: string;

  to?: WrappedArray<User>,
  from?: WrappedArray<User>,
} & Datable &
  Identifiable;

export type MessagesFilter = {
  box?: string;
  q?: string;
}
