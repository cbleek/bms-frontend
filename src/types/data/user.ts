import { Datable, Identifiable, Company, Photo, Message, Nullable } from 'src/types';

type UserProps = 'firstname' | 'lastname' | 'email';
type OptionalUserProps = 'ui_display' | 'ui_language' | 'ui_startpage' | 'ui_timezone';
export type User = {
  // preferedLanguage?: string,
  confirmed: boolean;

  jobs?: [];
  candidates?: [];
  messages?: Array<Message>;

  photo?: Photo;
  company?: Company;
} & Datable &
  Identifiable &
  Record<'username' | UserProps, string> &
  Partial<Record<OptionalUserProps, Nullable<string>>>;

export type UserProfileData = Partial<Pick<User, UserProps | OptionalUserProps>>;
