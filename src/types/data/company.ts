import { Datable, Identifiable, Photo, Settings, WrappedData, WrappedArray } from 'src/types';

// ?location?

type CompanyProps = 'description' | 'homepage' | 'industry' | 'name' | 'size';
export type Company = {
  careersiteEnabled: boolean;
  setting: Settings
  logo?: WrappedData<Photo>;
  header?: WrappedArray<Photo>;
  jobs?: []; // WrappedArray<Job>;
  companies?: [];
} & Record<CompanyProps, string>
  & Identifiable
  & Datable;

export type CompanyFilter = {
  q?: string;
};
