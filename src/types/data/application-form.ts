import { Identifiable, Datable } from 'src/types';

type ApplicationFormStringProps = 'description' | 'name' | 'textApply' | 'textPrivacyPolicy';

type ApplicationFormBooleanProps = 'enableJobSharing' |
  'enableApplyLater' |
  'enablePhoto' |
  'enableFirstname' |
  'enableLastname' |
  'enableStreet' |
  'enablePostalcode' |
  'enableCity' |
  'enableGender' |
  'enableEmail' |
  'enablePhone';

export type ApplicationFormBooleans = Record<ApplicationFormBooleanProps, boolean>

export type ApplicationForm = Identifiable
  & Record<ApplicationFormStringProps, string>
  & ApplicationFormBooleans
  & Datable;

export type ApplicationForms = {
  events: Array<ApplicationForm>
}

export type ApplicationFormFilter = {
  q?: string;
  locale: string;
};

