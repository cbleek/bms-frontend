export type Setting = {
  name: string;
  value: boolean | string | number |  Array<object>;
  order?: number;
  type?: string;
  default?: boolean | string | number |  Array<object>;
};
