import { Identifiable, Datable } from 'src/types';

type EventProps = 'description' | 'name' ;

export type Event = {
  date: Date;
  time: number;
  duration: number;
  subject: string;
  desciption?: string;
} & Record<EventProps, string>
  & Identifiable
  & Datable;

export type Events = {
  events: Array<Event>
}
