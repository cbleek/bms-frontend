import { Datable, Identifiable, Nullable } from 'src/types';

type PhotoFormat = {
  // name: string,
  // hash: string,
  // mime: string,
  // size: number,
  ext: string;
  url: string;
  width?: number;
  height?: number;
};

// type PhotoDetails = {
//   alternativeText?: string;
//   caption?: string;
//   previewUrl?: string;
//   provider: string;
//   provider_metadata?: string;
// };

type FormatTypes = 'thumbnail' | 'xsmall' | 'small' | 'medium';
export type Photo = {
  formats: Partial<Record<FormatTypes, Nullable<PhotoFormat>>>; // & { path?: string }>
} & Identifiable &
  Datable &
  PhotoFormat;
