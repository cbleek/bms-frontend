// export type Datable = {
//   createdAt?: Date | string;
//   updatedAt?: Date | string;
// };

// export type Identifiable = {
//   id?: number;
// };

import { APIPagination, ContentType, ContentTypeResponse } from 'src/core/types';

export type Nullable<T> = T | null | undefined;

/**
 * Provides store typed data structure
 */
export type StoredData<T> = T extends ContentTypeResponse<infer TContentType>
  ? {
      result: Array<T>;
      filter?: string;
      paginator?: Partial<APIPagination>;
      sort: `${string & keyof ContentType<TContentType>}:${'asc' | 'desc'}`;
    }
  : never;

/**
 * Provides api typed data structure
 */
// export type Wrapper<T> = { attributes: T } & Identifiable;
// export type WrappedData<T> = { data: Wrapper<T> };
// export type WrappedArray<T> = { data: Array<Wrapper<T>> };

/*
export type ApiResult<T> = {
  attributes: Array<T>
} & Identifiable;
*/

// TODO: improve & move to core

export type APIParams<T> = {
  _q?: string;

  // TODO: infer type relations
  fields?: Array<`${string & keyof T}`>;
  populate?: string | Array<string>;
  sort?: `${string & keyof T}:${'asc' | 'desc'}`;

  [key: `filters[${string}]`]: unknown;

  'pagination[withCount]'?: boolean;
} & Partial<Record<`pagination[${'limit' | 'page' | 'pageSize' | 'start'}]`, number>>;

export type PaginationState = {
  paginator?: Partial<APIPagination>;
  sort: string;
};

export type PaginationProps<T> = {
  descending?: boolean;
  page?: number;
  rowsPerPage?: number;
  rowsNumber?: number;
  sortBy?: string & keyof T;
};

export type FetchParams<T, TFilter> = {
  pagination: PaginationProps<T>;
  filter?: TFilter;

  // TODO: remove from globals
  // used by store/job
  status?: boolean;
};
