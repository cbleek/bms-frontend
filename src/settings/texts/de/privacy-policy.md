# Datenschutzhinweis [^1]

## Wer ist verantwortlich für die Verarbeitung Ihrer personenbezogenen Daten?

Die Personalwerk Holding GmbH (im Folgenden als „wir“ bezeichnet) ist Verantwortlicher im Sinne der EU Datenschutz-Grundverordnung („DSGVO“).

## Für welche Zwecke und auf welcher Rechtsgrundlage verarbeiten wir personenbezogene Daten?

Wir verarbeiten personenbezogene Daten über Sie für den Zweck Ihrer Bewerbung für ein Beschäftigungsverhältnis, soweit dies für die Entscheidung über die Begründung eines Beschäftigungsverhältnisses mit uns erforderlich ist. Rechtsgrundlage ist dabei § 26 Abs. 1 i.V.m Abs. 8 S. 2 BDSG (neu).

Weiterhin können wir personenbezogene Daten über Sie verarbeiten, soweit dies zur Abwehr von geltend gemachten Rechtsansprüchen aus dem Bewerbungsverfahren gegen uns erforderlich ist. Rechtsgrundlage ist dabei Art. 6 Abs. 1, Buch­stabe f DSGVO, das berechtigte Interesse ist beispielsweise eine Beweispflicht in einem Verfahren nach dem Allgemeinen Gleichbehandlungsgesetz (AGG).

Soweit es zu einem Beschäftigungsverhältnis zwischen Ihnen und uns kommt, können wir gemäß § 26 Abs. 1 BDSG (neu) die bereits von Ihnen erhaltenen personenbezogenen Daten für Zwecke des Beschäftigungsverhältnisses weiterverarbeiten, wenn dies für die Durchführung oder Beendigung des Beschäftigungsverhältnisses oder zur Ausübung oder Erfüllung der sich aus einem Gesetz oder einem Tarifvertrag, einer Betriebs- oder Dienstvereinbarung (Kollektivvereinbarung) ergebenden Rechte und Pflichten der Interessenvertretung der Beschäftigten erforderlich ist.

## Welche Kategorien personenbezogener Daten erheben und verarbeiten wir?

Wenn Sie uns im Rahmen des Bewerbungsprozesses personenbezogene Daten mitteilen, werden diese zur Erhebung, Verarbeitung und / oder Nutzung in folgende Datenarten und Datenkategorien eingeteilt:

- Personendaten (Anrede, Titel, Vor- und Nachname, Adresse)
- Kommunikationsdaten (Telefonnr., E-Mail-Adresse)
- Auskunftsangaben (Wodurch sind Sie aufmerksam geworden)
- Daten über die Beurteilung und Bewertung im Bewerbungsverfahren
- Bewerber-Daten via Upload zur Verfügung gestellt (z.B. Anschreiben, Angaben zum bisherigen beruflichen Werdegang, Ausbildungs- und Arbeitszeugnisse, Zertifikate)
- Bewerbungsfoto
- Angaben zu Gehaltsvorstellung
- Bewerbungshistorie

## Welchen Empfängern werden Daten übergeben?

Wir können Ihre personenbezogenen Daten an mit uns verbundene Unternehmen übermitteln, soweit dies im Rahmen der unter Ziff. 2 dargelegten Zwecke und Rechtsgrundlagen zulässig ist. Im Übrigen werden personenbezogene Daten in unserem Auftrag auf Basis von Verträgen nach Art. 28 DSGVO verarbeitet, dies insbesondere durch Hostprovider oder Anbieter von Bewerbermanagementsystemen.

Aufgrund einer gesonderten Vereinbarung über die Verarbeitung von personenbezogenen Daten im Auftrag werden Ihre personenbezogenen Daten von der Firma

- Personalwerk Communications GmbH, Kasteler Str. 22-24, 65205 Wiesbaden

im Rahmen einer Auftragsverarbeitung nach Art. 28 DSGVO gemäß den entsprechenden gesetzlichen Vorgaben in unserem Auftrag erhoben, verarbeitet und genutzt. Hiermit ist jedoch keine Übermittlung Ihrer persönlichen Daten an Dritte im datenschutzrechtlichen Sinne verbunden. Wir, Personalwerk Holding GmbH, bleiben Ihnen gegenüber datenschutzrechtlich verantwortlich.

## Ist die Übermittlung in ein Drittland beabsichtigt?

Eine Übermittlung in ein Drittland ist nicht beabsichtigt.

## Wie lange werden Ihre Daten gespeichert?

Wir speichern Ihre personenbezogenen Daten, solange wie dies für die Entscheidung über Ihre Bewerbung erforderlich ist. Soweit ein Beschäftigungsverhältnis zwischen Ihnen und uns nicht zustande kommt, können wir darüber hinaus noch Daten weiter speichern, soweit dies zur Verteidigung gegen mögliche Rechtsansprüche erforderlich ist. Dabei werden die Bewerbungsunterlagen spätestens 12 Monate nach Bekanntgabe der Absageentscheidung gelöscht, sofern nicht eine längere Speicherung aufgrund von Rechtsstreitigkeiten erforderlich ist.

## Welche Rechte haben Sie?

Als Bewerber bei uns haben Sie je nach Situation im Einzelfall folgende Datenschutzrechte, zu deren Ausübung Sie unseren Datenschutzbeauftragten der Personalwerk Communications GmbH jederzeit unter der in Ziff. 12 genannten Daten kontaktieren können:

### Auskunft

Sie haben das Recht, Auskunft über Ihre bei uns verarbeiteten personenbezogenen Daten zu erhalten sowie Zugang zu Ihren personenbezogenen Daten und/oder Kopien dieser Daten zu verlangen. Dies schließt Auskünfte über den Zweck der Nutzung, die Kategorie der genutzten Daten, deren Empfänger und Zugriffsberechtigte sowie, falls möglich, die geplante Dauer der Datenspeicherung oder, falls dies nicht möglich ist, die Kriterien für die Festlegung dieser Dauer, ein;

### Berichtigung, Löschung oder Einschränkung der Verarbeitung

Sie haben das Recht, von uns unverzüglich die Berichtigung Sie betreffender unrichtiger personenbezogener Daten zu ver­langen. Unter Berücksichtigung der Zwecke der Verarbeitung haben Sie das Recht, die Vervollständigung unvollständiger personenbezogener Daten – auch mittels einer ergänzenden Erklärung – zu verlangen.

### Widerspruchsrecht

Soweit die Verarbeitung Sie betreffender personenbezogener Daten aufgrund von Art. 6 Abs. 1 Buchstabe f DSGVO erfolgt, haben Sie das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit Widerspruch gegen die Verarbeitung dieser Daten einzulegen. Wir verarbeiten diese personenbezogenen Daten dann nicht mehr, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.

### Widerrufsrecht

Wenn die Verarbeitung auf einer Einwilligung beruht, haben Sie das Recht, die Einwilligung jederzeit zu widerrufen, ohne dass die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung berührt wird. Hierzu kön­nen Sie uns oder unsere/n Datenschutzbeauftragten jederzeit unter den oben genannten Daten kontaktieren.

### Recht auf Löschung

Sie haben das Recht, von uns zu verlangen, dass Sie betreffende personenbezogene Daten unverzüglich gelöscht werden, und wir sind verpflichtet, personenbezogene Daten unverzüglich zu löschen, sofern einer der folgenden Gründe zutrifft:

- Die personenbezogenen Daten sind für die Zwecke, für die sie erhoben oder auf sonstige Weise verarbeitet wurden, nicht mehr notwendig
- Sie legen gemäß obiger Nummer 7.c Widerspruch gegen die Verarbeitung ein und es liegen keine vorrangigen berech­tigten Gründe für die Verarbeitung vor.
- Die personenbezogenen Daten wurden unrechtmäßig verarbeitet.
- Die Löschung der personenbezogenen Daten ist zur Erfüllung einer rechtlichen Verpflichtung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten erforderlich, dem wir unterliegen.

Dies gilt nicht, soweit die Verarbeitung erforderlich ist:

- zur Erfüllung einer rechtlichen Verpflichtung, die die Verarbeitung nach dem Recht der Union oder der Mitgliedstaaten, dem wir unterliegen, erfordert.
- zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.

### Recht auf Einschränkung der Verarbeitung

Sie haben das Recht, von uns die Einschränkung der Verarbeitung zu verlangen, wenn eine der folgenden Voraussetzungen gegeben ist:

- ie Richtigkeit der personenbezogenen Daten von Ihnen bestritten wird, und zwar für eine Dauer, die es uns ermög­licht, die Richtigkeit der personenbezogenen Daten zu überprüfen,
- die Verarbeitung unrechtmäßig ist und Sie die Löschung der personenbezogenen Daten ablehnen und stattdessen die Einschränkung der Nutzung der personenbezogenen Daten verlangen;
- wir die personenbezogenen Daten für die Zwecke der Verarbeitung nicht länger benötigen, Sie sie jedoch zur Geltend­machung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen, oder
- Sie Widerspruch gegen die Verarbeitung gemäß obiger Nummer 7.c eingelegt haben, solange noch nicht feststeht, ob unsere berechtigten Gründe gegenüber den Ihrigen überwiegen.

Wurde die Verarbeitung gemäß diesem Buchstaben f eingeschränkt, so dürfen diese personenbezogenen Daten – von ihrer Speicherung abgesehen – nur mit Ihrer Einwilligung oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person oder aus Gründen eines wichtigen öffentlichen Interesses der Union oder eines Mitgliedstaats verarbeitet werden.

Haben Sie eine Einschränkung der Verarbeitung erwirkt, werden wir Sie unterrichten, bevor die Einschränkung aufgehoben wird.

### Beschwerderecht

Sie haben unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs das Recht auf Be­schwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat Ihres Aufenthaltsorts, Ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbe­zogenen Daten gegen die DSGVO verstößt

## Verwendung von Cookies oder sitzungsspezifischen Informationen

Um die reibungslose Nutzung unseres Online-Bewerbermanagementsystems zu gewährleisten, verwenden wir standardmäßig Cookies. Cookies sind kleine Textdateien, die Ihr Browser auf Ihrem Rechner ablegt, damit wir Sie während eines Besuchs auf unseren Online-Bewerbungsseiten eindeutig identifizieren können. Nach dem Ende Ihres Besuchs auf unseren Online-Bewerbungsseiten werden die Cookies wieder gelöscht (sogenannte sitzungsspezifische Cookies). Es ist technisch weder möglich, Ihre Festplatte durch Cookies auszuspähen, noch wird durch den Einsatz von Cookies in anderer Weise das Datengeheimnis verletzt.

Es folgt eine Aufstellung der verwendeten Cookies:

| Bezeichung   | Art              | Zweck          | Beschreibung | Verantwortlicher |
|--------------|------------------|----------------|--------------|------------------|
| fe_typo_user | Session Cookie   | Funktionalität | Dieses Cookie dient der Identifizierung des Nutzers während der Benutzung der Bewerbermanagementsoftware „Homepagerecruiter“ auf dieser Domain. Das Cookie ist für die korrekte Funktionalität zwingend erforderlich. Das Cookie verliert beim Schließen des Browsers seine Gültigkeit. | Personalwerk Communications GmbH |
| atriger      | Permanent Cookie | Funktionalität | Dieses Cookie dient dazu, dass die Aufklappelemente im Bewerbungsformular der Bewerbermanagementsoftware „Homepagerecruiter“ auf dieser Domain dargestellt werden können. Das Cookie ist für die korrekte Funktionalität zwingend erforderlich. Das Cookie verliert nach 1 Tag  seine Gültigkeit. | Personalwerk Communications GmbH |

## Erforderlichkeit des Bereitstellens personenbezogener Daten

Die Bereitstellung personenbezogener Daten ist weder gesetzlich noch vertraglich vorgeschrieben, noch sind Sie verpflich­tet, die personenbezogenen Daten bereitzustellen. Allerdings ist die Bereitstellung personenbezogener Daten für einen Ver­tragsabschluss über ein Beschäftigungsverhältnis mit uns erforderlich. Das heißt, soweit Sie uns keine personenbezogenen Daten bei einer Bewerbung bereitstellen, werden wir kein Beschäftigungsverhältnis mit Ihnen eingehen.

## Keine automatisierte Entscheidungsfindung

Es findet keine automatisierte Entscheidung im Einzelfall im Sinne des Art. 22 DSGVO statt, das heißt, die Entscheidung über Ihre Bewerbung beruht nicht ausschließlich auf einer automatisierten Verarbeitung.

## Gültigkeit der Vereinbarung (salvatorische Klausel)

Von der Rechtswidrigkeit, Unwirksamkeit, Ungültigkeit oder Undurchführbarkeit von Teilen dieser Datenschutzbestimmungen und Nutzungsbedingungen bleiben die restlichen Bestimmungen in ihrer Wirksamkeit und Gültigkeit unberührt.

## Kontaktdaten des datenschutzrechtlich verantwortlichen Unternehmens

Zu allen mit der Verarbeitung ihrer personenbezogenen Daten und mit der Wahrnehmung ihrer Rechte gemäß der DSGVO im Zusammenhang stehenden Fragen im Rahmen des Bewerbungsverfahren können Sie unseren Datenschutzbeauftragten der Personalwerk Communications GmbH (Ronald Baranowski, SIX DATENSCHUTZ GmbH, Kasseler Straße 30, 61118 Bad Vilbel, T: +49 6101 982 94 22, E-Mail: datenschutz@personalwerk.de) zu Rate zie­hen.



[^1]: Aus Gründen der besseren Lesbarkeit wird auf die gleichzeitige Verwendung der Sprachformen männlich, weiblich und divers (m/w/d) verzichtet. Sämtliche Personenbezeichnungen gelten gleichermaßen für alle Geschlechter.
