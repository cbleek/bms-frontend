'use strict';

const salaryDefault = {
  min: 0,
  max: 0,
  currency: 'EUR',
  visible: true,
  intervall: 'year'
};
const currencyOptions = ['CHF', 'EUR', 'GBP', 'USD'];
const intervallOptions = ['hour', 'day', 'week', 'month', 'year'];

export {
  salaryDefault,
  currencyOptions,
  intervallOptions
}
