'use strict';

const name = 'education';

const options = [
  'school',
  'completedEducation',
  'bachelorStudent',
  'bachelorCompleted',
  'masterStudent',
  'masterCompleted',
  'phdCompleted'
];

export {
  options as educationOptions,
  name as educationName
}
