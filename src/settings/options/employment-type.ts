'use strict';

const name = 'employmentType';

const options = [
  'employee',
  'freelance',
  'contract',
  'trainee',
  'internship',
  'apprenticeship',
  'student',
  'sideJob'
];

export {
  options as employmentTypeOptions,
  name as employmentTypeName
}
