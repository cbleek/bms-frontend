'use strict';

const name = 'careerLevel';

const options = [
  'entry',
  'junior',
  'senior',
  'executive',
  'seniorExecutive'
];

export {
  options as careerLevelOptions,
  name as careerLevelName
}
