'use strict';

const name = 'workExperience';

const options = [
  'lessThanOneYear',
  'oneToThreeYears',
  'threeToFiveYears',
  'fiveToSevenYears',
  'moreThanSevenYears'
];

export {
  options as workExperienceOptions,
  name as workExperienceName
}
