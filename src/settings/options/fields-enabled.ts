'use strict';

const fieldsEnabled = {
  enableGender: false,
  enableFirstname: true,
  enableLastname: true,
  enableEmail: true,
  enablePhone: false
}

export {
  fieldsEnabled
}
