'use strict';

const name = 'category';

const options = [
  'Administrative/Office',
  'Business Development',
  'Business Intelligence/Data Science',
  'Cleaning/Facilities',
  'Construction'
];

export {
  options as categoryOptions,
  name as categoryName
}
