'use strict';

const name = 'contract';

const options = [
  'fulltime',
  'fulltimeTemporary',
  'parttime',
  'parttimeTemporary'
];

export {
  options as contractOptions,
  name as contractName
}
