'use strict';

import { candidateStatusOptions } from 'src/settings/candidateStatus'
import { ROLES }  from 'src/settings/roles';

export {
  candidateStatusOptions,
  ROLES
}

export * from 'src/settings/options/career-level'
export * from 'src/settings/options/category'
export * from 'src/settings/options/contract'
export * from 'src/settings/options/education'
export * from 'src/settings/options/employment-type'
export * from 'src/settings/options/work-experiences'
export * from 'src/settings/options/fields-enabled'
export * from 'src/settings/options/salary'

