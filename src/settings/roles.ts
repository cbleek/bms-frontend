'use strict';

const ROLES = {
  EMPLOYEE: 'employee',
  COMPANY_OWNER: 'company_owner',
  CANDIDATE: 'candidate'
};

export {
  ROLES
}

