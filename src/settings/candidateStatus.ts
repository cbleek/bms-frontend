'use strict';

import { matLooksOne, matLooksTwo, matLooks3, matLooks4, matLooks5, matCancel } from '@quasar/extras/material-icons';

const submenu = [
  {
    value: 'cancelAuto',
    label: 'cancelAutoCaption'
  },
  {
    value: 'cancelManual',
    label: 'cancelManualCaption'
  },
  {
    value: 'cancelSilent',
    label: 'cancelSilentCaption'
  }
];

// const states = [
//   {
//     'applied': {
//       label: 'status.applied',
//       color: 'grey-6',
//       icon: matLooksOne,
//     }
//   },
//   {
//     'underExamination': {
//       label: 'status.underExamination',
//       color: 'blue-3',
//       icon: matLooksTwo,
//     }
//   },
//   {
//     'screening': {
//       label: 'status.screening',
//       color: 'orange-3',
//       icon: matLooks3,
//     }
//   },
//   {
//     'interviews': {
//       label: 'status.interviews',
//       color: 'purple-3',
//       icon: matLooks4,
//     }
//   },
//   {
//     'testing': {
//       label: 'status.testing',
//       color: 'green-3',
//       icon: matLooks5,
//     }
//   },
//   {
//     'hired': {
//       label: 'status.hired',
//       icon: 'check',
//       separator: true,
//       color: 'green',
//     }
//   },
//   {
//     'canceled': {
//       label: 'status.canceled',
//       icon: matCancel,
//       color: 'red',
//       separator: true,
//       disable: true,
//       submenu,
//     }
//   }
// ]

const candidateStatusOptions = [
  {
    value: 'applied',
    label: 'status.applied',
    color: 'grey-6',
    icon: matLooksOne,
    draggable: false
  },
  {
    value: 'underExamination',
    label: 'status.underExamination',
    color: 'blue-3',
    icon: matLooksTwo,
    recruitmentEnabled: true,

    recruitmentPerms: {
      read: true,
      write: false,
      mail: false,
      delete: false
    },
    departmentEnabled: false,
    departmentPerms: {
      read: true,
      write: false,
      mail: false,
      delete: false
    },
    disabled: false,
    councilEnabled: false,
    draggable: true
  },
  {
    value: 'screening',
    label: 'status.screening',
    color: 'orange-3',
    icon: matLooks3,
    recruitmentEnabled: true,
    recruitmentPerms: {
      read: true,
      write: false,
      mail: false,
      delete: false
    },
    departmentEnabled: true,
    departmentPerms: {
      read: true,
      write: false,
      mail: false,
      delete: false
    },
    disabled: false,
    councilEnabled: false,
    draggable: true
  },
  {
    value: 'interviews',
    label: 'status.interviews',
    color: 'purple-3',
    icon: matLooks4,
    recruitmentEnabled: true,
    recruitmentPerms: {
      read: true,
      write: false,
      mail: false,
      delete: false
    },
    departmentEnabled: false,
    departmentPerms: {
      read: true,
      write: false,
      mail: false,
      delete: false
    },
    disabled: false,
    councilEnabled: false,
    draggable: true
  },
  {
    value: 'testing',
    label: 'status.testing',
    color: 'green-3',
    icon: matLooks5,
    recruitmentEnabled: true,
    recruitmentPerms: {
      read: true,
      write: false,
      mail: false,
      delete: false
    },
    departmentEnabled: false,
    departmentPerms: {
      read: true,
      write: false,
      mail: false,
      delete: false
    },
    disabled: false,
    councilEnabled: false,
    draggable: true
  },
  {
    value: 'hired',
    label: 'status.hired',
    icon: 'check',
    separator: true,
    color: 'green',
    disable: true,
    draggable: false
  },
  {
    value: 'canceled',
    label: 'status.canceled',
    icon: matCancel,
    color: 'red',
    separator: true,
    disable: true,
    draggable: false,
    submenu
  }
];

export { candidateStatusOptions };
