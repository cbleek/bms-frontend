import { i18n } from 'src/boot/i18n';

const t = i18n.global.t;

const required = v => (v && v.length > 0) || t('rules.required');

const validEmail = v => v
  ? /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(v)
        || t('rules.invalidEmail')
  : true;

export default {
  required,
  validEmail,
};
