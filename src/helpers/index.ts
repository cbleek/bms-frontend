import { PaginationState, PaginationProps, FetchParams, APIParams } from 'src/types';

// TODO: improve utility, param serializer

export const preparePagination = <T>({ paginator, sort }: PaginationState): PaginationProps<T> => ({
  page: paginator?.page ?? 1,
  rowsPerPage: paginator?.pageSize ?? 10,
  rowsNumber: paginator?.total ?? 1,
  sortBy: (sort?.split(':')[0] ?? 'id') as string & keyof T,
  descending: sort?.split(':')[1] === 'desc'
});

export const prepareParams = <T, TFilter extends object>(
  props: FetchParams<T, TFilter>,
  filterProps: (props: APIParams<T>, filter?: TFilter) => APIParams<T>
): APIParams<T> => {
  const { sortBy, descending, page = 1, rowsPerPage: pageSize = 10 } = props.pagination;
  const params: APIParams<T> = {
    'pagination[page]': page,
    'pagination[pageSize]': pageSize,
    ...(sortBy && { sort: `${sortBy}:${descending ? 'desc' : 'asc'}` })
  };

  if (!props.filter) return filterProps(params);
  return filterProps(Object.assign(params, 'q' in props.filter ? { _q: props.filter.q || null } : null), props.filter);
};
