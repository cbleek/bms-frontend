'use strict';

const routeName = {
  applyHeadless: 'apply',
  applyPremium: 'applyPremium',
  apply: 'jobApply',
  dashboard: 'dashboard',
  error404: 'error-404',
  candidates: 'candidates',
  jobHeadless: 'job',
  jobPremium: 'jobPremium',
  jobIntern: 'jobs',
  job: 'jobView',
  login: 'login'
}

export {
  routeName
};
