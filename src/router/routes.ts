import type { RouteRecordRaw } from 'vue-router';
import { h, resolveComponent } from 'vue';
import { routeName } from './names';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/AnonymousLayout.vue'),
    children: [
      {
        path: '',
        name: 'homepage',
        component: () => import('pages/public/HomePage.vue')
      },
      {
        path: 'reset-password',
        name: 'resetPassword',
        component: import('layouts/AnonymousLayout.vue')
      },
      {
        path: '/login',
        name: 'login',
        component: () => import('pages/public/HomePage.vue')
      },
      {
        path: 'public',
        children: [
          {
            path: 'companies',
            component: () => import('pages/public/CompanyListPage.vue')
          },
          {
            path: 'company/:id',
            component: () => import('pages/public/CompanyProfilePage.vue')
          },
          {
            path: 'for-companies',
            component: () => import('pages/public/RegisterCompanyPage.vue')
          },
          {
            path: 'magazine',
            component: () => import('pages/public/MagazinePage.vue')
          },
          {
            path: 'imprint',
            name: 'imprint',
            component: () => import('pages/public/ImprintPage.vue')
          },
          {
            path: 'privacy-policy',
            name: 'privacyPolicy',
            component: () => import('pages/public/PrivacyPolicyPage.vue')
          },
          {
            path: 'jobs/:id',
            name: routeName.job,
            component: () => import('pages/public/JobPosting.vue')
          },
          {
            path: 'jobs/:id/apply',
            name: routeName.apply,
            component: () => import('pages/public/JobApplyPage.vue')
          }
        ]
      }
    ],
  },
  {
    path: '/',
    component: () => import('layouts/AdminLayout.vue'),
    children: [
      {
        path: 'bms',
        name: 'bms',
        component: () => import('pages/admin/IndexPage.vue')
      },
      {
        path: 'login-admin',
        name: 'login-admin',
        component: () => import('pages/public/LoginAdminPage.vue')
      },
      {
        path: 'admin',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'admin',
            component: () => import('pages/admin/IndexPage.vue'),
            meta: { requiresAuth: false }
          },
          {
            path: 'customers',
            name: 'admin.customers',
            component: () => import('pages/admin/CustomersPage.vue')
          },
          {
            path: 'applicants',
            name: 'admin.applicants',
            component: () => import('pages/admin/ApplicantsPage.vue')
          },
          {
            path: 'messages',
            name: 'admin.messages',
            component: () => import('pages/admin/MessageTemplatePage.vue')
          },
          {
            path: 'messages/:id/edit',
            name: 'admin.messages.edit',
            component: async () => import('pages/admin/EditMessageTemplate.vue')
          }
        ]
      }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/CompanyLayout.vue'),
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('pages/personal/DashboardPage.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/profile',
        name: 'profile',
        component: () => import('pages/personal/UserProfile/UserProfilePage.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/team',
        name: 'team',
        component: () => import('pages/company/TeamManager.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/private/messages',
        component: () => import('pages/personal/MessagesPage.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/api',
        name: 'api',
        component: () => import('pages/ApiPage.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/company',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'company',
            component: () => import('pages/company/CompanyProfileForm.vue'),
          },
          {
            path: 'workflows',
            name: 'workflow',
            component: () => import('pages/company/WorkflowPage.vue'),
          },
          {
            path: 'widget',
            name: 'widget',
            component: () => import('pages/company/WidgetPage.vue'),
          },
          {
            path: 'jobs',
            name: 'jobs',
            component: () => import('pages/company/JobsListPage.vue')
          },
        ]
      },
      {
        path: '/jobs',
        component: {
          setup() {
            return () => h(resolveComponent('router-view'));
          }
        },
        meta: { requiresAuth: true },
      },
      {
        path: '/invoice',
        name: 'invoice',
        component: () => import('pages/InvoicePage.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/company/settings',
        name: 'settings',
        component: () => import('pages/company/SettingsPage.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/private/notifications',
        name: 'notifications',
        component: () => import('pages/personal/NotificationSettingsPage.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/messages',
        name: 'messages',
        component: () => import('pages/company/MessageTemplatesPage.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/candidates',
        name: routeName.candidates,
        component: () => import('pages/company/CandidatesList.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/candidate/:id',
        name: 'candidate',
        component: () => import('pages/company/CandidateDetail.vue'),
        meta: { requiresAuth: true }
      },
      {
        path: '/public/forms',
        component: {
          setup() {
            return () => h(resolveComponent('router-view'));
          }
        },
        children: [
          {
            path: '',
            name: 'public-forms',
            component: () => import('pages/ApplicationFormList.vue')
          },
          {
            path: ':id/preview',
            name: 'applicationFormPreview',
            component: () => import('pages/company/JobApplyPreview.vue')
          },
        ]
      },
      {
        path: '/confirm/:token',
        component: () => import('pages/company/ConfirmInvitation.vue')
      },
      {
        path: '/my',
        children: [
          {
            path: 'applications',
            component: () => import('pages/personal/MyApplicationsPage.vue')
          },
          {
            path: 'jobs',
            component: () => import('pages/personal/MyJobsPage.vue')
          },
          {
            path: 'cv',
            component: () => import('pages/personal/MyCvPage.vue')
          }
        ]
      },
    ]
  },
  {
    path: '/job',
    redirect: '/',
    component: () => import('layouts/JobLayout.vue'),
    children: [
      {
        path: ':id',
        name: routeName.jobHeadless,
        component: () => import('pages/public/JobPosting.vue')
      },
      {
        path: ':id/apply',
        name: routeName.applyHeadless,
        component: () => import('pages/public/JobApplyPage.vue')
      }
    ]
  },
  {
    path: '/premium-job',
    redirect: '/',
    component: () => import('layouts/JobPremiumLayout.vue'),
    children: [
      {
        path: ':id',
        name: routeName.jobPremium,
        component: () => import('pages/public/JobPosting.vue')
      },
      {
        path: ':id/apply',
        name: routeName.applyPremium,
        component: () => import('pages/public/JobApplyPage.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    name: 'error-404',
    component: () => import('pages/ErrorNotFound.vue')
  }
];

export default routes;
