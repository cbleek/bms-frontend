import { route } from 'quasar/wrappers';
import { useAuthStore } from 'stores/auth';
import { ROLES } from 'src/settings';
import { routeName } from './names';
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory,
} from 'vue-router';

import routes from './routes';

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route((/* { store, ssrContext } */) => {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
    ? createWebHistory
    : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });

  Router.beforeEach(async (to, from, next) => {
    const store = useAuthStore();
    let requiresAuth=true;
    if(to.meta && !to.meta.requiresAuth){
      requiresAuth=false;
    }
    if (
      // Make sure the user is authenticated
      !store.isLoggedIn &&
      requiresAuth &&
      // ❗️ Avoid an infinite redirect
      to.name !== routeName.login
    ) {
      // Redirect the user to the login page
      return next({ name: routeName.login })
    }
    const role=store.getRole
    if (role && role.type==ROLES.EMPLOYEE ) {
      if(!to.query.group && to.name == routeName.candidates){
        const query = {
          ...to.query,
          group: 'clerk'
        }
        next({ path: to.path, query: query });
      }else{
        next();
      }
    } else {
      next()
    }
  });

  return Router;
});
