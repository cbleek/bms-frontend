import { describe, it, expect } from 'vitest'
import { preparePagination } from 'src/helpers'

describe('preparePagination', () => {
  const paginationData = {
    paginator: {
      page: 2,
      pageSize: 10,
      total: 100,
    },
    sort: 'name:desc',
  };

  it('should return pagination data in the correct format', () => {
    const result = preparePagination(paginationData);

    expect(result).toEqual({
      page: 2,
      rowsPerPage: 10,
      sortBy: 'name',
      descending: true,
      rowsNumber: 100,
    });
  });

  it('should handle missing sort parameter correctly', () => {
    const paginationDataWithNoSort = {
      paginator: {
        page: 1,
        pageSize: 20,
        total: 50,
      },
      sort: 'name:asc',
    };

    const result = preparePagination(paginationDataWithNoSort);

    expect(result).toEqual({
      page: 1,
      rowsPerPage: 20,
      sortBy: 'name',
      descending: false,
      rowsNumber: 50,
    });
  });
});
