import { boot } from 'quasar/wrappers';
import { QSsrContext } from '@quasar/app-vite';
import { AxiosProvider, provideHttp } from 'src/core/lib/http';

declare module '@quasar/app-vite' {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface BootFileParams<TState> {
    ssrContext: QSsrContext;
  }
}
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $http: ReturnType<typeof AxiosProvider>['client'];
  }
}
declare module 'pinia' {
  export interface PiniaCustomProperties {
    $http: ReturnType<typeof AxiosProvider>['client'];
  }
}

export default boot(({ app, store, ssrContext }) => {
  // injecting the provider into app
  const $http = provideHttp(AxiosProvider(true, ssrContext), app);

  // making the client available via `this.$http` in options API & in data stores
  app.config.globalProperties.$http = $http;
  store.use(({ store }) => {
    store.$http = $http;
  });

  // TODO: upgrade pinia & pass client on store definitions for composition support
});
