import { boot } from 'quasar/wrappers';
import { createI18n, I18n } from 'vue-i18n';
import { nextTick, App } from 'vue';
import { useAuthStore } from 'stores/auth';
import deQ from 'quasar/lang/de-DE';
import enU from 'quasar/lang/en-US';
import frF from 'quasar/lang/fr';

import { Quasar } from 'quasar'

const langPacks = {
  'de-DE': deQ,
  'en-US': enU,
  'fr-FR': frF,
};
/**
 * Dynamically import the localization files and swap locales
 *
 * @param i18n
 * @param locale
 */
export const changeLocale = async (i18n: I18n<unknown, unknown, unknown, string, false>, locale: string) => {
  const { default: messages } = await import(`../i18n/locales/${locale.slice(0, 2)}/index.ts`)
  i18n.global.setLocaleMessage(locale, messages)
  i18n.global.locale.value = locale
  console.log('Local ', locale)
  Quasar.lang.set(langPacks[locale]);
  return nextTick()
}

/**
 * Returns an i18n plugin instance
 */
export const setupI18n = (globalInjection = true): I18n<unknown, unknown, unknown, string, false> => {
  return createI18n({
    legacy: false,
    silentTranslationWarn: true,
    fallbackLocale: 'en-US',
    warnHtmlInMessage: 'off',
    globalInjection
  })
}
export const i18n = setupI18n()

/**
 * Quasar boot loader
 */
export default boot(async ({ app }: { app: App }) => {
  const defaultLocale = 'de-DE'
  const { user, isLoggedIn } = useAuthStore()
  await changeLocale(i18n, isLoggedIn ? user?.ui_language || defaultLocale : defaultLocale)

  app.use(i18n)
})
