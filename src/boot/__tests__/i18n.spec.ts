import { describe, beforeAll, beforeEach, it, expect, vi, expectTypeOf } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import boot, { i18n, setupI18n, changeLocale } from 'boot/i18n';
import { useAuthStore } from 'stores/auth';
import { User } from 'src/types';

const setLocaleMessage = vi.fn();

describe('boot/i18n', () => {
  let authStore: ReturnType<typeof useAuthStore>;

  beforeAll(() => {
    setActivePinia(createPinia());
    authStore = useAuthStore();

    vi.mock('vue-i18n', () => ({
      createI18n: vi.fn(() => ({
        global: { locale: {}, setLocaleMessage: (...args: Array<unknown>) => setLocaleMessage(...args) }
      }))
    }));
  });

  beforeEach(() => {
    authStore.isLoggedIn = false;
    authStore.user = undefined;

    vi.clearAllMocks();
  });

  it('initializes localization', async () => {
    const appUseMock = vi.fn();
    await boot({ app: { use: appUseMock }} as never);

    expect(appUseMock).toHaveBeenCalledOnce();
    expect(appUseMock).toHaveBeenCalledWith(i18n);

    expect(setLocaleMessage).toHaveBeenCalledOnce();
    expect(i18n.global.locale).toEqual({ value: 'de-DE' });
  });

  it('initializes localization for user', async () => {
    authStore.isLoggedIn = true;
    authStore.user = { ui_language: 'fr-FR' } as User;

    await boot({ app: { use: vi.fn() }} as never);
    expect(i18n.global.locale).toEqual({ value: 'fr-FR' });
  });

  it('initializes localization for user with default language', async () => {
    authStore.isLoggedIn = true;

    await boot({ app: { use: vi.fn() }} as never);
    expect(i18n.global.locale).toEqual({ value: 'de-DE' });
  });

  it('updates language', async () => {
    vi.mock('../../i18n/locales/en/index.ts', () => ({
      default: { key: 'translation' }
    }));

    const isolatedI18n = setupI18n(false);

    expectTypeOf(isolatedI18n).toBeObject();
    expect(Object.keys(isolatedI18n.global.locale)).toHaveLength(0);

    await changeLocale(isolatedI18n, 'en-US');
    expect(setLocaleMessage).toHaveBeenCalledWith('en-US', { key: 'translation' });
    expect(isolatedI18n.global.locale).toEqual({ value: 'en-US' });
  });
});
