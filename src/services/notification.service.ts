import { Notify, QNotifyCreateOptions } from 'quasar';

export default class NotificationService {
  private readonly notify: Notify = Notify;

  constructor() {
    this.notify.setDefaults({
      position: 'bottom',
      textColor: 'white',
      attrs: {
        style: {
          'min-width': '350px',
        },
      },
    });
  }

  public showError(message: string, options: QNotifyCreateOptions = {}) {
    return this.notify.create({
      message,
      color: 'negative',
      attrs: {
        style: {
          'background-color': '#ff3d24 !important',
          'border-radius': '5px',
          'font-weight': '700',
        },
      },
      ...options,
    });
  }

  public showSuccess(message: string, options: QNotifyCreateOptions = {}) {
    return this.notify.create({
      message,
      color: 'positive',
      attrs: {
        style: {
          'background-color': '#00B241 !important',
          'border-radius': '5px',
          'font-weight': '700',
        },
      },
      ...options,
    });
  }
}
