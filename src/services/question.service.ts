// NOT IN USE

// class QuestionService {
//   private readonly ROUTES = {
//     CREATE_QUESTION: '/api/questions',
//     UPDATE_QUESTION: '/api/questions/:id',
//     DELETE_QUESTION: '/api/questions/:id',
//   }
//
//   public async createQuestion(params) {
//     return api.post(this.ROUTES.CREATE_QUESTION, params)
//   }
//
//   public async updateQuestion(params, id) {
//     const url = this.ROUTES.UPDATE_QUESTION.replace(':id', id)
//     return api.put(url, params)
//   }
//
//   public async deleteQuestion(id) {
//     const url = this.ROUTES.DELETE_QUESTION.replace(':id', id)
//     return api.delete(url)
//   }
// }
//
// export default QuestionService;
