// NOT IN USE

// import { defineStore } from 'pinia';
// import { preparePagination } from 'src/helpers';
// import { settingsService } from '../services/_singletons';
//
// export const useSettingStore = defineStore('settings', {
//   state: () => ({
//     isLoading: false,
//     setting: null,
//
//     settings: {
//       result: [],
//       sort: 'id:asc',
//       filter: undefined,
//       paginator: undefined
//     }
//   }),
//
//   getters: {
//     getCompany(state) {
//       return state.setting;
//     },
//
//     getPagination(state) {
//       return preparePagination(state.settings);
//     }
//   },
//
//   actions: {
//     // async fetchSetting(props) {
//     //   const id = props
//     //   const params = {
//     //     populate: '*',
//     //   };
//     //   try {
//     //     await settingsService.fetchSetting({ params }, id)
//     //       .then((response) => {
//     //         this.setting = response.data.data;
//     //       })
//     //   } catch (e) {
//     //     console.log(e);
//     //   }
//     // },
//
//     async fetchSettings() {
//       try {
//         await settingsService.fetchCompanySettings().then(response => {
//           this.settings.result = response.data;
//         });
//       } catch (e) {
//         console.log(e);
//       }
//     },
//
//     async updateSetting(setting) {
//       const id = setting.id;
//       delete setting.id;
//
//       try {
//         await settingsService.updateCompanySettings(
//           {
//             data: setting
//           },
//           id
//         );
//       } catch (e) {
//         console.log(e);
//       }
//     }
//
//     // setSettingsPagination() {
//     //   console.log() // noop
//     // }
//   },
//
//   persist: {
//     storage: localStorage,
//     paths: ['settings.result']
//   }
// });
