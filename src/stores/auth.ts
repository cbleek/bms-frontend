import { defineStore } from 'pinia';
import { i18n, changeLocale } from 'boot/i18n';
import { useAuthService, useEmployeeService, useMessageService } from 'src/core/services';
import { preparePagination, prepareParams } from 'src/helpers';
import {
  ContentTypeID,
  MessageType,
  MessageTypeFilter,
  MessageTypeResponse,
  UserType,
  UserTypeResponse
} from 'src/core/types';
import { StoredData, Nullable, FetchParams, APIParams } from 'src/types';
import { HttpError } from 'src/core/lib/http';

const setUserData = (
  state: AuthState,
  data: { jwt?: string; refresh_token?: string; user?: Partial<UserType> },
  context = ''
) => {
  state.isLoggedIn = true;
  state.user = data.user;
  state.token = data.jwt;
  state.refresh_token = data.refresh_token;
  state.passwordlessLoginContext = context;
};

const userLoggedInSuccess = (store: ReturnType<typeof useAuthStore>) => {
  // check if user has a preferred language set
  if (store.user?.ui_language) store.switchLanguage(store.user?.ui_language);

  // TODO: make notification "welcome back .." wroking again. Uncommenting next line will break tests.
  // notifyLoginSuccess(`${this.user.firstname + ' ' + this.user.lastname}`);

  // redirect
  // TODO: test user data, fix typing
  if (store.user?.role?.type != 'candidate')
    // TODO: fix layouts for router redirects
    store.$router.push({ name: store.user?.ui_startpage || 'dashboard' }).then(() => store.$router.go(0));
};

const updateUserData = (state: AuthState, data: Partial<UserType>) => {
  if (!state.user) return (state.user = data);
  Object.assign(state.user, data);
};

type AuthState = {
  isLoading: boolean;
  isLoggedIn: boolean;

  user: Nullable<Partial<UserType>>;
  token: Nullable<string>;
  refresh_token: Nullable<string>;
  passwordlessLoginContext: Nullable<string>;

  users: StoredData<UserTypeResponse>;
  messages: StoredData<MessageTypeResponse>;
  lang: string
};

export const useAuthStore = defineStore('auth', {
  state: (): AuthState => ({
    isLoggedIn: false,
    isLoading: false,

    user: null,
    token: null,
    refresh_token: null,
    passwordlessLoginContext: null,

    users: {
      result: [],
      sort: 'id:asc',
      filter: undefined,
      paginator: undefined
    },

    messages: {
      result: [],
      sort: 'id:asc',
      filter: undefined,
      paginator: undefined
    },
    lang: 'de-DE'
  }),

  getters: {
    getMessagesPagination(state) {
      const { sort, paginator } = state.messages;
      return preparePagination<MessageType>({ sort, paginator });
    },

    getRole(state) {
      return state.user?.role;
    }
  },

  actions: {
    switchLanguage(lang: string) {
      this.lang = lang;
      if (this.isLoggedIn && this.user) this.user.ui_language = lang;
      return changeLocale(i18n, lang);
    },

    async loginUser(identifier: string, password: string) {
      const authService = useAuthService(this.$http);

      return authService
        .loginUser({ identifier, password })
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          setUserData(this, r.data);
          userLoggedInSuccess(this);
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        });
    },

    async loginAdmin(email: string, password: string) {
      const authService = useAuthService(this.$http);

      return authService
        .loginAdmin({ email, password })
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          const { user, token: jwt } = r.data.data;
          setUserData(this, { user, jwt });

          this.$router.push({ path: '/admin' }).then(() => this.$router.go(0));
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        });
    },

    async loginWithToken(loginToken: string) {
      const authService = useAuthService(this.$http);

      return authService
        .loginWithToken(loginToken)
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          const { user, jwt, context } = r.data;
          setUserData(this, { user, jwt }, context);

          // check if user has a preferred language set
          if (this.user?.ui_language) this.switchLanguage(this.user?.ui_language);
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        });
    },

    async registerUser({ username, email, password }: Record<'username' | 'email' | 'password', string>) {
      const authService = useAuthService(this.$http);

      return authService.registerUser({ username, email, password }).catch((error: HttpError | Error) => {
        if ('response' in error) throw new Error(error.response?.data.error.message);
        throw error;
      });
    },

    async forgotPassword(email: string) {
      const authService = useAuthService(this.$http);

      return authService.forgotPassword(email).catch((error: HttpError | Error) => {
        if ('response' in error) throw new Error(error.response?.data.error.message);
        throw error;
      });
    },

    async resetPassword(password: string, passwordConfirmation: string, code: string) {
      const authService = useAuthService(this.$http);

      return authService
        .resetPassword({ password, passwordConfirmation, code })
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          setUserData(this, r.data);
          userLoggedInSuccess(this);
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        });
    },

    async refresh() {
      const authService = useAuthService(this.$http);

      return authService
        .refreshToken(this.refresh_token)
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          this.token = r.data.jwt;
          this.refresh_token = r.data.refresh_token;
          return r.data;
        })
        .catch((error: HttpError | Error) => {
          if (!('response' in error)) throw error;
          if (error.response?.status === 400) return this.logout();
          throw new Error(error.response?.data.error.message);
        });
    },

    logout() {
      this.$reset();
      this.$router.push({ name: 'login' }).then(() => this.$router.go(0));
    },

    async fetchMessages(props: FetchParams<MessageType, MessageTypeFilter>) {
      const messageService = useMessageService(this.$http);

      const params = prepareParams(props, (params, filter) =>
        Object.assign({}, params, {
          populate: ['attachments', 'to.photo', 'from.photo', 'candidateTo.photo', 'candidateFrom.photo'],
          ...(filter?.box && { box: filter.box })
        })
      );

      this.isLoading = true;
      return messageService
        .getAll(params)
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          this.messages.result = r.data.data;
          if (r.data.meta) this.messages.paginator = r.data.meta.pagination;
          if (params.sort) this.messages.sort = params.sort;
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        })
        .finally(() => (this.isLoading = false));
    },

    async deleteMessage(id: ContentTypeID) {
      const messageService = useMessageService(this.$http);

      return messageService
        .delete(id)
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          return r.data.data;
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        });
    },

    async me(params?: APIParams<UserType>) {
      const authService = useAuthService(this.$http);

      return authService
        .profileUser({
          populate: params?.populate ?? '*',
          ...(params?.fields && { fields: params.fields })
        })
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          const { messagesSent: _, ...data } = r.data;
          updateUserData(this, {
            ...data
          });
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        });
    },

    async updateProfile(data: Partial<UserType>, id?: ContentTypeID) {
      const userId = id ?? this.user?.id;
      if (!userId) return;

      const authService = useAuthService(this.$http);

      return authService
        .updateUser(userId, data)
        .then(r => {
          // do nothing if ids do not match
          if (userId !== this.user?.id) return;

          if (!r.data) throw new Error(r.error.message);

          // TODO: change store to setup mode instead so we can watch user changes and trigger updates
          const { ui_language } = r.data;
          if (ui_language && this.user?.ui_language !== ui_language) this.switchLanguage(ui_language);

          updateUserData(this, r.data);
        })
        .catch(error => console.error(error));
    },

    async updatePassword(password: string) {
      const employeeService = useEmployeeService(this.$http);

      this.isLoading = true;
      return employeeService.updatePassword(password).finally(() => (this.isLoading = false));
    }
  },

  persist: {
    storage: localStorage,
    paths: ['token', 'refresh_token', 'user', 'isLoggedIn']
  }
});
