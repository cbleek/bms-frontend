import { defineStore } from 'pinia';
import { useDepartmentService } from 'src/core/services';
import { preparePagination } from 'src/helpers';

export const useDepartmentStore = defineStore('departmentStore', {
  state: () => ({
    isLoading: false,
    department: {
      id: null,
      user: {},
      company: {}
    },
    departments: {
      result: [],
      filter: '',
      sort: 'id:asc',
      paginator: {
        page: undefined,
        pageSize: undefined,
        pageCount: undefined,
        total: undefined
      }
    }
  }),

  getters: {
    getPagination(state) {
      return preparePagination(state.departments);
    }
  },

  actions: {
    async fetchDepartments(props: { filter: string; pagination }) {
      this.isLoading = true;
      const { sortBy, descending, page, rowsPerPage } = props.pagination;
      const params = {
        'pagination[page]': page ?? 1,
        'pagination[pageSize]': rowsPerPage ?? 10,
        populate: ['company', 'department', 'user.photo', 'user.role']
      };
      if (sortBy) {
        params.sort = `${sortBy}:${descending ? 'desc' : 'asc'}`;
      }

      if (props.filter && typeof props.filter.valueOf() === 'string') {
        params._q = props.filter;
      }

      const departmentService = useDepartmentService(this.$http);

      await departmentService
        .getAll(params)
        .then(response => {
          this.departments.result = response.data.data;
          this.departments.paginator = { ...response.data.meta.pagination };
          if (params.sort) this.departments.sort = params.sort;
        })
        .catch(e => {
          console.log(e);
        })
        .finally(() => {
          this.isLoading = false;
        });
    }
  },
  persist: false
});
