import { store } from 'quasar/wrappers';
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';
import { markRaw } from 'vue';
import router from '../router';
import { Router } from 'vue-router';

declare module 'pinia' {
  export interface PiniaCustomProperties {
      $router: Router
  }
}
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store((/* { ssrContext } */) => {
  const pinia = createPinia();

  // You can add Pinia plugins here
  pinia.use(piniaPluginPersistedstate);
  pinia.use(({ store }) => {
    store.$router = markRaw(router())
  })

  return pinia;
});

export * from './admin';
export * from './auth';
export * from './candidate';
export * from './company';
export * from './department';
export * from './employee';
export * from './event';
export * from './form';
export * from './job';
// export * from './question';
// export * from './settings';
export * from './files';
export * from './workflow';
export * from './answer';
