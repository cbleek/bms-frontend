import { beforeAll, beforeEach, describe, expect, it, vi } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { faker } from '@faker-js/faker';

import { ref } from 'vue';
import { useCandidateStore } from 'stores/candidate';
import { HttpClient } from 'src/core/lib/http';

const httpClient = { post: vi.fn(), patch: vi.fn(), put: vi.fn(), get: vi.fn(), delete: vi.fn() };

describe('stores/candidate', () => {
  let store: ReturnType<typeof useCandidateStore>;

  faker.setLocale('en_US');
  const generateData = (id?: number) => ({
    ...(id && { id }),
    firstname: faker.name.firstName(),
    lastname: faker.name.lastName(),
    phone: faker.phone.number(),
    email: faker.internet.email(),
    city: faker.address.city(),
    postalcode: faker.address.zipCode(),
    street: faker.address.street()
  });
  const candidatesArray = [...new Array(10)].map((v, i) => ({ ...generateData(i + 1) }));

  const populateCandidate = [
    'photo',
    'attachments',
    'recruiter_users.photo',
    'job.company.logo',
    'job.company.header',
    'job.workflow',
    'job.departmentStaff.photo',
    'job.recruiters.photo',
    'reviews',
    'applicant'
  ];

  beforeAll(() => {
    setActivePinia(createPinia());
    store = useCandidateStore();
    store.$http = ref<HttpClient<unknown>>(httpClient);
  });

  beforeEach(() => {
    vi.clearAllMocks();
  });

  it('initializes store', () => {
    expect(store).toBeTypeOf('object');
    expect(store.isLoading).toEqual(false);
    expect(store.candidate).toBeNull();
  });

  it('stores new candidate', async () => {
    const candidateData = generateData();
    const apiPOST = vi.spyOn(httpClient, 'post').mockImplementation(() =>
      Promise.resolve({
        data: { data: { id: 55, attributes: candidateData }}
      })
    );

    await store.save(candidateData);
    expect(apiPOST).toHaveBeenCalledOnce();
    expect(apiPOST).toHaveBeenCalledWith('/api/candidates', { data: candidateData }, undefined);
    expect(store.candidate).toEqual({ ...candidateData, id: 55 });
  });

  it('updates a candidate', async () => {
    const candidateId = 84;
    const newCandidateData = generateData();
    const apiPUT = vi.spyOn(httpClient, 'put').mockImplementation(() =>
      Promise.resolve({
        data: { data: { id: 84, attributes: newCandidateData }}
      })
    );

    await store.save(newCandidateData, 84);
    expect(apiPUT).toHaveBeenCalledOnce();
    expect(apiPUT).toHaveBeenCalledWith(
      `/api/candidates/${candidateId}`,
      {
        data: { ...newCandidateData, id: undefined }
      },
      undefined
    );
    expect(store.candidate?.email).toEqual(newCandidateData.email);
  });

  it('retrieves all candidates', async () => {
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation(() =>
      Promise.resolve({
        data: { data: candidatesArray, meta: { pagination: { page: 1, pageSize: 10, total: 10 }}}
      })
    );
    const fetchParams = { status: true, pagination: { page: 1, rowsPerPage: 10 }};

    await store.fetchCandidates(fetchParams);
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith('/api/candidates', {
      params: {
        'pagination[page]': fetchParams.pagination.page,
        'pagination[pageSize]': fetchParams.pagination.rowsPerPage,
        populate: populateCandidate
      }
    });

    expect(store.candidates.result).toHaveLength(10);
    expect(store.candidates.result).toStrictEqual(candidatesArray);
  });

  it('retrieves candidates with pagination', async () => {
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation((_, config) => {
      const { pagination } = config?.params ?? { pagination: { page: 0, pageSize: 0 }};
      return Promise.resolve({
        data: { data: [], meta: { pagination: { ...pagination, pageCount: 1, total: 22 }}}
      });
    });

    await store.fetchCandidates({
      status: true,
      pagination: { page: 1, rowsPerPage: 5, sortBy: 'updatedAt', descending: false }
    });
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith('/api/candidates', {
      params: {
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate: populateCandidate,
        sort: 'updatedAt:asc'
      }
    });
    expect(store.getPagination).toEqual({
      descending: false,
      page: 1,
      rowsNumber: 22,
      rowsPerPage: 10,
      sortBy: 'updatedAt'
    });

    await store.fetchCandidates({
      status: true,
      pagination: { page: 2, rowsPerPage: 5, sortBy: 'updatedAt', descending: false }
    });
    expect(apiGET).toHaveBeenCalledTimes(2);
    expect(apiGET).toHaveBeenCalledWith('/api/candidates', {
      params: {
        'pagination[page]': 2,
        'pagination[pageSize]': 5,
        populate: populateCandidate,
        sort: 'updatedAt:asc'
      }
    });
    expect(store.getPagination).toEqual({
      descending: false,
      page: 1,
      rowsNumber: 22,
      rowsPerPage: 10,
      sortBy: 'updatedAt'
    });

    await store.fetchCandidates({
      status: true,
      pagination: { page: 3, rowsPerPage: 8, sortBy: 'id', descending: true }
    });
    expect(apiGET).toHaveBeenCalledTimes(3);
    expect(apiGET).toHaveBeenCalledWith('/api/candidates', {
      params: {
        'pagination[page]': 3,
        'pagination[pageSize]': 8,
        populate: populateCandidate,
        sort: 'id:desc'
      }
    });
    expect(store.getPagination).toEqual({
      descending: true,
      page: 1,
      rowsNumber: 22,
      rowsPerPage: 10,
      sortBy: 'id'
    });
  });

  it('retrieves candidates with filters', async () => {
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation(() =>
      Promise.resolve({
        data: { data: [], meta: { pagination: {}}}
      })
    );

    const q = faker.name.fullName();
    await store.fetchCandidates({ status: true, pagination: { page: 1, rowsPerPage: 5 }, filter: { q }});
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith('/api/candidates', {
      params: {
        _q: q,
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate: populateCandidate
      }
    });

    const value = faker.name.jobTitle();
    await store.fetchCandidates({ status: true, pagination: { page: 1, rowsPerPage: 5 }, filter: { job: value }});
    expect(apiGET).toHaveBeenCalledTimes(2);
    expect(apiGET).toHaveBeenCalledWith('/api/candidates', {
      params: {
        ['filters[job]']: value,
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate: populateCandidate
      }
    });

    vi.mock('stores/auth', () => ({
      useAuthStore: () => ({ user: { id: 36 }})
    }));
    await store.fetchCandidates({
      status: true,
      pagination: { page: 1, rowsPerPage: 5 },
      filter: { recruiter: 'own' }
    });
    expect(apiGET).toHaveBeenCalledTimes(3);
    expect(apiGET).toHaveBeenCalledWith('/api/candidates', {
      params: {
        me: 1,
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate: populateCandidate
      }
    });

    await store.fetchCandidates({
      status: true,
      pagination: { page: 1, rowsPerPage: 5 },
      filter: { recruiter: 'new' }
    });
    expect(apiGET).toHaveBeenCalledTimes(4);
    expect(apiGET).toHaveBeenCalledWith('/api/candidates', {
      params: {
        me: 1,
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate: populateCandidate
      }
    });
  });

  it('retrieves candidate by id', async () => {
    const candidateId = 15;
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation(() =>
      Promise.resolve({
        data: { data: generateData(candidateId) }
      })
    );

    await store.fetchCandidate(candidateId);
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith(`/api/candidates/${candidateId}`, {
      params: {
        populate: [
          'photo',
          'reviews.recruiter.photo',
          'events',
          'applicant',
          'attachments',
          'recruiter_users.photo',
          'applicant.messages_sent',
          'answers.question'
        ]
      }
    });
    expect(store?.candidate?.id).toEqual(candidateId);
  });

  it('invite candidate', async () => {
    const candidate = { email: 'test@example.com', id: 15 };
    const apiPOST = vi.spyOn(httpClient, 'post').mockImplementation(() =>
      Promise.resolve({
        data: {
          dara: candidate
        }
      })
    );

    await store.invite(candidate);
    expect(apiPOST).toHaveBeenCalledOnce();

    expect(store?.candidate?.id).toEqual(candidate.id);
  });
});
