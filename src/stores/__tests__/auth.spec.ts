import { beforeAll, beforeEach, describe, expect, it, vi } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { faker } from '@faker-js/faker';

import { ref } from 'vue';
import { useAuthStore } from 'stores/auth';
import { HttpClient, HttpResponse, HttpResponseError } from 'src/core/lib/http';

const httpClient = { post: vi.fn(), patch: vi.fn(), put: vi.fn(), get: vi.fn(), delete: vi.fn() };
const router = { push: vi.fn(() => Promise.resolve()), go: vi.fn() };
const changeLocale = vi.fn().mockImplementation((...args: Array<unknown>) => args[1]);

describe('stores/auth', () => {
  let store: ReturnType<typeof useAuthStore>;

  type UserTokens<R, T> = (T extends true ? { token: string } : { jwt: string }) &
    (R extends true ? { refresh_token: string } : object);
  const generateTokens = <R extends boolean, T extends boolean>(refresh?: R, asAdmin?: T): UserTokens<R, T> =>
    ({
      [asAdmin === true ? 'token' : 'jwt']: faker.datatype.string(25),
      ...(refresh === true && { refresh_token: faker.datatype.string(10) })
    } as UserTokens<R, T>);

  const generateError = (status: number, name?: string, message?: string) => ({
    response: {
      status,
      data: {
        data: null,
        error: {
          status,
          name: name || 'Error',
          message: message || 'Something went wrong',
          details: {}
        }
      }
    }
  });

  faker.setLocale('en_US');
  const generateData = (id?: number) => ({
    ...(id && { id }),
    firstname: faker.name.firstName(),
    lastname: faker.name.lastName(),
    username: faker.internet.userName(),
    email: faker.internet.email(),
    ui_display: null,
    ui_startpage: null,
    ui_timezone: null,
    ui_language: faker.random.locale(),
    createdAt: faker.datatype.datetime(),
    updatedAt: faker.datatype.datetime(),

    photo: {
      id: faker.datatype.number(),
      width: 286,
      height: 432,
      ext: 'png',
      url: '/uploads/random-test_f607454c4e.png',
      createdAt: faker.datatype.datetime(),
      updatedAt: faker.datatype.datetime(),

      formats: {
        thumbnail: {
          width: 103,
          height: 156,
          ext: 'png',
          url: '/uploads/thumbnail_random-test_f607454c4e.png'
        }
      }
    },

    company: {
      id: faker.datatype.number(),
      description: faker.company.catchPhrase(),
      homepage: faker.internet.url(),
      industry: faker.company.bsNoun(),
      name: faker.company.name(),
      size: '2-10',
      careersiteEnabled: false,
      createdAt: faker.datatype.datetime(),
      updatedAt: faker.datatype.datetime()
    },

    confirmed: true
  });
  const messagesArray = [...new Array(10)].map((v, i) => ({
    id: i + 1,
    message: faker.lorem.sentence(10),
    subject: faker.lorem.sentence(4),
    createdAt: faker.datatype.datetime(),
    updatedAt: faker.datatype.datetime(),

    to: {
      data: [{ id: faker.datatype.number(), attributes: generateData() }]
    },
    from: {
      data: [{ id: faker.datatype.number(), attributes: generateData() }]
    }
  }));

  type ReturnMethods<T> = keyof { [K in keyof T as T[K] extends (...args: never[]) => unknown ? K : never]: T[K] };
  const generateFailTest =
    (
      expected: () => Promise<unknown | HttpResponse<unknown> | HttpResponseError<unknown>>,
      defaultError: string,
      responseError: ReturnType<typeof generateError>,
      methodName: ReturnMethods<typeof httpClient> = 'post'
    ) =>
    async () => {
      const apiPost = vi.spyOn(httpClient, methodName);

      apiPost.mockRejectedValue(new Error(defaultError));
      await expect(expected).rejects.toThrowError(defaultError);

      apiPost.mockReset();
      apiPost.mockRejectedValue(responseError);
      await expect(expected).rejects.toThrowError(responseError.response.data.error.message);
    };

  beforeAll(() => {
    setActivePinia(createPinia());
    store = useAuthStore();
    store.$http = ref<HttpClient<unknown>>(httpClient);
    store.$router = router as never;

    vi.mock('boot/i18n', async () => ({
      i18n: null,
      changeLocale: vi.fn((...args: Array<unknown>) => changeLocale(...args))
    }));
  });

  beforeEach(() => {
    store.$reset();
    vi.clearAllMocks();
  });

  it('initializes store', () => {
    expect(store).toBeTypeOf('object');
    expect(store.isLoggedIn).toEqual(false);
    expect(store.user).toBeNull();
  });

  it('switches language', async () => {
    await store.switchLanguage('de-DE');
    expect(changeLocale).toHaveBeenCalledOnce();
    expect(changeLocale).toHaveReturnedWith('de-DE');
  });

  it('logs user in', async () => {
    const userData = generateData(44);
    vi.spyOn(httpClient, 'post').mockImplementation(() =>
      Promise.resolve({
        data: {
          ...generateTokens(),
          user: userData
        }
      })
    );

    await store.loginUser('username', 'password');
    expect(store.isLoggedIn).toBeTruthy();
    expect(store.user).toEqual(userData);
    expect(changeLocale).toHaveBeenCalledWith(null, userData.ui_language);
    expect(router.push).toHaveBeenCalledWith({ name: 'dashboard' });
  });

  it(
    'fails to log user in',
    generateFailTest(
      () => store.loginUser('username', 'password'),
      'something went wrong',
      generateError(400, 'ValidationError', 'Invalid identifier or password 3')
    )
  );

  it('logs user as admin', async () => {
    const userData = generateData(33);
    vi.spyOn(httpClient, 'post').mockImplementation(() =>
      Promise.resolve({
        data: {
          data: {
            ...generateTokens(false, true),
            user: userData
          }
        }
      })
    );

    await store.loginAdmin('email', 'password');
    expect(store.isLoggedIn).toBeTruthy();
    expect(store.user).toEqual(userData);
    expect(router.push).toHaveBeenCalledWith({ path: '/admin' });
  });

  it(
    'fails to log user as admin',
    generateFailTest(
      () => store.loginAdmin('email', 'password'),
      'something went wrong',
      generateError(400, 'ApplicationError', 'Invalid credentials')
    )
  );

  it('logs user in with token', async () => {
    const userData = generateData(44);
    vi.spyOn(httpClient, 'get').mockImplementation(() =>
      Promise.resolve({
        data: {
          ...generateTokens(false),
          user: userData,
          context: ''
        }
      })
    );

    await store.loginWithToken('token');
    expect(store.isLoggedIn).toBeTruthy();
    expect(store.user).toEqual(userData);
    expect(changeLocale).toHaveBeenCalledWith(null, userData.ui_language);
  });

  it(
    'fails to log user in with token',
    generateFailTest(
      () => store.loginWithToken('token'),
      'something went wrong',
      generateError(400, 'BadRequestError', 'token.invalid'),
      'get'
    )
  );

  it(
    'fails to register user',
    generateFailTest(
      () => store.registerUser({ username: 'username', email: 'email', password: 'password' }),
      'something went wrong',
      generateError(400, 'ApplicationError', 'Invalid credentials')
    )
  );

  it(
    'fails to trigger forgotten password',
    generateFailTest(
      () => store.forgotPassword('email'),
      'something went wrong',
      generateError(403, 'ForbiddenError', 'Forbidden')
    )
  );

  it('logs the user out', () => {
    // TODO: fix relationship state
    store.user = generateData(22);
    store.isLoggedIn = true;

    store.logout();
    expect(store.isLoggedIn).not.toBeTruthy();
    expect(store.user).toBeNull();
    expect(router.push).toHaveBeenCalledWith({ name: 'login' });
  });

  it('refreshes user tokens', async () => {
    const { jwt, refresh_token } = generateTokens(true, false);
    const apiPOST = vi.spyOn(httpClient, 'post').mockImplementation(() =>
      Promise.resolve({
        data: {
          ...generateTokens(true)
        }
      })
    );

    store.user = generateData(22);
    store.isLoggedIn = true;
    store.token = jwt;
    store.refresh_token = refresh_token;

    await store.refresh();
    expect(apiPOST).toHaveBeenCalledWith('/api/token/refresh', { refresh_token: refresh_token }, undefined);
    expect(store.token).not.toEqual(jwt);
    expect(store.refresh_token).not.toEqual(refresh_token);
  });

  it(
    'fails to refresh user tokens',
    generateFailTest(() => store.refresh(), 'something went wrong', generateError(403, 'ForbiddenError', 'Forbidden'))
  );

  it('fails to refresh expired user tokens', async () => {
    vi.spyOn(httpClient, 'post').mockRejectedValue(generateError(400, 'BadRequestError', 'No Authorization'));

    store.user = generateData(22);
    store.isLoggedIn = true;

    await store.refresh();
    expect(store.isLoggedIn).not.toBeTruthy();
    expect(store.user).toBeNull();
    expect(router.push).toHaveBeenCalledWith({ name: 'login' });
  });

  it('retrieves all user messages', async () => {
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation(() =>
      Promise.resolve({
        data: { data: messagesArray, meta: { pagination: { page: 1, pageSize: 10, total: 10 }}}
      })
    );
    const fetchParams = { pagination: { page: 1, rowsPerPage: 10 }};

    await store.fetchMessages(fetchParams);
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith('/api/messages', {
      params: {
        'pagination[page]': fetchParams.pagination.page,
        'pagination[pageSize]': fetchParams.pagination.rowsPerPage,
        populate: ['attachments', 'to.photo', 'from.photo', 'candidateTo.photo', 'candidateFrom.photo']
      }
    });

    expect(store.messages.result).toHaveLength(10);
    expect(store.messages.result).toStrictEqual(messagesArray);
  });

  it('retrieves user messages with pagination', async () => {
    const populate = ['attachments', 'to.photo', 'from.photo', 'candidateTo.photo', 'candidateFrom.photo'];
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation((_, config) => {
      const { pagination } = config?.params ?? { pagination: { page: 0, pageSize: 0 }};
      return Promise.resolve({
        data: { data: [], meta: { pagination: { ...pagination, pageCount: 1 }}}
      });
    });

    await store.fetchMessages({
      pagination: { page: 1, rowsPerPage: 5, sortBy: 'updatedAt', descending: false }
    });
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith('/api/messages', {
      params: {
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        sort: 'updatedAt:asc',
        populate
      }
    });
    expect(store.getMessagesPagination).toEqual({
      descending: false,
      page: 1,
      rowsNumber: 1,
      rowsPerPage: 10,
      sortBy: 'updatedAt'
    });

    await store.fetchMessages({
      pagination: { page: 3, rowsPerPage: 8, sortBy: 'id', descending: true }
    });
    expect(apiGET).toHaveBeenCalledTimes(2);
    expect(apiGET).toHaveBeenCalledWith('/api/messages', {
      params: {
        'pagination[page]': 3,
        'pagination[pageSize]': 8,
        sort: 'id:desc',
        populate
      }
    });
    expect(store.getMessagesPagination).toEqual({
      descending: true,
      page: 1,
      rowsNumber: 1,
      rowsPerPage: 10,
      sortBy: 'id'
    });

    await store.fetchMessages({
      pagination: { page: 2, rowsPerPage: 5, descending: true }
    });
    expect(apiGET).toHaveBeenCalledTimes(3);
    expect(apiGET).toHaveBeenCalledWith('/api/messages', {
      params: {
        'pagination[page]': 2,
        'pagination[pageSize]': 5,
        populate
      }
    });
    expect(store.getMessagesPagination).toEqual({
      descending: true,
      page: 1,
      rowsNumber: 1,
      rowsPerPage: 10,
      sortBy: 'id'
    });
  });

  it('retrieves user messages with filters', async () => {
    const populate = ['attachments', 'to.photo', 'from.photo', 'candidateTo.photo', 'candidateFrom.photo'];
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation(() =>
      Promise.resolve({
        data: { data: [], meta: { pagination: {}}}
      })
    );

    const q = faker.name.fullName();
    await store.fetchMessages({ pagination: { page: 1, rowsPerPage: 5 }, filter: { q }});
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith('/api/messages', {
      params: {
        _q: q,
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate
      }
    });

    await store.fetchMessages({ pagination: { page: 1, rowsPerPage: 5 }, filter: { box: 'inbox' }});
    expect(apiGET).toHaveBeenCalledTimes(2);
    expect(apiGET).toHaveBeenCalledWith('/api/messages', {
      params: {
        ['box']: 'inbox',
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate
      }
    });
  });

  it('refreshes user data', async () => {
    const user = generateData(15);
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation((_, __) =>
      Promise.resolve({
        data: {
          ...user,
          jobs: [],
          candidates: [],
          messagesReceived: [],
          messagesSent: []
        }
      })
    );

    expect(store.user).toBeNull();

    await store.me();
    expect(apiGET).toHaveBeenCalledWith('/api/users/me', { params: { populate: '*' }});
    expect(store.user).not.toBeNull();
    expect(store.user?.messagesReceived).toEqual([]);
    expect(Object.keys(store.user ?? {})).not.toContain('messagesSent');
  });

  it('refreshes user data with populated fields', async () => {
    const getFields = (fields: Array<string>) => fields.reduce((config, field) => ({ ...config, [field]: []}), {});
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation((_, config) =>
      Promise.resolve({
        data: {
          ...generateData(15),
          ...(config?.params?.populate && config?.params?.fields && getFields(config.params.fields))
        }
      })
    );
    store.user = generateData(15);

    await store.me({ populate: ['jobs.candidates'], fields: ['jobs']});
    expect(apiGET).toHaveBeenCalledWith('/api/users/me', {
      params: { populate: ['jobs.candidates'], fields: ['jobs']}
    });

    const userProps = Object.keys(store.user ?? {});
    expect(userProps).toContain('jobs');
    expect(userProps).not.toContain('messagesReceived');
  });

  it('updates current user profile', async () => {
    const apiPUT = vi.spyOn(httpClient, 'put').mockImplementation((_, data) =>
      Promise.resolve({
        data: {
          ...store.user,
          ...data
        }
      })
    );
    store.user = generateData(28);

    const { firstname, lastname, ui_language } = generateData(28);
    await store.updateProfile({ firstname, lastname, ui_language });
    expect(apiPUT).toHaveBeenCalledWith('/api/users/28', { firstname, lastname, ui_language }, undefined);
  });

  it('updates a user profile', async () => {
    const apiPUT = vi.spyOn(httpClient, 'put').mockImplementation((_, data) =>
      Promise.resolve({
        data: {
          ...store.user,
          ...data
        }
      })
    );
    store.user = generateData(28);

    const { firstname, lastname, ui_language } = generateData(28);
    await store.updateProfile({ firstname, lastname, ui_language }, 26);
    expect(apiPUT).toHaveBeenCalledWith('/api/users/26', { firstname, lastname, ui_language }, undefined);
    expect(store.user?.firstname).not.toEqual(firstname);
  });
});
