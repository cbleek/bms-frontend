import { beforeAll, beforeEach, describe, expect, it, vi } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { faker } from '@faker-js/faker';

import { ref } from 'vue';
import { useCompanyStore } from 'stores/company';
import { HttpClient } from 'src/core/lib/http';
import { MailTemplateType } from 'src/core/types';

const httpClient = { post: vi.fn(), patch: vi.fn(), put: vi.fn(), get: vi.fn(), delete: vi.fn() };

describe('stores/company', () => {
  let store: ReturnType<typeof useCompanyStore>;

  faker.setLocale('en_US');
  const generateLogo = () => ({
    id: faker.datatype.number(),
    attributes: {
      width: 286,
      height: 432,
      url: '/uploads/random-test_f607454c4e.png',
      createdAt: faker.datatype.datetime(),
      updatedAt: faker.datatype.datetime()
    }
  });

  const generateData = (name: MailTemplateType['name']) => ({
    name: name,
    subject: faker.lorem.sentence(6),
    enabled: faker.datatype.boolean()
  });

  beforeAll(() => {
    setActivePinia(createPinia());
    store = useCompanyStore();
    store.$http = ref<HttpClient<unknown>>(httpClient);
  });

  beforeEach(() => {
    vi.clearAllMocks();
  });

  it('initializes store', () => {
    expect(store).toBeTypeOf('object');
    expect(store.isLoading).toEqual(false);
    expect(store.company).toBeNull();
  });

  it("retrieves user's company", async () => {
    vi.mock('stores/auth', () => ({
      useAuthStore: () => ({ user: { id: 36, company: { id: 29 }}})
    }));

    const wrap = (data: { id: number }) => {
      const { id, ...attributes } = data;
      return { id, attributes };
    };

    const userCompany = {
      id: 29,
      description: faker.company.catchPhrase(),
      homepage: faker.internet.url(),
      industry: faker.company.bsNoun(),
      name: faker.company.name(),
      size: '2-10',
      createdAt: faker.datatype.datetime(),
      updatedAt: faker.datatype.datetime(),

      logo: {
        data: generateLogo()
      },
      header: [generateLogo(), generateLogo()]
    };

    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation(() =>
      Promise.resolve({
        data: { data: wrap(userCompany), meta: {}}
      })
    );

    await store.fetchCompany();
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith(`/api/companies/${userCompany.id}`, {
      params: { populate: ['logo', 'header']}
    });
    expect(store.company).toEqual(userCompany);
  });

  it('retrieves all mail templates', async () => {
    const mailTemplatesArray = [...new Array(10)].map((v, i) => ({
      id: i + 1,
      attributes: generateData('inviteCandidate')
    }));

    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation(() =>
      Promise.resolve({
        data: { data: mailTemplatesArray, meta: { pagination: { page: 1, pageSize: 10, total: 10 }}}
      })
    );
    const fetchParams = { pagination: { page: 1, rowsPerPage: 10 }};

    await store.fetchMailTemplates(fetchParams);
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith('/api/mail-template-companies', {
      params: {
        'pagination[page]': fetchParams.pagination.page,
        'pagination[pageSize]': fetchParams.pagination.rowsPerPage,
        populate: '*'
      }
    });

    expect(store.mailTemplates.result).toHaveLength(10);
    expect(store.mailTemplates.result).toStrictEqual(mailTemplatesArray);
  });

  it('retrieves mail templates with pagination', async () => {
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation((_, config) => {
      const { pagination } = config?.params ?? { pagination: { page: 0, pageSize: 0 }};
      return Promise.resolve({
        data: { data: [], meta: { pagination: { ...pagination, pageCount: 1, total: 12 }}}
      });
    });

    await store.fetchMailTemplates({
      pagination: { page: 1, rowsPerPage: 5, sortBy: 'subject', descending: false }
    });
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith('/api/mail-template-companies', {
      params: {
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate: '*',
        sort: 'subject:asc'
      }
    });
    expect(store.getMailTemplatesPagination).toEqual({
      descending: false,
      page: 1,
      rowsNumber: 12,
      rowsPerPage: 10,
      sortBy: 'subject'
    });

    await store.fetchMailTemplates({
      pagination: { page: 2, rowsPerPage: 5, sortBy: 'subject', descending: true }
    });
    expect(apiGET).toHaveBeenCalledTimes(2);
    expect(apiGET).toHaveBeenCalledWith('/api/mail-template-companies', {
      params: {
        'pagination[page]': 2,
        'pagination[pageSize]': 5,
        populate: '*',
        sort: 'subject:desc'
      }
    });
    expect(store.getMailTemplatesPagination).toEqual({
      descending: true,
      page: 1,
      rowsNumber: 12,
      rowsPerPage: 10,
      sortBy: 'subject'
    });
  });

  it('retrieves mail templates with filters', async () => {
    const apiGET = vi.spyOn(httpClient, 'get').mockImplementation(() =>
      Promise.resolve({
        data: { data: [], meta: { pagination: {}}}
      })
    );

    const q = faker.random.word();
    await store.fetchMailTemplates({ pagination: { page: 1, rowsPerPage: 5 }, filter: { q }});
    expect(apiGET).toHaveBeenCalledOnce();
    expect(apiGET).toHaveBeenCalledWith('/api/mail-template-companies', {
      params: {
        _q: q,
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate: '*'
      }
    });

    const locale = faker.random.locale();
    await store.fetchMailTemplates({ pagination: { page: 1, rowsPerPage: 5 }, filter: { locale }});
    expect(apiGET).toHaveBeenCalledTimes(2);
    expect(apiGET).toHaveBeenCalledWith('/api/mail-template-companies', {
      params: {
        locale,
        'pagination[page]': 1,
        'pagination[pageSize]': 5,
        populate: '*'
      }
    });
  });

  it('updates a mail template', async () => {
    const templateId = 33;
    const newMailTemplateData = generateData('inviteEmployee');
    const apiPUT = vi.spyOn(httpClient, 'put').mockImplementation(() =>
      Promise.resolve({
        data: newMailTemplateData
      })
    );

    await store.updateMailTemplate(templateId, newMailTemplateData);
    expect(apiPUT).toHaveBeenCalledOnce();
    expect(apiPUT).toHaveBeenCalledWith(
      `/api/mail-template-companies/${templateId}`,
      {
        data: newMailTemplateData
      },
      undefined
    );
  });
});
