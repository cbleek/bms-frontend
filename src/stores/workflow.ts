import { defineStore } from 'pinia';
import { useCompanyWorkflowService } from 'src/core/services';
import {
  CompanyWorkflowType,
  CompanyWorkflowTypeFilter,
  CompanyWorkflowTypeResponse,
  ContentTypeID
} from 'src/core/types';
import { FetchParams, Nullable, StoredData } from 'src/types';
import { HttpError } from 'src/core/lib/http';

type WorkflowState = {
  isLoading: boolean;
  workflow: Nullable<Partial<CompanyWorkflowType>>;

  workflows: StoredData<CompanyWorkflowTypeResponse>;
};

export const useWorkflowStore = defineStore('workflowStore', {
  state: (): WorkflowState => ({
    isLoading: false,
    workflow: null,

    workflows: {
      result: [],
      sort: 'createdAt:desc',
      filter: undefined,
      paginator: undefined
    }
  }),

  actions: {
    async fetchWorkflows(params?: FetchParams<CompanyWorkflowType, CompanyWorkflowTypeFilter>) {
      const workflowService = useCompanyWorkflowService(this.$http);

      this.isLoading = true;
      return new Promise((resolve, reject)=>{
        return workflowService
            .getAll(params)
            .then(r => {
              if (!r.data) throw new Error(r.error.message);

              this.workflows.result = r.data.data;
              if (r.data.meta) this.workflows.paginator = r.data.meta.pagination;
              resolve(r)
            })
            .catch((error: HttpError | Error) => {
              if ('response' in error) throw new Error(error.response?.data.error.message);
              reject(error)
              throw error;
            })
            .finally(() => (this.isLoading = false));
      })
    },
    async createWorkflow(data: Partial<CompanyWorkflowType>) {
      const workflowService = useCompanyWorkflowService(this.$http);

      return await workflowService
          .create(data)
          .then(r => {
            if (!r.data) throw new Error(r.error.message);
            this.workflow = r.data.data;
            return r;
          })
          .catch((error: HttpError | Error) => {
            if ('response' in error) throw new Error(error.response?.data.error.message);
            throw error;
          });
    },
    async updateWorkflow(id: ContentTypeID, data: Partial<CompanyWorkflowType>) {
      const workflowService = useCompanyWorkflowService(this.$http);

      return workflowService
        .update(id, data)
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          this.workflow = r.data.data;
          return r;
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        });
    }
  }
});
