import { defineStore } from 'pinia';
import { useQuestionService } from 'src/core/services';

export const useQuestionStore = defineStore('questionStore', {
    state: () => ({
        question: {
            id: null,
            question: null,
            type: null,
            question_type: [],
            section: null,
            options: []
        }
    }),
    actions: {
        async save(payload, id = null) {
            const questionService = useQuestionService(this.$http);
            return new Promise((resolve, reject) => {
                if (id != null) {
                    questionService.update(id, payload).then(response => resolve(response)).catch(error => reject(error))
                } else {
                    questionService.create(payload).then(response => resolve(response)).catch(error => reject(error))
                }
            })
        },
        async delete(id: number) {
            const questionService = useQuestionService(this.$http);
            return new Promise((resolve, reject) => {
                questionService.delete(id).then(response => resolve(response)).catch(error => reject(error))
            });
        }
    }
})
