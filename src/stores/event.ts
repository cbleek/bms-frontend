import { defineStore } from 'pinia';
import { useEventService } from 'src/core/services';
import { prepareParams } from 'src/helpers';

export const useEventStore = defineStore('eventStore', {
  state: () => ({
    event: {
      id: null,
      date: null,
      time: null,
      subject: null,
      duration: null,
      recruiters: null,
      candidates: null
    },
    events: {
      result: [],
      filter: '',
      sort: 'id:asc',
      paginator: {
        page: undefined,
        pageSize: undefined,
        pageCount: undefined,
        total: undefined
      }
    }
  }),
  actions: {
    async save(payload) {
      const eventService = useEventService(this.$http);

      return new Promise((resolve, reject) => {
        eventService
          .create(payload)
          .then(response => resolve(response))
          .catch(error => reject(error));
      });
    },
    async update(payload, id: number) {
      const eventService = useEventService(this.$http);

      return new Promise((resolve, reject) => {
        eventService
          .update(id, payload)
          .then(response => resolve(response))
          .catch(error => reject(error));
      });
    },
    async delete(id: number) {
      const eventService = useEventService(this.$http);

      return new Promise((resolve, reject) => {
        eventService
          .delete(id)
          .then(response => resolve(response))
          .catch(error => reject(error));
      });
    },

    async fetchEvents(props = { filter: { q: '' }, pagination: { page: 1, rowsPerPage: 1000 }}) {
      const eventService = useEventService(this.$http);

      const params = prepareParams(props, (params, filter) =>
        Object.assign({}, params, {
          filter: filter
        })
      );

      return new Promise((resolve, reject) => {
        eventService
          .getAll(params)
          .then(response => {
            this.events.result = response.data.data;
            resolve(response);
          })
          .catch(error => reject(error));
      });
    }
  }
});
