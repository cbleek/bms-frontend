import { defineStore } from 'pinia';
import { useCandidateService } from 'src/core/services';
import { preparePagination, prepareParams } from 'src/helpers';
import { CandidateType, CandidateTypeFilter, CandidateTypeResponse, ContentTypeID, ReviewType } from 'src/core/types';
import { StoredData, Nullable, FetchParams } from 'src/types';

type CandidateState = {
  isLoading: boolean;
  // TODO: add data transformers to keep types consistent
  candidate: Nullable<Partial<CandidateType>>;

  // TODO: improve state
  candidates: StoredData<CandidateTypeResponse>;
};

export const useCandidateStore = defineStore('candidate', {
  state: (): CandidateState => ({
    isLoading: false,
    candidate: null,

    candidates: {
      result: [],
      sort: 'updatedAt:desc',
      filter: undefined,
      paginator: undefined
    }
  }),

  getters: {
    getPagination(state) {
      const { sort, paginator } = state.candidates;
      return preparePagination<CandidateType>({ sort, paginator });
    }
  },

  actions: {
    async saveCandidateInFormData(formData, options) {
      const candidateService = useCandidateService(this.$http);
      this.isLoading = true;

      return await candidateService.createWithFormData(formData, options).then(response => {
        this.candidate = {
          id: response.data.data.id,
          ...response.data.data.attributes
        };
        return response;
      })
    },
    async save(data: Partial<CandidateType>, id?: ContentTypeID) {
      const candidateService = useCandidateService(this.$http);

      this.isLoading = true;

      if (id)
        return await candidateService
          .update(id, data)
          .then(r => {
            this.candidate = {
              id: r.data.data.id,
              ...r.data.data.attributes
            };
            return r;
          })
          .finally(() => {
            this.isLoading = false;
          });

      return await candidateService
        .create(data)
        .then(r => {
          this.candidate = {
            id: r.data.data.id,
            ...r.data.data.attributes
          };
          return r;
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async fetchCandidates(props: FetchParams<CandidateType, CandidateTypeFilter>) {
      const candidateService = useCandidateService(this.$http);

      const params = prepareParams(props, (params, filter) =>
        Object.assign({}, params, {
          populate: [
            'photo',
            'attachments',
            'recruiter_users.photo',
            'job.company.logo',
            'job.company.header',
            'job.workflow',
            'job.departmentStaff.photo',
            'job.recruiters.photo',
            'reviews',
            'applicant'
          ],
          ...(filter?.job && { ['filters[job]']: filter.job }),
          ...(filter?.status && { ['filters[status]']: filter.status }),
          ...(filter?.department && { ['filters[job][departments]']: filter.department }),
          ...(filter?.group && { ['group']: filter.group }),
          ...(filter?.recruiter && filter.recruiter === 'own' && { ['me']: 1 })
        })
      );
      this.isLoading = true;

      return await candidateService
        .getAll(params)
        .then(({ data }) => {
          this.candidates.result = data.data;
          this.candidates.paginator = data.meta.pagination;
          if (params.sort) this.candidates.sort = params.sort;
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async fetchCandidate(id: ContentTypeID) {
      const candidateService = useCandidateService(this.$http);

      this.isLoading = true;

      await candidateService
        .getById(id, {
          populate: [
            'photo',
            'reviews.recruiter.photo',
            'events',
            'applicant',
            'attachments',
            'recruiter_users.photo',
            'applicant.messages_sent',
            'answers.question'
          ]
        })
        .then(({ data }) => {
          this.candidate = {
            id: data.data.id,
            ...data.data.attributes
          };
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async invite(params: { email: string; id: number }) {
      const candidateService = useCandidateService(this.$http);

      this.isLoading = true;
      await candidateService
        .inviteCandidate(params)
        .then(({ data }) => {
          if (this.candidate) this.candidate.applicant = data;
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async review(data: Partial<ReviewType>) {
      const candidateService = useCandidateService(this.$http);

      this.isLoading = true;
      await candidateService
        .reviewCandidate(data)
        .then(result => {
          // what's the meaning of this
          this.candidate?.reviews?.data?.map(obj => result.data.data.id === obj.id || obj);
        })
        .finally(() => {
          this.isLoading = false;
        });
    }
  },

  persist: false
});
