import { defineStore } from 'pinia';
import { useEmployeeService } from 'src/core/services';
import { preparePagination } from 'src/helpers';
import { PaginationProps } from 'src/types';

export const useEmployeeStore = defineStore('employee', {
  state: () => ({
    isLoading: false,
    employee: {
      id: null,
      user: {},
      company: {}
    },
    employees: {
      result: [],
      filter: '',
      sort: 'id:asc',
      paginator: {
        page: undefined,
        pageSize: undefined,
        pageCount: undefined,
        total: undefined
      }
    }
  }),

  getters: {
    getJob(state) {
      return state.employee;
    },
    getJobs(state) {
      return state.employees;
    },
    getPagination(state) {
      return preparePagination(state.employees);
    }
  },

  actions: {
    async count() {
      console.log('Log');
    },
    async save(employee: Employee) {
      const employeeService = useEmployeeService(this.$http);

      this.isLoading = true;
      if (employee && employee?.id) {
        const id = employee.id;
        delete employee.id;

        await employeeService
          .update(id, employee)
          .then(r => {
            this.employee = r.data;
          })
          .finally(() => {
            this.isLoading = false;
          });
      } else {
        const data = { ...employee };

        await employeeService
          .create(data)
          .then(r => {
            this.employee = r.data;
          })
          .finally(() => {
            this.isLoading = false;
          });
      }
    },
    async delete(id) {
      const employeeService = useEmployeeService(this.$http);

      this.isLoading = true;
      return new Promise((resolve, reject) => {
        employeeService
          .delete(id)
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error);
          })
          .finally(() => {
            this.isLoading = false;
          });
      });
    },
    async fetchEmployees(props: { filter; pagination: PaginationProps }) {
      this.isLoading = true;
      const { sortBy, descending, page, rowsPerPage } = props.pagination;
      const params = {
        'pagination[page]': page ?? 1,
        'pagination[pageSize]': rowsPerPage ?? 10,
        populate: ['company', 'departments', 'user.photo', 'user.role']
      };
      if (sortBy) {
        params.sort = `${sortBy}:${descending ? 'desc' : 'asc'}`;
      }

      if (props.filter && typeof props.filter.valueOf() === 'string') {
        params._q = props.filter;
      }

      if (props.filter && typeof props.filter.valueOf() === 'object') {
        params.filters = props.filter;
      }
      console.log(params, typeof props.filter.valueOf());

      const employeeService = useEmployeeService(this.$http);

      await employeeService
        .getAll(params)
        .then(response => {
          this.employees.result = response.data.data;
          this.employees.paginator = { ...response.data.meta.pagination };
          if (params.sort) this.employees.sort = params.sort;
        })
        .catch(e => {
          console.log(e);
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async setInitialPassword(password) {
      const employeeService = useEmployeeService(this.$http)

      this.isLoading = true;
      return new Promise((resolve, reject) => {
        employeeService
          .updatePassword(password)
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error);
          })
          .finally(() => {
            this.isLoading = false;
          });
      });
    },

    async saveEmployeePhoto(photo, id) {
      const employeeService = useEmployeeService(this.$http)

      this.isLoading = true;
      const formData = new FormData();
      formData.append('files', photo);
      return new Promise((resolve, reject) => {
        let response = null;
        if (id) {
          response = employeeService.updatePhoto(id, formData);
        } else {
          response = employeeService.savePhoto(formData);
        }
        response
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error);
          })
          .finally(() => {
            this.isLoading = false;
          });
      });
    }
  },
  persist: false
});
