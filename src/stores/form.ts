import { defineStore } from 'pinia';
import { useApplicationFormService } from 'src/core/services';
import { preparePagination, prepareParams } from 'src/helpers';
import {
  ApplicationFormType,
  ApplicationFormTypeFilter,
  ApplicationFormTypeResponse,
  ContentTypeID
} from 'src/core/types';
import { FetchParams, Nullable, StoredData } from 'src/types';
import { HttpError } from 'src/core/lib/http';

type FormState = {
  isLoading: boolean;
  form: Nullable<Partial<ApplicationFormType>>;

  forms: StoredData<ApplicationFormTypeResponse>;
};

export const useFormStore = defineStore('form', {
  state: (): FormState => ({
    isLoading: false,
    form: null,

    forms: {
      result: [],
      sort: 'updatedAt:desc',
      filter: undefined,
      paginator: undefined
    }
  }),

  getters: {
    getForm(state) {
      return state.form;
    },

    getForms(state) {
      return state.forms;
    },

    getPagination(state) {
      const { sort, paginator } = state.forms;
      return preparePagination<ApplicationFormType>({ sort, paginator });
    }
  },

  actions: {
    async save(data: Partial<ApplicationFormType>, id?: ContentTypeID) {
      const formService = useApplicationFormService(this.$http);

      if (id)
        return formService
          .update(id, data)
          .then(r => {
            if (!r.data) throw new Error(r.error.message);

            this.form = r.data.data;
          })
          .catch((error: HttpError | Error) => {
            if ('response' in error) throw new Error(error.response?.data.error.message);
            throw error;
          });

      return formService
        .create(data)
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          this.form = r.data.data;
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        });
    },

    async delete(id: number) {
      const formService = useApplicationFormService(this.$http);

      return formService.delete(id).catch((error: HttpError | Error) => {
        if ('response' in error) throw new Error(error.response?.data.error.message);
        throw error;
      });
    },

    async fetchForms(props: FetchParams<ApplicationFormType, ApplicationFormTypeFilter>) {
      const formService = useApplicationFormService(this.$http);

      const params = prepareParams(props, params =>
        Object.assign({}, params, {
          populate: ['locales', 'questions', 'company.logo']
        })
      );

      this.isLoading = true;
      return await formService
        .getAll(params)
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          this.forms.result = r.data.data;
          if (r.data.meta) this.forms.paginator = r.data.meta.pagination;
          if (params.sort) this.forms.sort = params.sort;
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async fetchForm(id: number) {
      const formService = useApplicationFormService(this.$http);

      return formService
        .getById(id, { populate: '*' })
        .then(r => {
          if (!r.data) throw new Error(r.error.message);

          this.form = r.data.data;
          return r.data.data;
        })
        .catch((error: HttpError | Error) => {
          if ('response' in error) throw new Error(error.response?.data.error.message);
          throw error;
        });
    }
  },

  persist: false
});
