import { defineStore } from 'pinia';
import { useAuthStore } from 'stores/auth';
import { useCompanyService, useCompanySettingService, useMailTemplateService } from 'src/core/services';
import { preparePagination, prepareParams } from 'src/helpers';
import {
  CompanySettings,
  CompanySettingType,
  CompanySettingTypeFilter,
  CompanyType,
  CompanyTypeFilter,
  CompanyTypeResponse, ContentTypeID,
  MailTemplateType,
  MailTemplateTypeFilter,
  MailTemplateTypeResponse
} from 'src/core/types';
import { FetchParams, Nullable, StoredData } from 'src/types';

type CompanyState = {
  isLoading: boolean;
  company: Nullable<Partial<CompanyType>>;
  settings: Nullable<Partial<CompanySettings>>;

  companies: StoredData<CompanyTypeResponse>;
  mailTemplates: StoredData<MailTemplateTypeResponse>;
};

export const useCompanyStore = defineStore('company', {
  state: (): CompanyState => ({
    isLoading: false,
    company: null,
    settings: {
      rolesEnabled: true
    },

    companies: {
      result: [],
      sort: 'id:asc',
      filter: undefined,
      paginator: undefined
    },
    mailTemplates: {
      result: [],
      sort: 'id:asc',
      filter: undefined,
      paginator: undefined
    }
  }),

  getters: {
    getPagination(state) {
      const { sort, paginator } = state.companies;
      return preparePagination<CompanyType>({ sort, paginator });
    },

    getMailTemplatesPagination(state) {
      const { sort, paginator } = state.mailTemplates;
      return preparePagination<MailTemplateType>({ sort, paginator });
    },

    getSettings(state) {
      return state.settings;
    }
  },

  actions: {
    /**
     * Authenticated users get the company they are related to.
     * Anonymous users can access public careersite
     * @param id
     */
    async fetchCompany(id?: ContentTypeID) {
      const { user } = useAuthStore();
      if (!id && !user) return;

      const companyService = useCompanyService(this.$http);

      this.isLoading = true;
      return await companyService
        // TODO: test user data, fix typing
        .getById(id ?? user?.company?.id, {
          populate: id ? ['logo', 'header', 'jobs'] : ['logo', 'header'],
          ...(id && { filters: { careersiteEnabled: true }})
        })
        .then(({ data }) => {
          const { id, attributes } = data.data;
          this.company = { id, ...attributes };
        })
        .finally(() => (this.isLoading = false));
    },

    async fetchCompanies(props: FetchParams<CompanyType, CompanyTypeFilter>) {
      const companyService = useCompanyService(this.$http);
      const params = prepareParams(props, params =>
        Object.assign({}, params, {
          populate: ['logo', 'jobs']
        })
      );

      this.isLoading = true;
      return await companyService
        .getAll(params)
        .then(({ data }) => {
          this.companies.result = data.data;
          this.companies.paginator = data.meta.pagination;
          if (params.sort) this.companies.sort = params.sort;
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async enableCareersite(value: boolean) {
      const companyService = useCompanyService(this.$http);

      this.isLoading = true;
      return await companyService
        .updateStatus(value)
        .catch(error => console.error(error))
        .finally(() => (this.isLoading = false));
    },

    async updateCareersite(data: Partial<CompanyType>) {
      const companyService = useCompanyService(this.$http);

      this.isLoading = true;
      return await companyService
        .update(data)
        .catch(error => console.error(error))
        .finally(() => (this.isLoading = false));
    },

    async fetchMailTemplates(props: FetchParams<MailTemplateType, MailTemplateTypeFilter>) {
      const mailTemplateService = useMailTemplateService(this.$http);
      const params = prepareParams(props, (params, filter) =>
        Object.assign({}, params, {
          populate: '*',
          ...(filter?.locale && { locale: filter.locale })
        })
      );

      this.isLoading = true;
      return await mailTemplateService
        .getAll(params)
        .then(({ data }) => {
          this.mailTemplates.result = data.data;
          this.mailTemplates.paginator = data.meta.pagination;
          if (params.sort) this.mailTemplates.sort = params.sort;
        })
        .finally(() => (this.isLoading = false));
    },

    async updateMailTemplate(id: ContentTypeID, data: Partial<MailTemplateType>) {
      const mailTemplateService = useMailTemplateService(this.$http);

      this.isLoading = true;
      return await mailTemplateService
        .update(id, data)
        .catch(error => console.error(error))
        .finally(() => (this.isLoading = false));
    },

    async register(data: { email: string; firstname: string; lastname: string }) {
      const companyService = useCompanyService(this.$http);

      this.isLoading = true;
      return new Promise((resolve, reject) => {
        companyService
            .register(data)
            .then(response => resolve(response))
            .catch(error => reject(error))
            .finally(() => (this.isLoading = false));
      })
    },

    async fetchSettings(params: FetchParams<CompanySettingType, CompanySettingTypeFilter>) {
      const settingService = useCompanySettingService(this.$http);

      this.isLoading = true;
      return await settingService
        .getAll(params)
        .then(result => {
          this.settings = result.data;
        })
        .catch(error => console.error(error))
        .finally(() => (this.isLoading = false));
    },

    async updateSetting(data: Partial<CompanySettings>) {
      const settingService = useCompanySettingService(this.$http);

      this.isLoading = true;
      return await settingService
        .update(data)
        .then(result => {
          if (this.settings) Object.assign(this.settings, result.data);
          return result;
        })
        .catch(error => console.error(error))
        .finally(() => (this.isLoading = false));
    },

    async saveLogo(data: FormData) {
      const companyService = useCompanyService(this.$http);

      this.isLoading = true;
      return companyService
        .updateLogo(data)
        .then(result => {
          const { id, ...attributes } = result.data;
          if (this.company?.logo) Object.assign(this.company.logo.data, { id, attributes });
          return result;
        })
        .catch(error => console.error(error))
        .finally(() => (this.isLoading = false));
    },

    async saveHeader(id: ContentTypeID, data: FormData) {
      const companyService = useCompanyService(this.$http);

      this.isLoading = true;
      return await companyService
        .updateHeader(id, data)
        .catch(error => console.error(error))
        .finally(() => (this.isLoading = false));
    }
  },

  persist: {
    storage: localStorage,
    paths: ['settings']
  }
});
