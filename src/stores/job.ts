import { defineStore } from 'pinia';
import { useJobService } from 'src/core/services';
import { preparePagination } from 'src/helpers';

export const useJobStore = defineStore('job', {
  state: () => ({
    isLoading: false,
    job: {
      id: null,
      title: '',
      intro: '',
      tasks: '',
      profile: '',
      benefits: '',
      outro: '',
      salary: {
        min: '',
        max: '',
        currency: 'EUR',
        intervall: 'year',
        visible: true
      },
      education: [],
      experience: [],
      contract: [],
      careerLevel: [],
      category: '',
      applicationLanguage: [],
      applicationDeadline: '',
      applicationStart: '',
      employmentType: [],
      country: '',
      city: '',
      homeoffice: false,
      applicationForm: '',
      worksCouncil: [],
      recruiters: [],
      workflow_recruiter_users: [],
      workflow_department_users: [],
      workflow_council_users: [],
      departments: [],
      departmentStaff: [],
      workflow: [],
      publishedAt: '',
      applicationSettings: {
        emailEnabled: true,
        formEnabled: true,
        deadlineEnabled: true,
        applicationStartEnabled: false
      }
    },
    jobs: {
      result: [],
      filter: '',
      sort: 'id:asc',
      paginator: {
        page: undefined,
        pageSize: undefined,
        pageCount: undefined,
        total: undefined
      }
    }
  }),

  getters: {
    getJob(state) {
      return state.job;
    },
    getJobs(state) {
      return state.jobs;
    },
    getPagination(state) {
      return preparePagination(state.jobs);
    }
  },

  actions: {
    async update(job) {
      const jobService = useJobService(this.$http);

      return jobService.update(job.id, job).then(r => {
        //   this.job = r.data;
        return r;
      });
    },

    async save(job) {
      const jobService = useJobService(this.$http);

      delete job.departments;
      delete job.locations;
      delete job.localizations;
      delete job.recruiters;
      delete job.applicationForm;
      delete job.company;
      delete job.candidates;
      delete job.workflow;
      delete job.createdAt;
      delete job.publishedAt;
      delete job.updatedAt;

      if (['de', 'fr', 'en'].indexOf(job.country?.value) > -1) {
        job.country = job.country.value;
      }

      if (['de', 'fr', 'en', 'it', 'es'].indexOf(job.applicationLanguage?.value) > -1) {
        job.applicationLanguage = job.applicationLanguage.value;
      }

      console.log('Start save action', job);
      if (job.id) {
        const id = job.id;
        delete job.id;
        await jobService
          .update(id, job)
          .then(r => {
            this.job = r.data;
          })
          .catch(e => {
            console.log(e);
          });
      } else {
        await jobService
          .create(job)
          .then(r => {
            console.log('CREATES', r);
            this.job = r.data.data;
          })
          .catch(e => {
            console.log(e);
          });
      }
    },

    async fetchJobs(props: { filter: string; pagination }) {
      const jobService = useJobService(this.$http);

      this.isLoading = true;
      const { sortBy, descending, page, rowsPerPage } = props.pagination;

      const params = {
        'pagination[page]': page ?? 1,
        'pagination[pageSize]': rowsPerPage ?? 10,
        populate: ['company.logo', 'company.header', 'applicationForm']
      };

      if (sortBy) {
        params.sort = `${sortBy}:${descending ? 'desc' : 'asc'}`;
      }
      if (props.filter && props.filter.q && typeof props.filter.q.valueOf() === 'string') {
        params._q = props.filter.q;
      }
      try {
        if (props.status) {
          params.status = 1;
        }
        await jobService
          .getAll(params)
          .then(response => {
            console.log(response);
            this.jobs.result = response.data.data;
            this.jobs.paginator = { ...response.data.meta.pagination };
            if (params.sort) this.jobs.sort = params.sort;
          })
          .catch(e => {
            console.log(e);
          })
          .finally(() => {
            this.isLoading = false;
          });
      } catch (e) {
        console.log(e);
      }
    },

    async fetchCompanyJobs(props: { filter: string; pagination }) {
      const jobService = useJobService(this.$http);

      this.isLoading = true;
      const { sortBy, descending, page, rowsPerPage } = props.pagination;

      const params = {
        'pagination[page]': page ?? 1,
        'pagination[pageSize]': rowsPerPage ?? 10,
        populate: ['company.logo', 'company.header', 'recruiters.photo', 'applicationForm', 'candidates', 'departmentStaff', 'workflow', 'workflow_council_users', 'workflow_department_users', 'workflow_recruiter_users']
      };

      if (sortBy) {
        params.sort = `${sortBy}:${descending ? 'desc' : 'asc'}`;
      }
      if (props.filter && props.filter.q && typeof props.filter.q.valueOf() === 'string') {
        params._q = props.filter.q;
      }
      try {
        if (props.status) {
          params.status = 1;
        }
        await jobService
          .getByCompany(params)
          .then(response => {
            this.jobs.result = response.data.data;
            this.jobs.paginator = { ...response.data.meta.pagination };
            if (params.sort) this.jobs.sort = params.sort;
          })
          .catch(e => {
            console.log(e);
          })
          .finally(() => {
            this.isLoading = false;
          });
      } catch (e) {
        console.log(e);
      }
    },

    async fetchJob(id, args?: object) {
      const jobService = useJobService(this.$http);

      const params = {
        populate: ['company.header', 'company.logo', 'applicationForm', 'job_template'],
        ...args
      };

      try {
        await jobService
          .getById(id, params)
          .then(response => {
            this.job = response.data.data;
          })
          .catch(e => {
            console.log(e);
          });
      } catch (e) {
        console.log(e);
      }
    }
  },

  persist: false
});
