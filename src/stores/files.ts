// NOT IN USE

// import { defineStore } from 'pinia';
// import { fileService } from '../services/_singletons';
//
// export const useFileStore = defineStore('files', {
//   state: () => ({
//     isLoading: false,
//     file: null,
//   }),
//
//   actions: {
//
//     async save(data, id = null) {
//       this.isLoading = true
//       return new Promise((resolve, reject) => {
//         let response = null
//         if (id) {
//           response = fileService.updateFile(data, id);
//         } else {
//           response = fileService.createFile(data);
//         }
//         response.then(response => {
//           this.file = Array.isArray(response.data) ? response.data[0] : response.data;
//           resolve(response)
//         })
//           .catch(error => {
//             reject(error);
//           })
//           .finally(() => {
//             this.isLoading = false;
//           });
//       });
//     },
//   },
//   persist: false,
// });
