import { defineStore } from 'pinia';
import { useAdminService } from 'src/core/services';
import { preparePagination } from 'src/helpers';
import { CompanyTypeResponse, MailTemplateCompanyTypeResponse, UserTypeResponse } from 'src/core/types';
import { Nullable, StoredData } from 'src/types';

type AdminState = {
  isLoading: boolean;
  component: Nullable<string>;

  adminCustomers: StoredData<CompanyTypeResponse>;
  adminApplicants: StoredData<UserTypeResponse>;
  adminMessageTemplates: StoredData<MailTemplateCompanyTypeResponse>;
};

export const useAdminStore = defineStore('admin', {
  state: (): AdminState => ({
    isLoading: false,
    component: null,

    adminCustomers: {
      result: [],
      sort: 'id:asc',
      filter: undefined,
      paginator: undefined
    },

    adminApplicants: {
      result: [],
      sort: 'id:asc',
      filter: undefined,
      paginator: undefined
    },

    adminMessageTemplates: {
      result: [],
      sort: 'id:asc',
      filter: undefined,
      paginator: undefined
    }
  }),

  getters: {
    getPagination(state) {
      let data = state.adminCustomers;
      if (state.component == 'customers') data = state.adminCustomers;
      else if (state.component == 'applicants') data = state.adminApplicants;
      else if (state.component == 'messages') data = state.adminMessageTemplates;
      return preparePagination(data);
    }
  },

  actions: {
    async fetchCustomers(props: { filter: string; pagination }) {
      const adminService = useAdminService(this.$http);

      this.component = 'customers';
      this.isLoading = true;
      const params = extractParams(props);
      await adminService
        .getCompanies(params)
        .then(response => {
          setResourceData(this.adminCustomers, response, params);
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async fetchApplicants(props: { filter: string; pagination }) {
      const adminService = useAdminService(this.$http);

      this.component = 'applicants';
      this.isLoading = true;
      const params = extractParams(props);
      params['filters[$and][0][role][name][$eq]'] = 'Candidate';
      await adminService
        .getUsers(params)
        .then(response => {
          setResourceData(this.adminApplicants, response, params);
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async fetchMessageTemplates(props: { filter: string; pagination }) {
      const adminService = useAdminService(this.$http);

      this.component = 'messages';
      this.isLoading = true;
      const params = extractParams(props);
      await adminService
        .getMailTemplates(params)
        .then(response => {
          setResourceData(this.adminMessageTemplates, response, params);
        })
        .finally(() => {
          this.isLoading = false;
        });
    },

    async fetchMessageTemplate(id: number | string) {
      const adminService = useAdminService(this.$http);

      return adminService.getMailTemplate(id);
    },

    async fetchMessageTemplateCompany(id: number | string) {
      const adminService = useAdminService(this.$http);

      return adminService.getMailTemplateCompany(id);
    },

    async fetchRelationsCompanies() {
      const adminService = useAdminService(this.$http);

      return adminService.getMailTemplateCompanyRelations();
    },

    async fetchRelationsMailTemplates() {
      const adminService = useAdminService(this.$http);

      return adminService.getMailTemplateRelations();
    },

    async fetchMessageTemplateMail(id: number | string) {
      const adminService = useAdminService(this.$http);

      return adminService.getMailTemplateMessage(id);
    },

    async updateMessageTemplate(data) {
      const adminService = useAdminService(this.$http);

      this.isLoading = true;
      return adminService.updateMailTemplate(data.id, data).finally(() => {
        this.isLoading = false;
      });
    }
  }
});

function setResourceData(resource, response, params) {
  resource.result = response.data.results;
  resource.paginator = { ...response.data.pagination };
  if (params.sort) resource.sort = params.sort;
}

function extractParams(props) {
  const { sortBy, descending, page, rowsPerPage } = props.pagination;
  const params = {
    page: page ?? 1,
    pageSize: rowsPerPage ?? 10,
    populate: '*'
  };
  if (sortBy) {
    params.sort = `${sortBy}:${descending ? 'desc' : 'asc'}`;
  }

  if (props.filter && typeof props.filter.valueOf() === 'string') {
    params._q = props.filter;
  }
  return params;
}
