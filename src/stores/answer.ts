import { defineStore } from 'pinia';
import { useAnswerService } from 'src/core/services';

export const useAnswerStore = defineStore('answerStore', {
    state: () => ({
        answer: {
            id: null,
            value: null
        }
    }),
    actions: {
        async save(payload, id = null) {
            const answerService = useAnswerService(this.$http);
            return new Promise((resolve, reject) => {
                if (id != null) {
                    answerService.update(id, payload).then(response => resolve(response)).catch(error => reject(error))
                } else {
                    answerService.create(payload).then(response => resolve(response)).catch(error => reject(error))
                }
            })
        },
        async delete(id: number) {
            const answerService = useAnswerService(this.$http);
            return new Promise((resolve, reject) => {
              answerService.delete(id).then(response => resolve(response)).catch(error => reject(error))
            });
        }
    }
})
