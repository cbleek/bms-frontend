import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import EssentialLink from '../EssentialLink.vue';

const defaultProps = {
  title: 'link',
  caption: 'text',
  to: '/',
  icon: 'home'
};

const mountComponent = (props = {}) => {
  return mount(EssentialLink, { shallow: true, props: { ...defaultProps, ...props }});
};

describe('components/EssentialLink', () => {
  it('renders EssentialLink component', () => {
    const wrapper = mountComponent();
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.findAll('q-item-stub').length).toEqual(1);
  });

  it('displays the link', () => {
    const wrapper = mountComponent();
    expect(wrapper.element.tagName).toEqual('Q-ITEM-STUB');
  });
});
