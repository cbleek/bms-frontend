'use strict';

import applicationForm from 'components/forms/ApplicationForm.vue';
import applyContact from 'components/forms/ContactCard.vue'
import applyContactSettings from 'components/forms/apply/ContactSettingCard.vue'
import applyDocumentSettings from 'components/forms/apply/DocumentSettingCard.vue'
import applyFormDialog from 'src/components/dialogs/ApplicationFormDialog.vue'
import applyQuestionSettings from 'components/forms/apply/QuestionSettingCard.vue'
import btnNav from 'components/generic/BtnNav.vue'
import cancelOrSave from 'components/generic/CancelOrSaveCardAction.vue'
import confirmDelete from 'components/fields/ConfirmDelete.vue'
import contactCard from 'components/forms/ContactCard.vue'
import countryIcon from 'components/generic/CountryIcon.vue'
import dialogBar from 'components/commons/DialogBar.vue'
import dropZone from 'components/generic/DropZone.vue'
import editorField from 'components/fields/EditorField.vue'
import emailForm from 'src/components/fields/EmailForm.vue'
import inputApplicationEmail from 'components/fields/InputApplicationEmail.vue'
import inputCity from 'components/fields/CityInput.vue'
import inputField from 'components/fields/InputField.vue'
import jobCategoryForm from 'components/job/JobCategoryForm.vue'
import jobDescriptionForm from 'components/job/JobDescriptionForm.vue'
import jobDetail from 'components/job/detail/JobDetail.vue';
import jobFormStepper from 'components/job/JobFormStepper.vue'
import jobListCompany from 'components/jobs/JobsTable.vue'
import jobPreviewDialog from 'components/job/JobPreviewDialog.vue'
import jobProcessForm from 'components/job/JobProcessForm.vue'
import jobListPublic from 'components/jobs/PublicJobsList.vue'
import jobWorkflowForm from 'components/job/JobWorkflowForm.vue'
import languageSelect from 'components/fields/LanguageSelect.vue'
import languageSwitch from 'components/generic/LanguageSwitch.vue'
import loginStrapi from 'components/auth/LoginBtn.vue'

import optionalMarker from 'components/fields/ChipField.vue'
import processSettingsTable from 'components/settings/ProcessSettingsTable.vue'
import selectApplicationForm from 'components/fields/ApplicationFormSelect.vue'
import selectCountry from 'components/fields/CountrySelect.vue'
import selectField from 'components/fields/SelectField.vue'
import selectEmployee from 'components/fields/SelectEmployee.vue'
import selectDepartment from 'components/fields/DepartmentSelect.vue'
import sinceDate from 'components/generic/SinceDate.vue'
import thumbnail from  'components/candidates/CandidatePhoto.vue'
import workflowSettings from  'components/forms/WorkflowSettings.vue'
import workflowSettingsTable from 'components/settings/WorkflowSettingsTable.vue'
import popupEdit from 'components/commons/popup-edit.vue'



export  {
  applicationForm,
  applyContact,
  applyContactSettings,
  applyDocumentSettings,
  applyFormDialog,
  applyQuestionSettings,
  btnNav,
  cancelOrSave,
  confirmDelete,
  contactCard,
  countryIcon,
  dialogBar,
  dropZone,
  editorField,
  emailForm,
  inputApplicationEmail,
  inputCity,
  inputField,
  jobCategoryForm,
  jobDescriptionForm,
  jobDetail,
  jobFormStepper,
  jobListCompany,
  jobListPublic,
  jobPreviewDialog,
  jobProcessForm,
  jobWorkflowForm,
  languageSelect,
  languageSwitch,
  loginStrapi,
  optionalMarker,
  processSettingsTable,
  selectApplicationForm,
  selectCountry,
  selectDepartment,
  selectEmployee,
  selectField,
  sinceDate,
  thumbnail,
  workflowSettings,
  workflowSettingsTable,
  popupEdit
}
