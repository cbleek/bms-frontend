import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import BmsCheckbox from '../BmsCheckbox.vue';

const defaultProps = {
  modelValue: false,
};

const mountComponent = (props = {}) => {
  return mount(BmsCheckbox, { shallow: true, props: { ...defaultProps, ...props }});
};

describe('components/EssentialLink', () => {
  it('renders BmsCheckbox component', () => {
    const wrapper = mountComponent();
    expect(wrapper.exists()).toBe(true);
  });

  it('displays the link', () => {
    const wrapper = mountComponent();
    expect(wrapper.element.tagName).toEqual('Q-CHECKBOX-STUB');
  });
});
