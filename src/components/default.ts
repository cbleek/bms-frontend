'use strict';

export const defaults = {
  pagination: {
    page: 1,
    rowsPerPage: 10,
    sortBy: 'updatedAt',
    descending: true
  },
  rowsPerPage: [10, 25, 50],
  filterJobs: {
    q: ''
  },
  filterCandidates: {
    q: '',
    job: '',
    department: '',
    group: '',
    recruiter: 'all',
    status: ''
  },
  filterMessages: {
    q: '',
    box: ''
  },
  filterMailTemplates: {
    q: '',
    locale: 'de-DE' as string | null
  },
  filterSettings: {
    q: ''
  },
  filterForms: {
    q: '',
    locale: 'de-DE'
  },
  filterEmployees: {
    q: ''
  },
  icons: {
    fileGeneral: 'description',
    fileDocx: 'img:/icons/mime-docx.svg',
    fileOpenoffice: 'img:/icons/mime-openoffice.svg',
    filePdf: 'img:/icons/mime-pdf.svg',
    fileTxt: 'img:/icons/mime-txt.svg',
    stepTwo: 'img:/icons/mime-txt.svg'
  }
};


