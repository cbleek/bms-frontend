import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';
import { describe, it, expect } from 'vitest';
import { mount, config } from '@vue/test-utils';
import StatusSelect from '../StatusSelect.vue';

import { createI18n } from 'vue-i18n';
import messages from 'src/i18n';

installQuasarPlugin();

// const defaultProps = {
//   status: 'applied',
//   size: 'sm',
//   iconOnly: false
// }

const i18n = createI18n({
  legacy: false,
  locale: 'de-DE',
  fallbackLocale: 'en-US',
  silentTranslationWarn: true,
  globalInjection: true,
  messages
});
config.global.plugins = [i18n];
config.global.mocks.$t = phrase => phrase;

describe('components/fields/StatusSelect', () => {
  it('renders component', () => {
    const wrapper = mount(StatusSelect, { props: { size: 'md' }});
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.element.tagName).toEqual('BUTTON');
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.findAll('BUTTON').length).toEqual(0);
  });

  it('displays applied status', () => {
    const wrapper = mount(StatusSelect, { props: { status: 'applied' }});
    expect(wrapper.text()).toBe('status.applied');
  });
  it('displays cancelled status', () => {
    const wrapper = mount(StatusSelect, { props: { status: 'canceled' }});
    expect(wrapper.text()).toBe('status.canceled');
  });
  it('emit change status', () => {
    // const wrapper = mountComponent();
    const wrapper = mount(StatusSelect, { props: { size: 'md' }});
    wrapper.vm.$emit('statusChange', 'applied');
    expect(wrapper.emitted().statusChange).toBeTruthy();
  });

  it('check size prop', () => {
    const wrapper = mount(StatusSelect, { props: { size: 'md' }});
    expect(wrapper.props().size).toBe('md');
  });
});
