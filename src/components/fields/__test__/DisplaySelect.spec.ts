import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';
import { describe, it, expect } from 'vitest'
import { mount, config } from '@vue/test-utils'
import DisplaySelect from '../DisplaySelect.vue'

import { createI18n } from 'vue-i18n'
import messages from 'src/i18n'


installQuasarPlugin();

const defaultProps = {
  value: 'auto',
}

const i18n = createI18n({
  legacy: false,
  locale: 'de-DE',
  fallbackLocale: 'en-US',
  silentTranslationWarn: true,
  globalInjection: true,
  messages
})
config.global.plugins = [i18n]
config.global.mocks.$t = phrase => phrase;

describe('components/fields/DisplaySelect', () => {
  it('renders component', () => {
    const wrapper =  mount(DisplaySelect, { props: defaultProps });
    expect(wrapper.exists()).toBe(true)
    expect(wrapper.element.tagName).toEqual('LABEL')
    expect(wrapper.exists()).toBe(true)
    expect(wrapper.findAll('LABEL').length).toEqual(0)
  })

  it('displays applied status', () => {
    const wrapper = mount(DisplaySelect, { props: { value: 'bright' }});
    expect(wrapper.text()).toContain('Helles Design');
  })
  it('displays cancelled status', () => {
    const wrapper = mount(DisplaySelect, { props: { value: 'dark' }});
    expect(wrapper.text()).toContain('Dunkles Design');
  })


})
