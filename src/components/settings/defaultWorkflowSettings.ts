'use strict';

const applicationForm = {
  default: false,
  type: String
}

const applicationEmail = {
  default: '',
  type: String
}

export const settings = {
  applicationEmail,
  applicationForm
};
