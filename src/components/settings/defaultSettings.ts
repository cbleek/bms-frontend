'use strict';

const darkmode = {
  default: 'auto',
  type: String,
  options: ['dark', 'light', 'auto']
}

const employesCanCreateCandidate = {
  default: true,
  type: Boolean
}

const jobRequiresRecruiter = {
  default: false,
  type: Boolean
}

const jobRequiresDepartment = {
  default: false,
  type: Boolean
}

const separateDepartments = {
  default: false,
  type: Boolean
}

const languageDefault = {
  default: 'de-DE',
  type: String
}

const languagesAvailable = {
  default: [
    { label: 'German', value: 'de-DE' },
    { label: 'English', value: 'en-US' },
    { label: 'French', value: 'fr-FR' }
  ],
  type: Array
}

const maxUploadFiles = {
  default: 3,
  type: Number
}

const maxUploadSizeAttachments = {
  default: 10240000,
  type: Number,
  min:512000,
  max:8388608,
  step:19,
}

const maxUploadSizePhoto = {
  default: 512000,
  type: Number,
  min:102400,
  max:2048000,
  step:19,
}

const minPasswordLength = {
  default: 8,
  min: 6,
  type: Number,
}

const multilingualEnabled = {
  default: false,
  type: Boolean
}

const rolesEnabled = {
  default: false,
  type: Boolean
}

const redirectAfterLogin = {
  default: 'dashboard',
  type: String,
  options: [
    'dashboard',
    'jobs',
    'candidate'
  ]
}

export const settings = {
  darkmode,
  redirectAfterLogin,
  employesCanCreateCandidate,
  maxUploadFiles,
  maxUploadSizeAttachments,
  maxUploadSizePhoto,
  minPasswordLength,
  multilingualEnabled,
  languageDefault,
  languagesAvailable,
  rolesEnabled,
  jobRequiresRecruiter,
  jobRequiresDepartment,
  separateDepartments,
};
