'use strict';

const departmentMembers = {
  default: '',
  type: String
}

const recruiterMembers = {
  default: '',
  type: String
}

export const settings = {
  departmentMembers,
  recruiterMembers
};
