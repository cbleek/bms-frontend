import { createI18n } from 'vue-i18n';
import { Quasar } from 'quasar';

import { describe, it, expect } from 'vitest';
import { mount, config } from '@vue/test-utils';
import messages from 'src/i18n';

import LanguageSwitch from '../LanguageSwitch.vue';

const i18n = createI18n({
  legacy: false,
  locale: 'de-DE',
  fallbackLocale: 'en-US',
  silentTranslationWarn: true,
  globalInjection: true,
  messages
});
const wrapperFactory = (props?: object) =>
  mount(LanguageSwitch, {
    global: {
      plugins: [Quasar]
    },
    ...props
  });

config.global.plugins = [i18n];
config.global.mocks.$t = phrase => phrase;

describe('components/generic/LanguageSwitch', () => {
  it('mount component without option LangaugeSwitch', () => {
    expect(LanguageSwitch).toBeTruthy();
    const wrapper = wrapperFactory();
    // expect(wrapper.get('span').text()).contains('Deutsch')
    expect(wrapper.get('div.q-field__label').text()).contains('label.language');
    // console.log(wrapper.html());
  });

  it('emit changeLang', () => {
    const wrapper = wrapperFactory();
    wrapper.vm.$emit('changeLang', 123);
    expect(wrapper.emitted().changeLang).toBeTruthy();
  });
});
