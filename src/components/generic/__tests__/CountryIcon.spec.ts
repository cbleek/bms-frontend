import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import CountryIcon from '../CountryIcon.vue'

const defaultProps = {
  name: 'de',
  size: 'xs'
}

const mountComponent = (props = {}) => {
  return mount(CountryIcon, { shallow: true, props: { ...defaultProps, ...props }})
}

describe('components/generic/CountryIcon', () => {
  it ('renders component', () => {
    const wrapper = mountComponent()
    expect(wrapper.exists()).toBe(true)
    expect(wrapper.findAll('q-icon-stub').length).toEqual(1)
  })

  it ('displays the icon', () => {
    const wrapper = mountComponent()
    expect(wrapper.element.tagName).toEqual('Q-ICON-STUB')
    expect(wrapper.attributes('size')).toBe('xs')
    expect(wrapper.attributes('name')).toBe('img:flags/de.svg')
  })

  it('displays the correct flag', () => {
    const wrapper = mountComponent({ name: 'en' })
    expect(wrapper.attributes('name')).toBe('img:flags/en.svg')
  })

  it ('displays the correct size', () => {
    const wrapper = mountComponent({ size: 'md' })
    expect(wrapper.attributes('size')).toBe('md')
  })
})
