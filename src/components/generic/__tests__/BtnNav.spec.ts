import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import BtnNav from '../BtnNav.vue';

const defaultProps = {
  title: 'linkOptions',
};

const mountComponent = (props = {}) => {
  return mount(BtnNav, { shallow: true, props: { ...defaultProps, ...props }});
};

describe('components/generic/BtnNav', () => {
  it('renders BtnNav component', () => {
    const wrapper = mountComponent();
    expect(wrapper.exists()).toBe(true);
  });

  it('displays the link', () => {
    const wrapper = mountComponent();
    expect(wrapper.element.tagName).toEqual('DIV');
  });
});
