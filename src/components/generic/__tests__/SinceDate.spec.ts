import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';
import { describe, it, expect } from 'vitest'
import { mount, config } from '@vue/test-utils'
import SinceDate from '../SinceDate.vue'

config.global.mocks.$t = phrase => phrase;

installQuasarPlugin();

const now = new Date();
const minute = now.setMinutes(now.getMinutes() - 1)
const minutes = now.setMinutes(now.getMinutes() - 2)
const hour = now.setHours(now.getHours() - 1)
const hours = now.setHours(now.getHours() - 2)
const yesterday = now.setDate(now.getDate() - 1)
const days = now.setDate(now.getDate() - 2)
const week = now.setDate(now.getDate() - 7)
const weekN = now.setDate(now.getDate() - 14)
const month = now.setMonth(now.getMonth() - 1)
const months = now.setMonth(now.getMonth() - 2)
const year = now.setDate(now.getDate() - 365)
const years = now.setYear(now.getYear() - 2)

const defaultProps = {
  date: '2023-03-04 11:23:67',
}

const mountComponent = (props = {}) => {
  return mount(SinceDate, { shallow: true, props: { ...defaultProps, ...props }})
}

describe('components/generic/SinceDate', () => {
  it ('renders component', () => {
    const wrapper = mountComponent()
    expect(wrapper.exists()).toBe(true)
  })

  it ('displays the date', () => {
    const wrapper = mount(SinceDate, { props: { date: '01.02.2023' }});
    expect(wrapper.element.tagName).toEqual('DIV')
  })

  it ('displays minute', () => {
    const wrapper = mount(SinceDate, { props: { date: minute }});
    expect(wrapper.text()).contains('date.minute1')
  })

  it ('displays N minutes', () => {
    const wrapper = mount(SinceDate, { props: { date: minutes }});
    expect(wrapper.text()).contains('date.minutesN')
  })

  it ('displays hour', () => {
    const wrapper = mount(SinceDate, { props: { date: hour }});
    expect(wrapper.text()).contains('date.hour1')
  })

  it ('displays N hours', () => {
    const wrapper = mount(SinceDate, { props: { date: hours }});
    expect(wrapper.text()).contains('date.hoursN')
  })

  it ('displays yesterday', () => {
    const wrapper = mount(SinceDate, { props: { date: yesterday }});
    expect(wrapper.text()).contains('date.yesterday')
  })

  it ('displays N days', () => {
    const wrapper = mount(SinceDate, { props: { date: days }});
    expect(wrapper.text()).contains('date.daysN')
  })

  it ('displays one week', () => {
    const wrapper = mount(SinceDate, { props: { date: week }});
    expect(wrapper.text()).contains('date.week1')
  })

  it ('displays n weeks', () => {
    const wrapper = mount(SinceDate, { props: { date: weekN }});
    expect(wrapper.text()).contains('date.weeksN')
  })

  it ('displays one month', () => {
    const wrapper = mount(SinceDate, { props: { date: month }});
    expect(wrapper.text()).contains('date.month1')
  })

  it ('displays n month', () => {
    const wrapper = mount(SinceDate, { props: { date: months }});
    expect(wrapper.text()).contains('date.monthsN')
  })

  it ('displays one year', () => {
    const wrapper = mount(SinceDate, { props: { date: year }});
    expect(wrapper.text()).contains('date.year1')
  })

  it ('displays n years', () => {
    const wrapper = mount(SinceDate, { props: { date: years }});
    expect(wrapper.text()).contains('date.yearsN')
  })

})
