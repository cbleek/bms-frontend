import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';
import { describe, it, expect } from 'vitest'
import { mount, config } from '@vue/test-utils'

import StartpageSelect from '../StartpageSelect.vue'

installQuasarPlugin();

config.global.mocks.$t = phrase => phrase;

describe('components/generic/StartpageSelect', () => {
  it ('renders component', () => {
    const wrapper = mount(StartpageSelect, { props: { value: 'jobs' }});
    expect(wrapper.exists()).toBe(true)
    expect(wrapper.findAll('label').length).toEqual(1)
    expect(wrapper.findAll('input').length).toEqual(1)
    expect(wrapper.text()).contains('jobs')
  })
})
