export enum EDisplay {
  dark = true,
  bright = false,
  auto = 'auto'
}
