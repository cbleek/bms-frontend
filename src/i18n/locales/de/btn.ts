/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  abort: 'Abbrechen',
  all: 'Alle',
  applyPostmail: 'Bewerbung per Post',
  applyText: 'jetzt bewerben',
  back: 'Zurück',
  cancel: 'Abbrechen',
  close: 'Schliessen',
  continue: 'Weiter',
  delete: 'Löschen',
  download: 'Download',
  email: 'E-Mail',
  fields: 'Felder',
  forCompanies: 'Für Arbeitgeber',
  inbox: 'Posteingang',
  inviteNewUser: 'Team Mitglied einladen',
  login: 'Anmelden',
  logout: 'Abmelden',
  magazin: 'Magazin',
  my: 'Meine',
  myCareer: 'Meine Karriere',
  myJobs: 'Meine Jobs',
  new: 'Neue',
  next: 'Weiter',
  no: 'Nein',
  outbox: 'Postausgang',
  preview: 'Vorschau',
  previous: 'Zurück',
  publish: 'Veröffentlichen',
  unPublish: 'Veröffentlichung aufheben',
  rate: 'Bewerten',
  register: 'Registrieren',
  save: 'Speichern',
  send: 'Senden',
  today: 'Heute',
  reinviteUser: 'Re-invite User',
  updateUser: 'Update User',
  yes: 'Ja'
};
