export default {
  label: 'Darstellung',
  dark: 'Dunkles Design',
  bright: 'Helles Design',
  auto: 'Gerätezustand'
}

