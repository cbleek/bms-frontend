// import table from './table';
// import input from './input';
import applyForm from  './application-form'
import btn from './btn';
import careerLevel from './career-level';
import contract from './contract';
import date from './date';
import dialog from './dialog';
import display from './display';
import documents from './documents';
import education from './education';
import employmentType from './employment-type';
import event from './event';
import help from './help';
import job from './job';
import label from './label';
import login from './admin/login';
import admin_menu from './admin/menu';
import menu from './menu';
import message from './message';
import notify from './notify';
import rules from './rules';
import salary from './salary';
import settings from './settings';
import status from './candidate/status';
import widget from './widget';
import workExperience from './work-experience';


export default {
  localeName: 'Deutsch',
  french: 'Französisch',
  german: 'Deutsch',
  english: 'Englisch',
  jobs: 'Stellenanzeigen',
  logo: 'Logo',
  header: 'Headerbild',
  create_job: 'Neue Stellenanzeige eingeben',
  edit_job: 'Stellenanzeige bearbeiten',
  jobboard: 'Stellenbörse',
  public_jobs: 'aktuelle Stellenanzeigen',
  ad_management: 'Anzeigenverwaltung',
  team_management: 'Teamverwaltung',
  templates: 'Vorlagen',
  company: 'Firma',
  recruiter: 'Rekruter',
  unpublished: 'unveröffentlicht',
  title: 'Bitte melden Sie sich an',
  action: 'Aktion',
  'ad-management-description':
    'Mit der Anzeigenverwaltung können die Stellenanzeigen speichern, bearbeiten und löschen. Die Anzeigenverwaltung wird auch benötigt, um Stellenanzeigen in ihre Homepage zu integrieren.',
  preis1: 'Die Anmeldung ist kostenlos',
  please_register:
    'Sie sind momentan nicht angemeldet. Um die Anzeigenverwaltung zu nutzen, müssen Sie sich registrieren. Die Registrierung ist kostenlos.',
  alert: 'Achtung',
  wizard_help_title: 'Stellenanzeige erstellen',
  wizard_help_text:
    'Der Jobwizard unterstützt sie bei der Erstellung einer Anzeige. Die Nutzung des Jobwizard und die Erstellung der Anzeige ist kostenlos. Über die Einstellungen können sie weitere Felder aktivieren und deaktivieren.',
  wizard_help_anonymous:
    'Sie sind momentan nicht angemeldet. Sie können als anonymer Benutzer alle Funktionen nutzen. Allerdings können Sie am Ende die Anzeige nur als HTML downloaden.',
  select_or_create: 'Wählen sie eine Firma oder erstellen sie eine neue.',
  welcome: 'Willkommen',
  user_profile: 'Benutzerprofil',
  company_profile: 'Unternehmensprofil',
  mail: {
    application_received: 'Bewerbungseingang',
    application_received_caption:
      'Wenn ein Kandidat sich auf eine Ihrer Stellen bewirbt, senden wir diesem umgehend die unten stehende Nachricht.',
    reject_after_first_review: 'Ablehnung nach Prüfung der Unterlagen',
    reject_after_first_review_caption:
      'Diese Nachricht wird gesendet, nachdem Sie die Bewerbungsunterlagen eines Bewerbers geprüft und ihn als „nicht qualifiziert“ eingestuft haben.',
    reject_during_process: 'Ablehnung im späteren Verlauf',
    reject_during_process_caption:
      'Diese Nachricht wird am nächsten Morgen verschickt, wenn ein Bewerber zuerst als „qualifiziert“ markiert wurde und dann als „nicht qualifiziert“ eingestuft wird, nachdem er einen Teil Ihres Prozesses durchlaufen hat (z. B. nach einem zweiten Screening / einem Interview / einer Arbeitsprobe).',
    placeholders_help:
      'Um dynamische Informationen einzufügen, können Sie [firstName], [lastName], [companyName] oder [jobTitle] verwenden.',
    language: 'Sprache der Nachrichten',
    language_caption:
      'Automatische Nachrichten werden in der gleichen Sprache wie das Bewerbungsformular gesendet. Sie können die Sprache auf dem Bildschirm "Details zur Stellenanzeige" ändern.',
    help: 'Was sind automatisierte Nachrichten?',
    help_caption:
      'Automatisierte Nachrichten werden versendet, sobald Bewerber eine vordefinierte Stufe durchlaufen. Die hier gezeigten E-Mail-Vorlagen werden dabei als Standard verwendet - der Inhalt der Nachrichten kann jedoch pro Job angepasst werden. Automatisierte Nachrichten können sowohl standardmäßig für alle Jobs als auch individuell pro Job aktiviert oder deaktiviert werden.',
  },
  role: {
    company_owner: 'Hauptbenutzer',
    recruiter: 'Rekruter',
    authenticated: 'Authentifiziert',
  },
  confirm: {
    delete_organization: 'Möchten Sie die Firma wirklich gelöscht werden?',
    delete_job: 'Möchten Sie die Stellenanzeige wirklich löschen?',
  },
  location: {
    country: 'Land',
    city: 'Stadt',
    city_placeholder: 'Stadt suchen',
    homeoffice: 'Remote Job',
    homeofficeDescription: 'Job kann im Home Office ausgeführt werden',
    section_hint:
      'Selbst wenn diese Stelle Remote ausgeschrieben ist, geben Sie einen Ort an, um sicherzustellen, dass sie in den Jobbörsen erscheint',
    section_desc:
      'Die Plattformen listen die Positionen nach dem Standort auf, daher ist es wichtig, einen Standort zu definieren, um sicherzustellen, dass Ihre Anzeige für die Kandidaten sichtbar ist.',
  },
  yes: 'Ja',
  no: 'Nein',
  published: 'veröffentlicht',
  draft: 'in Bearbeitung',


  admin_menu,
  applyForm,
  btn,
  careerLevel,
  contract,
  date,
  dialog,
  display,
  documents,
  education,
  employmentType,
  event,
  help,
  job,
  label,
  login,
  menu,
  message,
  notify,
  rules,
  salary,
  settings,
  status,
  widget,
  workExperience
};
