export default {
  loggedIn: 'Hallo {name}, willkommen zurück!',
  loginFailed: 'Anmeldung fehlgeschlagen. {error}',
  forgotPasswordSuccess: 'Kennwort zurückgesetzt. Bitte prüfen Sie ihre Mails',
  forgotPasswordFail: 'Das Zurücksetzen des Kennwort schlug fehl. {error}',
  registrationSuccess: 'Die Registrierung war erfolgreich. Bitte prüfen Sie ihre Mails',
  registrationFail: 'Die Registrierung schlug fehl. {error}',
  photoSizeValidation: 'Die Größe des hochgeladenen Fotos sollte nicht größer sein {size}',
  companyLogoUploadedSuccess: 'Firmenlogo erfolgreich gespeichert.',
  axiosBackendNotAvailable: 'Das Backend ist derzeit nicht verfügbar.',
  messageTemplateUpdatedSuccess: 'Nachrichtenvorlage erfolgreich aktualisiert.',
  deletedSuccessMessage: '{name} wurde erfolgreich gelöscht.',
  unknownError: 'Unbekannter Fehler aufgetreten.',
  candidateSavedSuccess: 'Der Kandidat wurde erfolgreich gespeichert.',
  jobSavedSuccess: 'Job erfolgreich gespeichert.',
  companyRegisteredSuccess: 'Firma erfolgreich registriert.',
  companyRegisteredFailed: 'Dieses Unternehmen konnte nicht registriert werden.',
}
