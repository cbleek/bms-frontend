export default {
  // Shared tranlations key should be at root level, and other translations
  // should be categorized according to the component/page where they are used.
  name: 'Name',
  email: 'Email',
  countries: 'Länder',
  actions: 'Aktionen',
  team: {
    invited_at: 'Einladung versendet',
    confirmed_at: 'Einladung angenommen',
  },
};
