
export default {
  apprenticeship: 'Lehrstelle',
  contract: 'Befristet',
  employee: 'Angestellter',
  freelance: 'Freie Mitarbeit',
  internship: 'Praktikum',
  label: 'Anstellungsart',
  sideJob: 'Nebenjob',
  student: 'Werkstudent',
  trainee: 'Traineeship',
};
