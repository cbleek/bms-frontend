export default {
  title: 'Betreff',
  type: 'Interview',
  videocall: 'Videocall',
  onSite: 'vor Ort',
  interview: 'Interview',
  phone: 'Telefonat'
}
