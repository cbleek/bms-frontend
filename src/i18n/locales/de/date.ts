export default {
  date: 'Datum',
  today: 'Heute',
  minute1: 'vor einer Minute',
  minutesN: 'vor {0} Minuten',
  hour1: 'vor einer Stunde',
  hoursN: 'vor {0} Stunden',
  yesterday: 'Gestern',
  daysN: 'vor {0} Tagen',
  week1: 'vor einer Woche',
  weeksN: 'vor {0} Wochen',
  month1: 'vor einem Monat',
  monthsN: 'vor {0} Monaten',
  year1: 'vor einem Jahr',
  yearsN: 'vor {0} Jahren'
}
