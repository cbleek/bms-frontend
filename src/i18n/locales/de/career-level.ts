export default {
  label: 'Karrierestufe',
  entry: 'Einstiegsposition',
  junior: 'Junior',
  senior: 'Senior',
  executive: 'Führungsebene',
  seniorExecutive: 'Geschäftsleitung (CEO, CTO etc.)',
}
