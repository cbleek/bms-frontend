export default {
  certificate: 'Zertifikate',
  certificateOfEmployment: 'Arbeitszeugnisse',
  diploma: 'Zeugnisse',
  portfolio: 'Portfolio',
}
