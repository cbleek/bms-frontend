export default {
  label: 'Arbeits Pensum',
  fulltime: 'Vollzeit, unbefristet',
  fulltimeTemporary: 'Vollzeit, befristet',
  parttime: 'Teilzeit, unbefristet',
  parttimeTemporary: 'Teilzeit, befristet',
}
