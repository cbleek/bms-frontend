export default {
  label: 'Berufserfahrung',
  lessThanOneYear: 'Weniger als 1 Jahr',
  oneToThreeYears: '1-3 Jahre',
  threeToFiveYears: '3-5 Jahre',
  fiveToSevenYears: '5-7 Jahre',
  moreThanSevenYears: '> 7 Jahre',
}
