export default {
  label: "Taux d'occupation",
  fulltime: 'Temps plain, durée indéterminée',
  fulltime_temporary: 'Temps plain, durée déterminée',
  parttime: 'Temps partiel, durée indéterminée',
  parttime_temporary: 'Temps partiel, durée déterminée',
}
