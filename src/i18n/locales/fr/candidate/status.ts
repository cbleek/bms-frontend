export default {
  applied: 'postulé',
  underExamination: "cours d'examen",
  screening: 'screening',
  interviews: 'interviews',
  testing: 'tests',
  hired: 'engagé',
  canceled: 'annulé',
  cancelAuto : 'Annulation rapide',
  cancelAutoCaption : 'Le candidat recevra un refus automatique demain matin',
  cancelManual : 'Annulation individuelle',
  cancelManualCaption : 'Vous pouvez amputer le refus individuellement',
  cancelSilent : 'Ne pas informer le candidat',
  cancelSilentCaption : 'Seul le statut sera modifié'
}
