export default {
  label: 'Position',
  entry: 'Débutant',
  junior: 'Junior',
  senior: 'Senior',
  executive: 'Cadre',
  senior_executive: 'Cadre supérieurs (PDG, CTO, etc.)',
}
