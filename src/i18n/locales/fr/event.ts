export default {
  title: 'Sujet',
  type: 'Interview',
  videocall: 'appel vidéo',
  onSite: 'sur le site',
  interview: 'interview',
  phone: 'appel téléphonique'
}
