export default {
  label: 'Formation',
  school: "Diplôme d'études secondaires",
  completed_education: 'Formation professionnelle achevée',
  bachelor_student: 'Étudiant de premier cycle',
  bachelor_completed: 'Bachelor (Bac +3)',
  master_student: 'Étudiant en Master',
  master_completed: 'Master (Bac +5)',
  phd_completed: 'Doctorat (Bac +8)'
}