export default {
  date: 'Date',
  today : "aujourd'hui",
  minute1 : 'il y a une minute',
  minutesN : 'il y a {0} minutes',
  hour1 : 'il y a une heure',
  hoursN : 'il y a {0} heures',
  yesterday : 'hier',
  daysN : 'il y a {0} jours',
  week1 : 'il y a une semaine',
  weeksN : 'il y a {0} semaines',
  month1 : 'il y a un mois',
  monthsN : 'il y a {0} mois',
  year1 : 'il y a un an',
  yearsN : 'il y a {0} ans'
}
