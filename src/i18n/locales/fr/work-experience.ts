export default {
  label : 'Expérience professionnelle',
  lessThanOneYear :"'Moins d'un an",
  oneToThreeYears : '1-3 ans',
  threeToFiveYears : '3-5 ans',
  fiveToSevenYears : '5-7 ans',
  moreThanSevenYears : '> 7 ans',
}
