import admin_menu from './admin/menu'
import applyForm from './application-form';
import btn from './btn';
import careerLevel from './career-level';
import contract from './contract';
import date from './date';
import dialog from './dialog';
import display from './display';
import documents from './documents';
import event from './event';
import education from './education';
import employmentType from './employment-type';
import help from './help';
import job from './job';
import label from './label';
import menu from './menu';
import message from './message';
import notify from './notify';
import rules from './rules';
import salary from './salary';
import settings from './settings';
import status from './candidate/status';

export default {
  localeName: 'French',
  english: 'Anglais',
  french: 'Français',
  german: 'Allemand',
  jobs: "Offres d'emploi",
  jobboard: "Bourse d'emploi",
  public_jobs: "annonces d'emploi actuelles",
  ad_management: "Gestion d'annonces",
  team_management: "Gestion de l'équipe",
  invite_new_user: "Inviter des membres de l'équipe",
  statistics: 'Statistiques',
  templates: 'Modèles',
  company: 'Entreprise',
  recruiter: 'Recruteur',
  title: 'Veuillez vous identifier.',
  unpublished: 'non publié',
  action: 'Action',
  please_register: "Vous n'êtes pas connecté pour le moment. Pour pouvoir utiliser la gestion des annonces, vous devez vous inscrire. L'inscription est gratuite.",
  preis1: "L'inscription est gratuite",
  confirm_del: "Voulez-vous vraiment supprimer l'offre d'emploi ?",
  alert: 'Alerte',
  wizard_help_title: "Créer une offre d'emploi",
  wizard_help_text: "Le Jobwizard vous aide à créer une annonce. L'utilisation du Jobwizard et la création de l'annonce sont gratuites. Vous pouvez activer ou désactiver d'autres champs dans les paramètres.",
  wizard_help_anonymous: "Vous n'êtes actuellement pas connecté. Vous pouvez utiliser toutes les fonctions en tant qu'utilisateur anonyme. Cependant, vous ne pourrez finalement télécharger l'annonce qu'au format HTML.",
  select_or_create: 'Choisissez une entreprise ou créez-en une nouvelle.',
  welcome: 'Willkommen',
  user_profile: "Profil d'utilisateur",
  company_profile: "Profil de l'entreprise",

  msg: {
    job_saved_success: "l'offre d'emploi a été sauvegardée avec succès",
  },
  location: {
    country: 'Pays',
    city: 'Ville',
    city_placeholder: 'Cherchez une ville',
    homeoffice: 'Télétravail',
    homeofficeDescription: 'Cet emploi peut être effectué en télétravail',
    section_hint:
      "Afin d'assurer la visibilité de votre offre vous devez indiquer le lieu, même si ce sera en télétravail.",
    section_desc:
      "Certaines platesformes classent les postes par lieu. Il est donc essentiel d'en définir un pour vous assurer que votre poste sera visible sur toutes les platesformes.",
  },
  yes: 'Qui',
  no: 'Non',
  published: 'Publié',
  draft: 'en cours',

  admin_menu,
  applyForm,
  btn,
  careerLevel,
  contract,
  date,
  dialog,
  display,
  documents,
  education,
  employmentType,
  event,
  help,
  job,
  label,
  menu,
  message,
  notify,
  rules,
  salary,
  settings,
  status,
};
