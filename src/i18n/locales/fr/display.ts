export default {
  label: 'Présentation',
  dark: 'Design foncé',
  bright: 'Design clair',
  auto: "État de l'appareil"
}
