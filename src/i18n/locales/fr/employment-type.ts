
export default {
  label: 'Type de contrat',
  employee: 'CDI',
  freelance: 'Freelance',
  contract: 'CDD',
  trainee: 'Stage professionnel',
  internship: 'Stage',
  apprenticeship: 'Apprentissage',
  student: 'Job étudiant',
  sideJob: 'Activité complémentaire',
};
