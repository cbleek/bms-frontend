/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  abort : 'Annuler',
  all : 'Tout le monde',
  applyPostmail : 'Candidature par courrier',
  applyText : 'Postuler maintenant',
  back : 'Retour',
  cancel : 'Annuler',
  close : 'Fermer',
  continue : 'Continuer',
  delete : 'supprimer',
  download : 'Télécharger',
  email : 'E-mail',
  forCompanies: 'pour les employeurs',
  inbox : 'Boîte de réception',
  inviteNewUser: 'Invite team members',
  login : 'Se connecter',
  logout : 'Se déconnecter',
  magazin : 'Magazin',
  my : 'Mes',
  myCareer : 'Ma carrière',
  myJobs : 'Mes emplois',
  new : 'Nouveau',
  next: 'Continuer',
  non: 'Non',
  outbox : "Boîte d'envoi",
  preview : 'Aperçu',
  previus: 'Retour',
  publish : 'Publier',
  unPublish : 'dépublier',
  rate: 'Évaluer',
  register : 'Enregistrer',
  save : 'Sauver',
  send : 'Envoyer',
  today: "Aujourd'hui",
  reinviteUser: 'Re-invite User',
  updateUser: 'Update User',
  yes: 'Oui'
};
