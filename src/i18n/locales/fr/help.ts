/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  addAttachments: 'Ajouter des fichiers joints',
  addFile: 'Ajouter un fichier',
  applyPost : "vous pouvez indiquer ici si vous préférez recevoir les candidatures sous forme traditionnelle par courrier. Le bouton de candidature dans l'offre d'emploi s'adapte alors en conséquence",
  darkmode : "Activer ou désactiver l'affichage nocturne",
  forgotPassword : "Nous enverrons un e-mail avec un lien pour réinitialiser le mot de passe de l'adresse e-mail saisie",
  requestAdditionalDocuments: 'Demande de documents supplémentaires',
  reference : "Vous pouvez attribuer un numéro de référence à votre annonce. Un candidat peut s'y référer.",
  visibility : "Masquer le salaire dans l'offre d'emploi",
  visibilityCarrerPage : "Vous pouvez mettre temporairement hors ligne la page carrière de votre entreprise.{0} La visibilité de vos offres d'emploi pour les candidats, les sites d'emploi et les moteurs de recherche n'en est pas affectée.",
};
