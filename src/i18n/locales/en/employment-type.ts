
export default {
  label: 'Type of employment',
  employee: 'Employee',
  freelance: 'Freelancer',
  contract: 'Contact',
  trainee: 'Traineeship',
  internship: 'Internship',
  apprenticeship: 'Apprenticeship',
  student: 'Student',
  sideJob: 'Side job',
};
