export default {
  date: 'Date',
  today: 'Today',
  minute1: 'one minute ago',
  minutesN: '{0} minutes ago',
  hour1: 'one hour ago',
  hoursN: '{0} hours ago',
  yesterday: 'yesterday',
  daysN: '{0} days ago',
  week1: 'a week ago',
  weeksN: '{0} weeks ago',
  month1: 'one month ago',
  monthsN: '{0} months ago',
  year1: 'one year ago',
  yearsN: '{0} years ago'
}
