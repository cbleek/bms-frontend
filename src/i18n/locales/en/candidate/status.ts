export default {
  applied: 'applied',
  underExamination: 'under examination',
  screening: 'screening',
  interviews: 'interviews',
  testing: 'tests',
  hired: 'hired',
  canceled: 'canceled',
  cancelAuto: 'Quick rejection',
  cancelAutoCaption: 'The applicant will receive an automatic rejection tomorrow morning',
  cancelManual: 'Individual rejection',
  cancelManualCaption: 'You can customize the cancellation',
  cancelSilent: 'Do not notify applicant',
  cancelSilentCaption: 'Only the status will be changed'
}
