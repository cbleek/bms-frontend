export default {
  label: 'Presentation',
  dark: 'Dark Design',
  bright: 'Light Design',
  auto: 'Device status'
}

