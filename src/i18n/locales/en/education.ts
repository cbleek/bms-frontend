export default {
  label: 'Berufsausbildung',
  school: 'Schüler',
  completedEducation: 'abgeschlossene Ausbildung',
  bachelorStudent: 'Bachelor-Student',
  bachelorCompleted: 'Bachelor abgeschlossen',
  masterStudent: 'Master-Student',
  masterCompleted: 'Master abgeschlossen',
  phdCompleted: 'Dr. / PhD'
}