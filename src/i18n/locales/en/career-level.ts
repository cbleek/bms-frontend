export default {
  label: 'career level',
  entry: 'entry level',
  junior: 'junior position',
  senior: 'senior position',
  executive: 'executive position',
  seniorExecutive: 'senior executive position',
}
