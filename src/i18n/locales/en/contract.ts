export default {
  label: 'Workload',
  fulltime: 'Full-time, permanent',
  fulltime_temporary: 'Full-time, temporary',
  parttime: 'Part-time, permanent',
  parttime_temporary: 'Part-time, temporary',
}
