export default {
  label: 'Work experience',
  lessThanOneYear: 'Less than 1 year',
  oneToThreeYears: '1-3 years',
  threeToFiveYears: '3-5 years',
  fiveToSevenYears: '5-7 years',
  moreThanSevenYears: '> 7 years',
}
