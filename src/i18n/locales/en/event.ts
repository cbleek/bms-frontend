export default {
  title: 'Subject',
  type: 'interview',
  videocall: 'video call',
  onSite: 'on site',
  interview: 'interview',
  phone: 'phone call'
}
