/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  abort: 'Cancel',
  all: 'All',
  applyPostmail: 'Apply by mail',
  applyText: 'Apply now',
  back: 'Back',
  cancel: 'Cancel',
  close: 'Close',
  continue: 'Continue',
  delete: 'Delete',
  download: 'Download',
  email: 'Email',
  forCompanies: 'for employers',
  inbox: 'Inbox',
  inviteNewUser: 'Invite team members',
  login: 'Log in',
  logout: 'Logout',
  magazin: 'Magazin',
  my: 'My',
  myCareer: 'My Career',
  myJobs: 'My Jobs',
  new: 'New',
  next: 'Next',
  no: 'No',
  outbox: 'Outbox',
  preview: 'Preview',
  previous: 'Prev',
  publish: 'Publish',
  unPublish: 'Unpublish',
  rate: 'Rate',
  register: 'Register',
  save: 'Save',
  send: 'Send',
  today: 'Today',
  reinviteUser: 'Re-invite User',
  updateUser: 'Update User',
  yes: 'Yes'
};
