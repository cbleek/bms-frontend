import admin_menu from  './admin/menu'
import applyForm from  './application-form'
import btn from './btn';
import careerLevel from './career-level';
import contract from './contract';
import date from './date';
import dialog from './dialog';
import display from './display';
import documents from './documents';
import education from './education';
import employmentType from './employment-type';
import event from './event';
import help from './help';
import job from './job';
import label from './label';
import menu from './menu';
import message from './message';
import notify from './notify';
import rules from './rules';
import salary from './salary';
import settings from './settings';
import status from './candidate/status';
import widget from './widget';
import workExperience from './work-experience';

export default {
  localeName: 'English',
  french: 'French',
  german: 'German',
  english: 'English',
  jobs: 'Jobs',
  logo: 'Logo',
  create_job: 'Create new job',
  edit_job: 'Edit job',
  header: 'Headerimage',
  jobboard: 'Jobboard',
  ad_management: 'Ad management',
  team_management: 'Team management',
  invite_new_user: 'Invite team members',
  public_jobs: 'Current job offers',
  templates: 'Templates',
  company: 'Company',
  recruiter: 'Recruiter',
  title: 'Please register',
  action: 'Action',
  unpublished: 'unpublished',
  'ad-management-description':
    'With the advertisement management you can save, edit and delete job advertisements. The advertisement management is also required to integrate job advertisements into your homepage.',
  preis1: 'Registration is free of charge',
  please_register:
    'You are currently not logged in. To use the ad management, you must register. Registration is free of charge.',
  confirm_del: 'Do you really want to delete the job advertisement?',
  alert: 'Alert',
  wizard_help_title: 'Create job ad',
  wizard_help_text:
    'The Jobwizard supports you in creating an advertisement. The use of the Job Wizard and the creation of the advertisement is free of charge. You can activate and deactivate further fields via the settings.',
  wizard_help_anonymous:
    'You are currently not logged in. You can use all functions as an anonymous user. However, at the end you can only download the advertisement as HTML.',
  select_or_create: 'Choose a company or create a new one.',
  welcome: 'Willkommen',
  user_profile: 'User profile',
  company_profile: 'Company profile',

  msg: {
    job_saved_success: 'job posting successfully saved',
  },
  location: {
    country: 'Country',
    city: 'City',
    city_placeholder: 'Search city',
    homeoffice: 'Remote Job',
    homeofficeDescription: 'Job can be performed remotely',
    section_hint:
      'Even if this job is remote, specify a location to ensure it will appear on job boards',
    section_desc:
      'Some platforms list positions by location, so defining one is essential to ensure your job will be visible on all platforms.',
  },
  mail: {
    application_received: 'application received',
    application_received_caption:
      'If a candidate applies for one of your positions, we will immediately send them the message below',
    reject_after_first_review: 'Rejection after review of the documents',
    reject_after_first_review_caption:
      'This message will be sent after you have reviewed an applicant\'s application materials and deemed them "not qualified".',
    reject_during_process: 'Rejection at a later stage',
    reject_during_process_caption:
      'This message is sent the next morning when an applicant was first marked as "qualified" and then is deemed "not qualified" after going through part of your process (e.g., after a second screening/interview/work sample)',
    placeholders_help:
      'To insert dynamic information, you can use [firstName], [lastName], [companyName] or [jobTitle].',
    language: 'language of the messages',
    language_caption:
      'Automatic messages are sent in the same language as the application form. You can change the language on the Job Posting Details screen.',
    help: 'What are automated messages?',
    help_caption:
      'Automated messages are sent when applicants pass a predefined stage. The email templates shown here are used as defaults - but the content of the messages can be customized per job. Automated messages can be enabled or disabled by default for all jobs as well as individually per job.'
  },

  yes: 'Yes',
  no: 'No',
  published: 'published',
  draft: 'in process',

  admin_menu,
  applyForm,
  btn,
  careerLevel,
  contract,
  date,
  dialog,
  display,
  documents,
  education,
  employmentType,
  event,
  help,
  job,
  label,
  menu,
  message,
  notify,
  rules,
  salary,
  settings,
  status,
  widget,
  workExperience
};
