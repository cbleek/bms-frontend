export default {
  label: 'salary',
  range: 'salary range',
  singleValue: 'single value',
  to: 'to',
  visibility: 'show in job ad',
  enterSalary: 'enter salary',
  hour: 'per hour',
  day: 'per day',
  week: 'per week',
  month: 'per month',
  year: 'per year',
  sectionHint: 'Platforms that offer filtering options to candidates use this information to better position your job.',
  sectionDesc: 'This information can significantly improve your position on some job boards.'
}
