/*
 Shared tranlations key should be at root level, and other translations
 should be categorized according to the component/page where they are used.
*/

export default {
  addAttachments: 'Add attachments',
  addFile: 'Pick file',
  addPhoto: 'Select photo',
  applyPost: 'here you can specify whether you prefer applications in the traditional form by mail. The apply button in the job ad will then adjust accordingly.',
  darkmode: 'turn night display on and off',
  forgotPassword: 'We will send a mail with a link to reset the password for the entered e-mail address',
  maxFilesize: 'Die maximale Dateigröße beträgt {size}',
  requestAdditionalDocuments: 'Eequest additional Documents',
  reference: 'You can assign a reference number to your ad. An applicant can refer to it.',
  visibility: 'Hide salary in job ad',
  visibilityCarrerPage: "You can temporarily take your company's career page offline.{0} This does not affect the visibility of your job ads for applicants, job boards and search engines.",
};
