# Frontend

Frontend of the [BMS](https://bms.jobsintown.org). See [Docs](https://gitlab.com/pwrk/bewerbermanagement/bms-backend/-/blob/main/README.md?ref_type=heads)

## Demo

https://bms.jobsintown.org

you can login with different [roles](https://gitlab.com/pwrk/bewerbermanagement/bms-frontend/-/wikis/roles):


| Role           | Username                 | Password   |
|-               |-                         |-           |
| Company Owner  | `demo`                   | `demo00`   |
| Employee       | `employee`               | `demo00`   |
| Candidate      | `candidate`              | `demo00`   |

Admin users can be created using the [commanline of the backend](https://gitlab.com/pwrk/bewerbermanagement/bms-backend#setup).

Admin users can login into the frontend at: https://bms.jobsintown.org/#/login-admin

## Requirements

* Running strapi Backend located at: https://gitlab.com/pwrk/bewerbermanagement/bms-backend
* [node](https://nodejs.org/) (16|18|20) and [yarn](https://yarnpkg.com/getting-started/install)

## Install

install dependencies and start a development environment by:

```bash
git clone https://gitlab.com/pwrk/bewerbermanagement/bms-backend.git
cd bms-backend
yarn && yarn dev
```

Browser should open at http://localhost:9000

## Configuration

[.env](https://gitlab.com/pwrk/bewerbermanagement/bms-frontend/-/blob/main/.env) contains settings. It's git controlled. You can override it by using a `.env.local`


